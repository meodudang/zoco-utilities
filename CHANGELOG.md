ZOCO Utility Change Log
==========================

0.1.0 under development
-----------------------

- Chg: thay đổi namespace `hoksilato` sang `zoco` (cangtt)
- Chg: thay đổi tên class `zoco\helpers\common\HArrayHelper` thành `zoco\helpers\common\ArrayHelper` (cangtt)
- Enh: thêm phpdoc cho các hàm (cangtt)
- Enh: reformat code, chỉnh sửa lại một số tên biến (cangtt)
- Enh: refactoring hàm `getNotification` của `zoco\helpers\common\Helper` (cangtt)
- Chg: đổi tên hàm `isChangeNickname` thành `isChangedNickname` của `zoco\helpers\common\RedisHelper` (cangtt)
- Chg: `api` và `id` sẽ sử dụng class `zoco\helpers\api\RedisHelper` (cangtt)
- Chg: `backend` sẽ sử dụng class `zoco\helpers\backend\RestHelper` (cangtt)
- Chg: đổi field `is_show` thành `status` trong các mảng Parameters khi gọi sang API