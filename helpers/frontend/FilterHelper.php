<?php

namespace zoco\helpers\frontend;

use Yii;

/**
 * Helper provides helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.1.0
 */
class FilterHelper
{
    /**
     * Get bad words list
     * @return array
     */
    public static function getBadWordList()
    {
        $badwords = Yii::$app->cache->get('BadWordList');

        if ($badwords === false) {
            $filterword = RestHelper::getItems('filter-word/all', [
                'orderby' => ['created_at' => SORT_ASC],
                'limit' => 1,
                'notIndex' => 1,
            ]);

            if (!empty($filterword)) {
                $badwords = explode('|', $filterword[0]['words']);
                Yii::$app->cache->set('BadWordList', $badwords, 3600);
            } else {
                $badwords = [];
            }
        }

        return $badwords;
    }

    /**
     * Replace bad words
     * @param  string $text    text to replace
     * @param  string $replace replace string
     * @return string
     */
    public static function clearBadWord($text = '', $replace = '***')
    {
        if (!empty($text)) {
            $find = static::getBadWordList();
            if (!empty($find)) {
                return str_ireplace($find, $replace, $text);
            }
        }

        return $text;
    }

    /**
     * Check whether current word is bad word
     * @param  string $text string need to be checked
     * @return boolean
     */
    public static function checkBadWord($text = '')
    {
        $result = false;
        if (!empty($text)) {
            $find = static::getBadWordList();
            if (!empty($find)) {
                $matchFound = preg_match_all(
                    "/\b(" . implode($find, "|") . ")\b/i",
                    $text
                );

                if ($matchFound > 0) {
                    $result = true;
                }
            }
        }

        return $result;
    }
}
