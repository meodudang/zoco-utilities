<?php

namespace zoco\helpers\frontend;

use zoco\helpers\common\Helper as BaseHelper;

/**
 * Helper provides helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.5.0
 */
class Helper extends BaseHelper
{
    /**
     * Processing website url for get item list
     * @param  string $url website url
     * @return string
     */
    public static function processWebsiteUrl($url)
    {
        // Remove scheme from url
        $url = str_replace(['http://', 'https://'], '', $url);

        // Parse url
        $url = \Purl\Url::parse($url);
        if (is_null($url->subdomain) || $url->subdomain == 'www') {
            return $url->registerableDomain . $url->path;
        }

        return $url->subdomain . '.' . $url->registerableDomain . $url->path;
    }

    public static function getTotalPages($urlApi, $params, $pageSize, $isTotalRecord = false)
    {
        $totalPages = 0;
        $totalRecord = RestHelper::getItems($urlApi, $params);
        if (!empty($totalRecord)) {
            $totalPages = ceil($totalRecord / $pageSize);
        } else {
            $totalRecord = 0;
        }

        if ($isTotalRecord) {
            return [$totalPages, $totalRecord];
        }
        return $totalPages;
    }

    public static function getCreatePagesRedis($urlApi, $params, $pageSize, $numPage)
    {
        $totalPages = 0;
        $offset = 0;
        $limit = 0;
        $totalRecord = RestHelper::getItems($urlApi, $params);
        if (!empty($totalRecord)) {
            $totalPages = ceil($totalRecord / $pageSize);
            if ($numPage < $totalPages) {
                $offset = $totalRecord - ($numPage * $pageSize);
                $limit = $pageSize;
            } else if ($numPage == $totalPages) {
                $offset = 0;
                $limit =  $totalRecord - (($numPage-1) * $pageSize);
            }
        }
        return [$totalPages, $offset, $limit];
    }

    public static function randomPageIndex()
    {
        $temps = Helper::params('random-index');
        if ($temps['type'] == 1) {
            return mt_rand(1, 5);
        } else if ($temps['type'] == 2) {
            $hour = intval(date('h', time()));
            $istime = 0;
            foreach ($temps['times'] as $i => $value) {
                if ($hour > $value) {
                    $istime = $i;
                    break;
                }
                continue;
            }

            $arrRandomIndex = \Yii::$app->cache->get('random-index');
            if (($arrRandomIndex === false) ||($arrRandomIndex['istime'] != $istime)) {
                $arrRandomIndex = [];
                $arrRandomIndex['istime'] = $istime;
                $arrRandomIndex['numberRand'] = mt_rand(1, 5);

                \Yii::$app->cache->set('random-index', $arrRandomIndex, 3600*5);
            }
            return $arrRandomIndex['numberRand'];
        }

        return 1;
    }
}
