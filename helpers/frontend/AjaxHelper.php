<?php

namespace zoco\helpers\frontend;

use Yii;
use yii\imagine\Image;
use yii\helpers\VarDumper;
use yii\helpers\FileHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;

/**
 * AjaxHelper provides ajax helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.1.0
 */
class AjaxHelper
{
    /**
     * Return json result
     * @param  string $message error message
     * @param  integer $error error code
     * @param  array $data data to return
     * @return json
     */
    public static function jsonResult($error, $message, $data = [])
    {
        $arrResult = [
            'error' => $error,
            'message' => $message,
        ];

        $arrResult = ArrayHelper::merge($arrResult, $data);

        echo Json::encode($arrResult);
        Yii::$app->end();
    }

    /**
     * Ajax delete button/link
     * @param  string $objectType set|collection|item|group|comment...
     * @param  integer $objectId
     * @return string
     */
    public static function ajaxDelete($objectType, $objectId, $buttonLabel, $cssClass)
    {
        return Html::a($buttonLabel, 'javascript:;', [
            'onclick' => "
                if (_executed == 0) {
                    _executed = 1;
                    swal({
                        title: '" . Yii::t('common', 'Are you sure?') . "',
                        text: '" . Yii::t('common', 'You will not be able to recover this {0}!', Yii::t('common', '{0}', $objectType)) . "',
                        type: 'warning',
                        showCancelButton: true,
                        confirmButtonColor: '#DD6B55',
                        confirmButtonText: '" . Yii::t('common', 'Yes, delete it!') . "',
                        closeOnConfirm: false
                    },
                    function() {
                        $.ajax({
                            type: 'POST',
                            cache: false,
                            dataType: 'json',
                            url: '/" . $objectType . "/delete',
                            data: {'id': '" . $objectId . "'},
                            success: function(response) {
                                if (response.error == 0) {
                                    swal({
                                        title: response.message,
                                        text: '" . Yii::t('common', 'Do you want to create new {0}?', Yii::t('common', '{0}', $objectType)) . "',
                                        type: 'success',
                                        showCancelButton: true,
                                        confirmButtonColor: '#CC0000',
                                        confirmButtonText: '" . Yii::t('common', 'Yes') . "',
                                        cancelButtonText: '" . Yii::t('common', 'No') . "',
                                        closeOnConfirm: true,
                                        closeOnCancel: true
                                    },
                                    function(isConfirm){
                                        var url;
                                        if (isConfirm){
                                            url = '" . (($objectType == 'set') ? Url::to(['/mix/mix/index']) : Url::to(['/mix/mix/' . $objectType])) . "';
                                        } else {
                                            url = '" . Url::to(['/set/set/index']) . "';
                                        }

                                        window.location = url;
                                    });
                                } else {
                                    swal('Have some errors', response.message, 'error');
                                }
                            },
                            error: function(response) {
                                swal('Have some errors', 'Error: ' + response.message, 'error');
                            },
                            complete: function(data) {
                                _executed = 0;
                            }
                        });
                    });
                }
                return false;
                ",
            'class' => $cssClass,
        ]);
    }

    /**
     * Ajax like button/link
     * @param  string $objectType set|collection|item|group|comment...
     * @param  integer $objectId
     * @param  string $objectName
     * @param  array $options
     * @param  string $sTitle
     * @param  array $sTag
     * @return string
     */
    public static function ajaxLike($objectType, $objectId, $objectName, $options = ['class' => 'like-btn'], $sTitle = '', $sTag = [])
    {
        $iIsFavorite = 0;

        if (isset(Helper::user()->id)) {
            $sUserLikeKey = sprintf(Helper::params('redisKeys')['userlike' . $objectType . 's'], Helper::user()->id);

            $iIsCheck = RestHelper::getItems('redis/check', [
                'key' => $sUserLikeKey,
                'element' => $objectId,
            ]);

            if (!empty($iIsCheck)) {
                $iIsFavorite = 1;
            }
        }

        if ($iIsFavorite == 1) {
            $sTitle = '<i class="fa fa-heart"></i>' . $sTitle;
        } else {
            $sTitle = '<i class="fa fa-heart-o"></i>' . $sTitle;
        }

        $title = $sTitle;
        if (!empty($sTag) && !empty($sTag['name'])) {
            if (!empty($sTag['options'])) {
                $title = Html::tag($sTag['name'], $sTitle, $sTag['options']);
            } else {
                $title = Html::tag($sTag['name'], $sTitle);
            }
        }
        $params['onclick'] = "
            if (_executed == 0) {
                _executed = 1;
                $.ajax({
                    type: 'POST',
                    cache: false,
                    url: '/like/like/likes',
                    data: {'objectType':'" . $objectType . "','objectId':'" . $objectId . "','objectName':'" . $objectName . "'},
                    dataType: 'json',
                    success: function(data) {
                        if (data.ok == 2) {
                            $('." . $options['class'] . "').find('i.fa-heart').removeClass('fa-heart').addClass('fa-heart-o');
                            if ($('span#objectlike').length) {
                                i = parseInt($('span#objectlike').html());
                                $('span#objectlike').html(--i);
                            }
                            if ($('div#likemore').length) {
                                $.ajax({
                                    type: 'POST',
                                    url: '/like/like/more',
                                    data: {'objecttype':'" . $objectType . "','object_id':'" . $objectId . "'},
                                    dataType: 'json',
                                    success: function(data) {
                                        $('div#likemore').html(data.html);
                                    }
                                });
                            }
                        } else if (data.ok == 1) {
                            $('." . $options['class'] . "').find('i.fa-heart-o').removeClass('fa-heart-o').addClass('fa-heart');
                            if ($('span#objectlike').length) {
                                i = parseInt($('span#objectlike').html());
                                $('span#objectlike').html(++i);
                            }
                            if ($('div#likemore').length) {
                                $.ajax({
                                    type: 'POST',
                                    url: '/like/like/more',
                                    data: {'objecttype':'" . $objectType . "','object_id':'" . $objectId . "'},
                                    dataType: 'json',
                                    success: function(data) {
                                        $('div#likemore').html(data.html);
                                    }
                                });
                            }
                        } else if (data.ok == 3) {
                            $('#user-login').trigger('click');
                        }
                    },
                    complete: function(data) {
                        _executed = 0;
                    }
                });
            }
            return false;
        ";

        if (!empty($options)) {
            foreach ($options as $key => $value) {
                $params[$key] = $value;
            }
        }

        return Html::a($title, "javascript:;", $params);
    }

    /**
     * Ajax follow button/link
     * @param  integer $objectId
     * @param  string $objectName
     * @param  string $sCssClass
     * @param  string $sPlace place where to hide/show button
     * @param  integer|null $iProfilePageUserId user if viewing profile page
     * @return string
     */
    public static function ajaxFollow($objectId, $objectName, $sCssClass = '', $sPlace = 'profile', $iProfilePageUserId = null)
    {
        $iIsFollow = 0;
        $owner = 0;
        $tip = Yii::t('common', 'Follow');
        $title = '<i class="fa fa-rss"></i>';
        if (isset(Helper::user()->id)) {
            $sUserFollowingsKey = sprintf(Helper::params('redisKeys')['userfollowings'], Helper::user()->id);
            $iIsCheck = RestHelper::getItems('redis/check', [
                'key' => $sUserFollowingsKey,
                'element' => $objectId,
            ]);

            if (!empty($iIsCheck)) {
                $iIsFollow = 1;
                $tip = Yii::t('common', 'Unfollow');
                $title = '<i class="fa fa-rss active"></i>';
            }

            if (Helper::user()->id == $objectId) {
                $owner = 1;
            }
        }

        $result = '<a href="javascript:;" class="' . $sCssClass . '"><i class="fa fa-rss"></i></a>';
        if ($owner == 0) {
            // Don't show follow button at other's profile page except herself/himself profile page
            if (isset(Helper::user()->id) && $sPlace == 'follow' && $iProfilePageUserId != Helper::user()->id) {
                return '';
            }

            $params['onclick'] = "
                if (_executed == 0) {
                    _executed = 1;
                    $.ajax({
                        type: 'POST',
                        cache: false,
                        url: '/like/like/follows',
                        data: {'objectId':'" . $objectId . "','objectName':'" . $objectName . "'},
                        dataType: 'json',
                        success: function(data) {
                            if (data.ok == 2) {
                                $('a#follow-" . $objectId . "').find('i.fa-rss').toggleClass('active');
                                $('a#follow-" . $objectId . "').attr('title','" . Yii::t('common', 'Follow') . "');
                                if ($('span#numfollow').length) {
                                    i = parseInt($('span#numfollow').html());
                                    $('span#numfollow').html(--i);
                                }
                            } else if (data.ok == 1) {
                                $('a#follow-" . $objectId . "').find('i.fa-rss').toggleClass('active');
                                $('a#follow-" . $objectId . "').attr('title','" . Yii::t('common', 'Unfollow') . "');
                                if ($('span#numfollow').length) {
                                    i = parseInt($('span#numfollow').html());
                                    $('span#numfollow').html(++i);
                                }
                            } else if (data.ok == 3) {
                                $('#user-login').trigger('click');
                            }
                        },
                        complete: function(data) {
                            _executed = 0;
                        }
                    });
                }
                return false;
            ";

            $params['class'] = $sCssClass;
            $params['id'] = 'follow-' . $objectId;
            $params['title'] = $tip;

            $result = Html::a($title, "javascript:;", $params);
        }

        return $result;
    }

    /**
     * Ajax bookmark button/link
     * @param  integer $objectId
     * @param  string $objectName
     * @param  string $objectType
     * @param  string $sTitle
     * @return string
     */
    public static function ajaxBookmark($objectId, $objectName, $objectType = 'item', $sTitle = '')
    {
        $iIsBookmark = 0;

        if (isset(Helper::user()->id)) {
            $sUserBookmarksKey = sprintf(Helper::params('redisKeys')['userbookmarks'], Helper::user()->id);

            $iIsCheck = RestHelper::getItems('redis/check', [
                'key' => $sUserBookmarksKey,
                'element' => $objectId,
            ]);

            if (!empty($iIsCheck)) {
                $iIsBookmark = 1;
            }
        }

        $messageUnBookmark = Yii::t('bookmark', 'You have bookmarked this item.');
        $messageBookmark = Yii::t('bookmark', 'You have unbookmarked this item.');

        return Html::a('<i class="fa fa-bookmark"></i><span class="f-s-80">' . (($iIsBookmark > 0) ? Yii::t('bookmark', 'Unbookmark this item') : Yii::t('bookmark', 'Bookmark this item')) . '</span>', "javascript:;", [
            'onclick' => "
                if (_executed == 0) {
                    _executed = 1;
                    $.ajax({
                        type: 'POST',
                        cache: false,
                        url: '/like/like/bookmarks',
                        data: {'objectId':'" . $objectId . "','objectType':'" . $objectType . "','objectName':'" . $objectName . "'},
                        dataType: 'json',
                        success: function(data) {
                            if (data.ok == 2) {
                                $('a.isbookmark').removeClass('active');
                                $('a.isbookmark').html('" . '<span class="f-s-80"><i class="fa fa-bookmark"></i> ' . Yii::t('bookmark', 'Bookmark this item') . "</span>');
                            } else if (data.ok == 1) {
                                $('a.isbookmark').addClass('active');
                                $('a.isbookmark').html('" . '<span class="f-s-80"><i class="fa fa-bookmark"></i> ' . Yii::t('bookmark', 'Unbookmark this item') . "</span>');
                            } else if (data.ok == 3) {
                                $('#user-login').trigger('click');
                            }
                        },
                        complete: function(data) {
                            _executed = 0;
                        }
                    });
                }
                return false;
            ",
            'class' => ($iIsBookmark > 0) ? 'isbookmark active' : 'isbookmark',
        ]);
    }

    /**
     * Ajax items/sets/collections load more
     * @param  string $objectType
     * @param  integer $objectId
     * @param  integer $iSimilar
     * @param  integer $iCategoryId
     * @return string
     */
    public static function ajaxItemMore($objectType, $objectId, $iSimilar, $iCategoryId = 0)
    {
        $strcate = '';

        if ($iCategoryId > 0) {
            $strcate = ",'categoryid':'" . $iCategoryId . "'";
        }

        return Html::a(($iSimilar == 0 ? Yii::t('common', "SIMILAR " . strtoupper($objectType) . "S") : Yii::t('common', strtoupper($objectType)) ), "javascript:;", [
            'onclick' => "
                if (_executed == 0) {
                    _executed = 1;
                    $.ajax({
                        type: 'POST',
                        cache: false,
                        url: '/item/item/more',
                        data: {'objectType':'" . $objectType . "','objectId':'" . $objectId . "'" . (!empty($strcate) ? $strcate : '') . "},
                        success: function(data) {
                            $('ul.similar > li').removeClass('active');
                            $('ul.similar > li:eq(" . $iSimilar . ")').addClass('active');
                            $('div.user-items-list').html(data);
                            resize_item('user-items-list', 'user-item', 4, 3, 2, 1);
                        },
                        complete: function(data) {
                            _executed = 0;
                        }
                    });
                }
                return false;
            ",
        ]);
    }

    /**
     * Ajax vote button
     * @param  integer $objectId
     * @param  string $objectName
     * @param  string $name
     * @param  string $class
     * @return string
     */
    public static function ajaxVote($objectId, $objectName, $name = '', $class = '')
    {
        $iIsVoted = 0;

        if (!Yii::$app->getModule('vote')->enable) {
            return '';
        }

        if (isset(Helper::user()->id)) {
            if (Helper::user()->id == $objectId) {
                return '';
            }

            $sUserVotesKey = sprintf(Helper::params('redisKeys')['uservoted'], Helper::user()->id);
            $iIsCheck = RestHelper::getItems('redis/check', [
                'key' => $sUserVotesKey,
                'element' => $objectId,
            ]);

            if (!empty($iIsCheck)) {
                 $iIsVoted = 1;
            }
        }

        $messageVoted = Yii::t('vote', 'You voted this user.');

        return $result = Html::a($name, "javascript:;", [
            'onclick' => "
                if (_executed == 0) {
                    _executed = 1;
                    $.ajax({
                        type: 'POST',
                        cache: false,
                        url: '/vote/vote/vote',
                        data: {'objectId':'" . $objectId . "','objectName':'" . $objectName . "'},
                        dataType: 'json',
                        success: function(data) {
                            if (data.ok == 1) {
                                $('a.{$class}').addClass('active');
                            } else if (data.ok == 3) {
                                $('#user-login').trigger('click');
                            }
                        },
                        complete: function(data) {
                            _executed = 0;
                        }
                    });
                }
                return false;
            ",
            'class'=> ($iIsVoted > 0) ? $class . ' active' : $class,
        ]);
    }

    /**
     * Ajax emotions button/link
     * @param  array $arrEmotionInfo
     * @param  array $arrSetInfo
     * @param  string $objectType
     * @return string
     */
    public static function ajaxEmotions($arrEmotionInfo, $arrSetInfo, $objectType)
    {
        $isChecked = 0;

        if (isset(Helper::user()->id)) {
            $sEmotionKey = sprintf('user:%d:' . $arrEmotionInfo['keyfield'] . ':' . $objectType . 's', Helper::user()->id);

            $iIsCheck = RestHelper::getItems('redis/check', [
                'key' => $sEmotionKey,
                'element' => $arrSetInfo['id'],
            ]);

            if (!empty($iIsCheck)) {
                 $isChecked = 1;
            }
        }

        $messageLike = Yii::t('like', 'You liked this '. $objectType);

        return Html::a('<span class="' . $arrEmotionInfo['class'] . '"></span><span id="num' . $arrEmotionInfo['keyfield'] . '">' . $arrSetInfo['setStatistic'][$arrEmotionInfo['keyfield']] . '</span> ', "javascript:;", [
            'onclick' => "
                if (_executed == 0) {
                    _executed = 1;
                    $.ajax({
                        type: 'POST',
                        cache: false,
                        url: '/like/like/emotions',
                        data: {'objectType':'" . $objectType . "','objectId':'" . $arrSetInfo['id'] . "', 'fieldEmotion':'" . $arrEmotionInfo['keyfield'] . "','objectName':'" . $arrSetInfo['name'] . "'},
                        success: function(data) {
                            if (data.ok == 1) {
                                $('#num" . $arrEmotionInfo['keyfield'] . "').html(parseInt($('#num" . $arrEmotionInfo['keyfield'] . "').html())+1);
                                $('a." . $arrEmotionInfo['keyfield'] . "').addClass('active');
                            } else if (data.ok == 3) {
                                $('#user-login').trigger('click');
                            }
                        },
                        complete: function(data) {
                            _executed = 0;
                        }
                    });
                }
                return false;
            ",
            'class' => ($isChecked > 0) ? "set-emo-link active tooltip " . $arrEmotionInfo['keyfield'] : "set-emo-link tooltip " . $arrEmotionInfo['keyfield'],
            'title' => $arrEmotionInfo['name'],
        ]);
    }

    /**
     * Ajax bad report type link
     * @param  string $sTitle
     * @param  string $objectType
     * @param  integer $objectId
     * @param  string $sObjectName
     * @param  array $arrOptions
     * @return string
     */
    public static function ajaxLinkBadReportType($sTitle, $objectType, $objectId, $objectName, $arrOptions = [])
    {
        return Html::a($sTitle, "#report-popup", [
            'onclick' => "
                if (_executed == 0) {
                    _executed = 1;
                    $.ajax({
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        url: '/bad-report/bad-report/view',
                        data: {'objectType':'" . $objectType . "','objectId':'" . $objectId . "','objectName':'" . $objectName . "'},
                        success: function(data) {
                            if (data.message == 'success')
                            {
                                $('#report-popup .body-badreport').html(data.html);
                                $('#" . $arrOptions['id'] . "').fancybox();
                            }
                            else if(data.message == 'fail')
                            {
                                $('#user-login').trigger('click');
                            }
                        },
                        complete: function(data) {
                            _executed = 0;
                        }
                    });
                }
                return false;
            ",
        ]);
    }

    /**
     * Ajax bad report button/link
     * @param  string $sTitle
     * @param  integer $iBadReportId
     * @param  integer $objectId
     * @param  string $sObjectName
     * @param  array $arrOptions
     * @return string
     */
    public static function ajaxBadReport($sTitle, $iBadReportId, $objectId, $objectName, $arrOptions = [])
    {
        return Html::a($sTitle, "javascript:;", [
            'onclick' => "
                if (_executed == 0) {
                    _executed = 1;
                    $.ajax({
                        type: 'POST',
                        cache: false,
                        dataType: 'json',
                        url: '/bad-report/bad-report/add',
                        data: {'brtid':'" . $iBadReportId . "','objectId':'" . $objectId . "','objectName':'" . $objectName . "'},
                        success: function(data) {
                            if (data.message == 'success') {
                                $('.cancel-btn').trigger('click');
                            } else if (data.message == 'fail') {
                                $('#user-login').trigger('click');
                            }
                        },
                        complete: function(data) {
                            _executed = 0;
                        }
                    });
                }
                return false;
            ",
        ]);
    }

    /**
     * Ajax new comment
     * @param  string  $objectType
     * @param  integer $objectId
     * @param  array   $options
     * @param  string  $textArea
     * @param  string  $container
     * @param  string  $title
     * @param  integer $parentID
     * @param  integer $level
     * @return string
     */
    public static function ajaxSendComment($objectType, $objectId, $options = [], $textArea, $container, $title = '', $parentID = 0, $level = 1)
    {
        $params['onclick'] = "
            if ($('#" . $textArea . "').val().trim() == '') return false ;
            if (_executed == 0) {
                _executed = 1;
                $.ajax({
                    type :'POST',
                    cache: false,
                    url: '/comment/comment/add',
                    dataType: 'json',
                    data: {'objectType':'" . $objectType . "','message':$('#" . $textArea . "').val(),'objectId':'" . $objectId . "','parent_id':" . $parentID . ",'level':" . $level . "},
                    success  : function(data) {
                        if (data.isok==3) {
                            $('#user-login').trigger('click');
                        } else if (data.isok == 5){
                            $.ajax({
                                type: 'POST',
                                url: '/user/user/update-profile',
                                dataType: 'json',
                                success: function(data_update){
                                    if (data_update.isok == 3) {
                                        $('#user-login').trigger('click');
                                    } else if(data_update.isok == 1) {
                                        $('#" . $objectType . "_change_profile').html('');
                                        $.fancybox.close();
                                    } else if(data_update.isok == 2) {
                                        $('#" . $objectType . "_change_profile').html(data_update.html);
                                        $('#" . $objectType . "_change_profile_btn').trigger('click');
                                    }
                                }
                            });
                        } else if (data.isok == 1) {
                            $('#" . $textArea . "').val('');
                            $('#" . $container . "').html(data.html+$('#" . $container . "').html());
                        } else if(data.isok == 4) {
                            swal('Warning', data.html);
                        }
                    },
                    complete: function(data) {
                        _executed = 0;
                    }
                });
            }
            return false;
        ";

        if (!empty($options)) {
            foreach ($options as $key => $value) {
                $params[$key] = $value;
            }
        }

        return Html::a($title, "javascript:;", $params);
    }

    /**
     * Ajax view more comment
     * @param  string  $objectType
     * @param  integer $objectId
     * @param  integer $hiddenNumber
     * @param  string  $container
     * @param  string  $title
     * @param  integer $parentID
     * @param  integer $level
     * @return string
     */
    public static function ajaxViewMoreComment($objectType, $objectId, $hiddenNumber, $container, $options = ['class' => 'view-more-comment', 'id' => 'view-more-comment'], $title = '')
    {
        if (empty($title)) {
            $title = '<strong>' . Yii::t('common', 'View more...') . '<i class="fa fa-angle-double-right"></i></strong>';
        }

        $params['onclick'] = "
            if (_executed == 0) {
                _executed = 1;
                $.ajax({
                    type: 'POST',
                    cache: false,
                    url: '/comment/comment/more',
                    dataType: 'json',
                    data: {'objectType':'" . $objectType . "','number':$('" . $hiddenNumber . "').val(),'objectId':'" . $objectId . "'},
                    success: function(data) {
                        if (data.numbercomment > 0) {
                            $('#" . $options['id'] . "').show();
                        } else {
                            $('#" . $options['id'] . "').hide();
                        }
                        $('" . $hiddenNumber . "').val(data.numbercomment);
                        $('#" . $container . "').html($('#" . $container . "').html() + data.html);
                    },
                    complete: function(data) {
                         _executed = 0;
                    }
                });
            }
            return false;
        ";

        if (!empty($options)) {
            foreach ($options as $key => $value) {
                $params[$key] = $value;
            }
        }

        return Html::a($title, "javascript:;", $params);
    }

    /**
     * Ajax view nested comments
     * @param  string  $objectType
     * @param  integer  $objectId
     * @param  integer $parentID
     * @param  integer $level
     * @param  string  $title
     * @param  array   $options
     * @return string
     */
    public static function ajaxViewChildComment($objectType, $objectId, $parentID = 0, $level = 1, $title = '', $options = [])
    {
        $params['onclick'] = "
            if (_executed == 0) {
                _executed = 1;
                if( $(this).parentsUntil('.comment-box').next().hasClass('sub_comment_active')){
                    $(this).parentsUntil('.comment-box').next().removeClass('sub_comment_active');
                    _executed = 0;
                } else {
                    $.ajax({
                        type: 'POST',
                        cache: false,
                        url: '/comment/comment/childcomment',
                        dataType: 'json',
                        data: {'objectType':'" . $objectType . "','parent_id':" . $parentID . ",'objectId':'" . $objectId . "','level':" . $level . "},
                        success: function(data) {
                            $('#comment_parent_id_" . $parentID . "').addClass('sub_comment_active');
                            $('#comment_parent_id_" . $parentID . "').html(data.html);
                        },
                        complete: function(data) {
                             _executed = 0;
                        }
                    });
                }
            }
            return false;
        ";

        if (!empty($options)) {
            foreach ($options as $key => $value) {
                $params[$key] = $value;
            }
        }

        return Html::a($title, "javascript:;", $params);
    }

    /**
     * Ajax delete comment
     * @param  string $objectType
     * @param  integer $objectId
     * @param  integer $authorId
     * @param  string $title
     * @param  array  $options
     * @return string
     */
    public static function ajaxDeleteComment($objectType, $objectId, $authorId, $title = '', $options = [])
    {
        $params['onclick'] = "
            if (_executed == 0) {
                _executed = 1;
                swal({
                    title: '" . Yii::t('common', 'Are you sure?') . "',
                    text: '" . Yii::t('common', 'You will not be able to recover this {0}!', Yii::t('common', '{0}', $objectType)) . "',
                    showCancelButton: true,
                    cancelButtonText: '" . Yii::t('common', 'No') . "',
                    confirmButtonClass: 'btn-danger',
                    confirmButtonText: '" . Yii::t('common', 'Yes') . "',
                    closeOnConfirm: false,
                    closeOnCancel: false
                },
                function(isConfirm) {
                    if (isConfirm) {
                        $.ajax({
                            type :'POST',
                            cache: false,
                            url: '/comment/comment/delete',
                            dataType: 'json',
                            data: {'objectId':'" . $objectId . "','authorId':'" . $authorId . "'},
                            success: function(data) {
                                if (data.isok == 1) {
                                    $('#comment_id_" . $objectId . "').remove();
                                    swal('" . Yii::t('common', 'Done') . "', '" . Yii::t('comment', 'Your comment is deleted.') . "', 'success');
                                } else if (data.isok == 5) {
                                    $.ajax({
                                        type: 'POST',
                                        url: '/user/user/update-profile',
                                        dataType: 'json',
                                        success: function(data_update) {
                                            if (data_update.isok == 3) {
                                                $('#user-login').trigger('click');
                                            } else if(data_update.isok == 1) {
                                                $('#" . $objectType . "_change_profile').html('');
                                                $.fancybox.close();
                                            } else if(data_update.isok == 2) {
                                                $('#" . $objectType . "_change_profile').html(data_update.html);
                                                $('#" . $objectType . "_change_profile_btn').trigger('click');
                                            }
                                        }
                                    });
                                }
                            },
                            complete: function(data) {
                                 _executed = 0;
                            }
                        });
                    }else{
                        _executed = 0;
                        swal('', '" . Yii::t('comment', 'You cancelled successfully.') . "', 'error');
                    }
                });
            }
            return false;
        ";

        if (!empty($options)) {
            foreach ($options as $key => $value) {
                $params[$key] = $value;
            }
        }

        return Html::a($title, "javascript:;", $params);
    }
}
