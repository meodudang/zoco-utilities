<?php

namespace zoco\helpers\frontend;

use Yii;

/**
 * SOCIAL SHARING helper provides helper functions for using in yii2 projects.
 *
 * FB references:
 * ```
 * https://developers.facebook.com/docs/web/share
 * Feed Dialog: https://developers.facebook.com/docs/sharing/reference/feed-dialog/v2.2
 * Share Dialog: https://developers.facebook.com/docs/sharing/reference/share-dialog
 * Share Button: https://developers.facebook.com/docs/plugins/share-button/
 * Best Practice: https://developers.facebook.com/docs/plugins/checklist/
 * ```
 *
 * Google+ references:
 * Share preview snippet: https://developers.google.com/+/web/snippet/article-rendering
 * Share button: https://developers.google.com/+/web/share/
 *
 * Pinterest references:
 * https://developers.pinterest.com/pin_it/
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @deprecated deprecated since version 0.3.0, use ShareThis instead
 * @since 0.1.0
 */
class ShareHelper
{
    /**
     * Generate link to share on facebook
     * @param  string $link link to share
     * @param  string $image image for facebook sharing article
     * @param  string $name name for facebook sharing article
     * @param  string $description description for facebook sharing article
     * @param  string $redirectUrl url will redirect after sharing
     * @return string
     */
    public static function facebook($link, $image, $name, $description, $redirectUrl = null, $caption = 'ZOCO - Mạng xã hội thời trang phong cách')
    {
        return   "https://www.facebook.com/dialog/feed?app_id=" . Helper::params('facebookAppId')
               . "&display=popup&link=". $link . "&picture=" . $image . "&name=" . urlencode($name) . "&caption=" . $caption
               . "&redirect_uri=" . $redirectUrl . "&description=" . (!empty($description) ? urlencode($description) : Helper::params('zocoDescription'));
    }
}
