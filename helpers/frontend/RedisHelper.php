<?php

namespace zoco\helpers\frontend;

use zoco\helpers\common\RedisHelper as BaseRedisHelper;

/**
 * Helper provides REDIS helper functions for FRONTEND & BACKEND.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.5.0
 */
class RedisHelper extends BaseRedisHelper
{
    /**
     * Get models with redis key
     * @param  string  $redisKey
     * @param  string  $apiMethod eg: item/all
     * @param  array  $apiParams condition parameters must not contain in condition
     * @param  integer  $pageSize
     * @param  integer $page
     * @return array
     */
    public static function all($redisKey, $apiMethod, $apiParams, $pageSize, $page = 0)
    {
        $arrIds = RestHelper::getItems('redis/all', [
            'key' => $redisKey,
            'offset' => $page * $pageSize,
            'count' => $pageSize,
        ]);

        if (isset($apiParams['params']['id'])) {
            $arrIds = array_intersect($arrIds, $apiParams['params']['id']);
        }

        $params = [];

        $apiResult = [
            'error' => 0,
            'message' => 'Success',
            'body' => [],
        ];

        if (!empty($arrIds)) {
            // Merge condition parameters
            $params['params'] = ArrayHelper::merge([
                'and',
                ['in', 'id', $arrIds],
            ], $apiParams['params']);
            unset($apiParams['params']);

            // Merge others parameters
            $params = ArrayHelper::merge($params, $apiParams);

            // Call API
            $apiResult = RestHelper::callApi($apiMethod, $params);
        }

        return $apiResult;
    }
}
