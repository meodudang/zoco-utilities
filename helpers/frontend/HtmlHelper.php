<?php

namespace zoco\helpers\frontend;

use zoco\helpers\common\HtmlHelper as BaseHtmlHelper;

/**
 * Helper provides IMAGE helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.5.0
 */
class HtmlHelper extends BaseHtmlHelper
{

}
