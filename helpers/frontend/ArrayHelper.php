<?php

namespace zoco\helpers\frontend;

use Yii;
use zoco\helpers\common\ArrayHelper as BaseArrayHelper;

/**
 * Helper provides helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.5.0
 */
class ArrayHelper extends BaseArrayHelper
{

}
