<?php

namespace zoco\helpers\id;

use Yii;
use zoco\helpers\common\RestHelper as BaseRestHelper;

/**
 * REST API helper provides helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.5.0
 */
class RestHelper extends BaseRestHelper
{

}
