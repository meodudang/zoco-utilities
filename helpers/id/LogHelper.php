<?php

namespace zoco\helpers\id;

use zoco\helpers\common\LogHelper as BaseLogHelper;

/**
 * Helper provides LOG helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.5.0
 */
class LogHelper extends BaseLogHelper
{

}
