<?php

namespace zoco\helpers\id;

use Yii;
use yii\imagine\Image;
use yii\helpers\VarDumper;
use yii\helpers\FileHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use zoco\helpers\common\Helper as BaseHelper;
use app\models\ProfileZoco;
use app\modules\user\models\Profile;

/**
 * Helper provides helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.5.0
 */
class Helper extends BaseHelper
{
    /**
     * Update ZOCO profile
     * @param  integer $iUserId
     * @param  string  $sNickname
     * @return boolean
     */
    public static function updateProfileZoco($iUserId = 0, $sNickname = '')
    {
        $result = false;

        if ($iUserId > 0) {
            $profileZoco = ProfileZoco::find()->where(['user_id' => $iUserId])->one();

            if (!isset($profileZoco)) {
                $profile = Profile::find()->where(['user_id' => $iUserId])->one();
                $profileZoco = new ProfileZoco;
                $profileZoco->user_id = $profile['user_id'];
                $profileZoco->nickname = empty($sNickname)
                                         ? $profile['last_name'] . ' ' . $profile['first_name']
                                         : $sNickname;

                if ($profileZoco->save()) {
                    $result = true;
                } else {
                    $message='';
                    $errors = $profileZoco->getFirstErrors();
                    foreach ($errors as $key => $value) {
                        $message = $value;
                    }
                    throw new \RuntimeException($message);
                }
            }
        }

        return $result;
    }
}
