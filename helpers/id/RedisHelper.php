<?php

namespace zoco\helpers\id;

use zoco\helpers\common\RedisHelper as BaseRedisHelper;

/**
 * Helper provides REDIS helper functions for FRONTEND & BACKEND.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.5.0
 */
class RedisHelper extends BaseRedisHelper
{

}
