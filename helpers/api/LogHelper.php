<?php

namespace zoco\helpers\api;

use Yii;
use yii\helpers\FileHelper;
use zoco\helpers\common\LogHelper as BaseLogHelper;

/**
 * Helper provides LOG helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.5.0
 */
class LogHelper extends BaseLogHelper
{
    /**
     * Write log
     * @param  string $filename log file name
     * @param  array $data array of data to write into log file
     * @return boolean
     */
    public static function writeLog($filename = '', $data = [])
    {
        if (!isset($filename) || !isset($data)) {
            return null;
        }

        if (!is_array($data)) {
            return null;
        }

        $dirlog = Yii::$app->basePath . Helper::params('dirlog');

        if (!is_dir($dirlog)) {
            FileHelper::createDirectory($dirlog);
        }

        $logfile = fopen($dirlog . '/' . $filename . '_' . date("Y_m") . '.txt', "a+");
        $message = date("Y-m-d H:i:s") . "\t" . implode("\t", array_values($data));
        fwrite($logfile, $message . "\n");
        fclose($logfile);
        return true;
    }
}
