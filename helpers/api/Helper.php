<?php

namespace zoco\helpers\api;

use Yii;
use zoco\helpers\common\Helper as BaseHelper;
use app\models\TagObject;
use app\models\Tag;

/**
 * Helper provides helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.5.0
 */
class Helper extends BaseHelper
{
	/**
     * Set tags to an item
     * @param string  $sTags
     * @param integer $iItemId
     * @return boolean
     */
    public static function setTagItem($sTags = '', $iItemId = 0)
    {
        if (empty($sTags) || $iItemId == 0) {
            return false;
        }

        $arrTags = explode(',', $sTags);

        TagObject::deleteAll([
        	'object_id' => $iItemId,
        	'object_type' => 'item',
    	]);

        foreach ($arrTags as $tag) {
            Tag::add([
            	'name' => $tag,
            	'itemId' => $iItemId,
        	]);
        }

        return true;
    }

    /**
     * Set tags to a set
     * @param integer $iSetId
     * @return boolean
     */
    public static function setTagSet($iSetId = 0)
    {
        if ($iSetId == 0) {
            return false;
        }

        $arrSetInfo = RestHelper::getItems('set/one', [
            'params' => ['id' => $iSetId],
        ]);

        if (!empty($arrSetInfo) && !empty($arrSetInfo['item_ids'])) {
            $arrItemIds = explode(',', $arrSetInfo['item_ids']);
            $arrTags = [];

            foreach ($arrItemIds as $item) {
                $sRedisKey = sprintf(Helper::params('redisKeys')['itemtags'], $item);
                $arrItemTags = RestHelper::getItems('redis/all', [
                    'key' => $sRedisKey,
                ]);

                if (!empty($arrItemTags)) {
                    foreach ($arrItemTags as $sItemTag) {
                        $sTagSetKey = sprintf(Helper::params('redisKeys')['tagsets'], $sItemTag);

                        RestHelper::getItems('redis/add', [
                            'key' => $sTagSetKey,
                            'element' => $iSetId,
                        ]);

                        $arrTags[] = $sItemTag;
                    }
                }
            }

            if (!empty($arrTags)) {
                $sSetTagKey = sprintf(Helper::params('redisKeys')['settags'], $iSetId);

                RestHelper::getItems('redis/add', [
                    'key' => $sSetTagKey,
                    'element' => $arrTags,
                ]);

                return true;
            }
        }

        return false;
    }

    /**
     * Set tags to a collection
     * @param integer $iCollectionId
     * @return boolean
     */
    public static function setTagCollection($iCollectionId = 0)
    {
        if ($iCollectionId == 0) {
            return false;
        }

        $arrCollectionInfo = RestHelper::getItems('collection/one', [
            'params' => ['id' => $iCollectionId],
        ]);
        $arrTags = [];

        if (!empty($arrCollectionInfo)) {
            if (!empty($arrCollectionInfo['item_ids'])) {
                $arrItemIds = explode(',', $arrCollectionInfo['item_ids']);

                foreach ($arrItemIds as $iItemId) {
                    $sRedisKey = sprintf(Helper::params('redisKeys')['itemtags'], $iItemId);
                    $arrItemTags = RestHelper::getItems('redis/all', [
                        'key' => $sRedisKey,
                    ]);

                    if (!empty($arrItemTags)) {
                        foreach ($arrItemTags as $itemTag) {
                            $sCollectionTagsKey = sprintf(Helper::params('redisKeys')['tagcollections'], $itemTag);

                            RestHelper::getItems('redis/add', [
                                'key' => $sCollectionTagsKey,
                                'element' => $iCollectionId,
                            ]);

                            $arrTags[] = $itemTag;
                        }
                    }
                }
            }

            if (!empty($arrCollectionInfo['set_ids'])) {
                $arrSetIds = explode(',', $arrCollectionInfo['set_ids']);

                foreach ($arrSetIds as $iSetId) {
                    $sRedisKey = sprintf(Helper::params('redisKeys')['settags'], $iSetId);
                    $arrSetTags = RestHelper::getItems('redis/all', [
                        'key' => $sRedisKey,
                    ]);

                    if (!empty($arrSetTags)) {
                        foreach ($arrSetTags as $setTag) {
                            $sTagCollectionsKey = sprintf(Helper::params('redisKeys')['tagcollections'], $setTag);

                            RestHelper::getItems('redis/add', [
                                'key' => $sTagCollectionsKey,
                                'element' => $iCollectionId,
                            ]);

                            $arrTags[] = $setTag;
                        }
                    }
                }
            }

            if (!empty($arrTags)) {
                $sCollectionTagsKey = sprintf(Helper::params('redisKeys')['collectiontags'], $iCollectionId);

                RestHelper::getItems('redis/add', [
                    'key' => $sCollectionTagsKey,
                    'element' => $arrTags,
                ]);

                return true;
            }
        }

        return false;
    }
}
