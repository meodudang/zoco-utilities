<?php

namespace zoco\helpers\api;

use Yii;
use yii\helpers\ArrayHelper;

/**
 * Helper provides REDIS helper functions for API & ID.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.4.0
 */
class RedisHelper
{
    /**
     * Get all elements of a redis key
     * @param  array $params [key, to, from[, 'wishcore'][, 'limit'][, offset, count]]
     * @return array
     */
    public static function all($params)
    {
        return Yii::$app->get('redis')->executeCommand('ZREVRANGEBYSCORE', $params);
    }

    /**
     * Number of elements of a redis key
     * @param  array $params [key, to, from]
     * @return int
     */
    public static function count($params)
    {
        return Yii::$app->get('redis')->executeCommand('ZCOUNT', $params);
    }

    /**
     * Add elements into key
     * @param array $params [key, wishcore1, element1, wishcore2, element2, ...]
     * @return array
     */
    public static function add($params)
    {
        $error   = 2;
        $message = 'The requested page invalid';
        $body    = '';

        try {
            $result = Yii::$app->get('redis')->executeCommand('ZADD', $params);

            if ($result > 0 ) {
                $error   = 0;
                $message = 'Add success';
            } else {
                $error   = 1;
                $message = 'Add unsuccess';
            }

            $body = $result;
        } catch (Exception $e) {
            $error   = 3;
            $message = $e->getMessage();
        }

        return [$error, $message, $body];
    }

    /**
     * Delete elements of a redis key
     * @param  array $params [key,element1,element2...]
     * @return array
     */
    public static function delete($params)
    {
        $error   = 2;
        $message = 'The requested page invalid';
        $body    = '';

        try {
            $result = Yii::$app->get('redis')->executeCommand('ZREM', $params);

            if ($result > 0 ) {
                $error   = 0;
                $message = 'Delete success';
            } else {
                $error   = 1;
                $message = 'Delete unsuccess';
            }

            $body = $result;
        } catch (Exception $e) {
            $error   = 3;
            $message = $e->getMessage();
        }

        return [$error, $message, $body];
    }

    /**
     * Delete a redis key
     * @param  array $params
     * @return integer
     */
    public static function deleteKey($params)
    {
        return Yii::$app->get('redis')->executeCommand('DEL', $params);
    }

    /**
     * Check if an element exists in a redis key
     * @param  array $params [key, element]
     * @return int
     */
    public static function check($params)
    {
        $result= Yii::$app->get('redis')->executeCommand('ZREVRANK', $params);

        if ($result !== null) {
            $result = 1;
        } else {
            $result = 0;
        }

        return $result;
	}
}
