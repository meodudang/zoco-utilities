<?php

namespace zoco\helpers\common;

use Yii;
use yii\helpers\Url;
use zoco\widgets\LinkPager;

/**
 * HTML helper provides helper functions for using in yii2 projects (with REST API).
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.1.0
 */
class HtmlHelper
{
    /**
     * Generate list for dropdown
     * @param  string $apiPrefix
     * @param  string $key
     * @param  string $value
     * @param  array $param
     * @return array
     */
    public static function listDropDown($apiPrefix, $key = 'id', $value = 'name', $param = [])
    {
        $result = [];

        if (!empty($apiPrefix)) {
            $params = [];
            $params['select'] = [$key, $value];

            if (!empty($param)) {
                $params['params'] = $param;
            }

            if ($key != 'id') {
                $params['notIndex'] = true;
            }

            $resultApi = RestHelper::callApi($apiPrefix . '/all', $params);

            if (isset($resultApi['body'])) {
                $result = ArrayHelper::map($resultApi['body'], $key, $value);
            }
        }

        return $result;
    }

    /**
     * Generate list for dropdown for nested set model
     * @param  string $apiPrefix
     * @param  string $key
     * @param  string $value
     * @param  array $param
     * @param  string $level
     * @param  string $lft
     * @return array
     */
    public static function listDropDownNest($apiPrefix, $key = 'id', $value = 'name', $param = [], $level = 'level', $lft = 'left')
    {
        $result = [];

        if (isset($apiPrefix)  && !empty($apiPrefix)) {
            $params = [];
            $params['select'] = [$key, $value, $level, $lft];
            $params['orderby'] = 'root, ' . $lft;

            if (!empty($param)) {
                $params['params'] = $param;
            }

            $resultApi = RestHelper::callApi($apiPrefix . '/all', $params);

            if (isset($resultApi['body'])) {
                $objects = $resultApi['body'];
                $nlevel = 1;

                foreach ($objects as $object) {
                    $n = $object[$level] - $nlevel;

                    if ($n > 0) {
                        $object[$value] = ' ' . $object[$value];
                        for ($i = $n; $i > 0; $i--) {
                            $object[$value] = '→' . $object[$value];
                        }
                    }

                    $result[$object[$key]] = $object[$value];
                }
            }
        }

        return $result;
    }

    /**
     * Generate list for dropdown for genders
     * @return array
     */
    public static function listGenderDropDown()
    {
        return [
            '0' => Yii::t('common', 'None'),
            '1' => Yii::t('common', 'Man'),
            '2' => Yii::t('common', 'Woman'),
            '4' => Yii::t('common', 'Kids'),
            '8' => Yii::t('common', 'Unisex'),
        ];
    }

    /**
     * Generate pagination div
     * @param Object $pages pagination object
     * @return array
     */
    public static function createPagination($pages)
    {
        return LinkPager::widget([
            'pagination'         => $pages,
            'maxButtonCount'     => 5,
            'activePageCssClass' => 'active',
            'firstPageLabel'     => false,
            'lastPageLabel'      => false,
            'prevPageLabel'      => '<span aria-hidden="true">&laquo;</span>',
            'nextPageLabel'      => '<span aria-hidden="true">&raquo;</span>',
            'prevPageCssClass'   => '',
            'nextPageCssClass'   => '',
        ]);
    }

    public static function createItem($object, $options = [])
    {
    }

    public static function createSet($object, $options = [])
    {
    }

    public static function createCollection($object, $options = [])
    {
    }
}
