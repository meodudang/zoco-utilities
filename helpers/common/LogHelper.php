<?php

namespace zoco\helpers\common;

use DOMDocument;
use Yii;
use yii\helpers\Url;
use yii\helpers\FileHelper;
use filsh\yii2\gearman\JobWorkload;

/**
 * Helper provides LOG helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @todo LOG MODULE IN API
 * @since 1.0
 */
class LogHelper
{
    /**
     * Write log with job
     * @param  string $sLogFileName
     * @param  array $arrDataLogging
     * @return void
     */
    public static function exWriteLog($sLogFileName, $arrDataLogging)
    {
        if (!empty($sLogFileName) && !empty($arrDataLogging)) {
            if (Helper::params('isjob') == 1) {
                $data = [
                    'filelog' => $sLogFileName,
                    'datalog' => $arrDataLogging,
                ];

                Yii::$app->gearman->getDispatcher()->background('writeLogJob', new JobWorkload([
                    'params' => [
                        'data' => $data,
                    ],
                ]));
            } else {
                static::writeLog($sLogFileName, $arrDataLogging);
            }
        }
    }

    /**
     * Write log
     * @param  string $sLogFileName log file name
     * @param  array $arrDataLogging array of data to write into log file
     * @return boolean
     */
    // if action login, register => data = ['userid', 'nickname', 'action', 'ip']
    // else data = ['userid', 'nickname', 'action', 'ip', 'objectType', 'objectID', 'objectName']
    public static function writeLog($sLogFileName = '', $arrDataLogging = [])
    {
        if (!isset($sLogFileName) || !isset($arrDataLogging)) {
            return null;
        }

        if (!is_array($arrDataLogging)) {
            return null;
        }

        $dirlog = Yii::$app->basePath . Helper::params('dirlog');

        if (!is_dir($dirlog)) {
            FileHelper::createDirectory($dirlog);
        }

        $logfile = fopen($dirlog . '/' . $sLogFileName . '_' . date("Y_m") . '.txt', "a+");
        $message = date("Y-m-d H:i:s") . "\t" .Helper::getClientIp(). "\t" .implode("\t", array_values($arrDataLogging));
        fwrite($logfile, $message . "\n");
        fclose($logfile);
        return true;
    }

    public static function writeXmlSiteMap($objectType, $datas, $route, $lang)
    {
        $isok = 0;

        $filename = $objectType . '_' . $lang . '.xml';
        $filePath = Yii::$app->basePath . '/web/' . $filename;

        $sitemap = new DOMDocument();
        $sitemap->formatOutput = true;
        $sitemap->preserveWhiteSpace = false;

        if (!file_exists($filePath)) {
            self::createXmlFile($sitemap);
            self::insertSiteMapIndexFile($filename);
        } else {
            $sitemap->load($filePath);
        }

        $params=[$route];
        if (!empty($datas)) {
            foreach ($datas as $data) {
                $params['id'] = !empty($data['object_id']) ? $data['object_id'] : (!empty($data['id']) ? $data['id'] : '');
                $params['slug'] = !empty($data['object_slug']) ? $data['object_slug'] : $data['slug'];
                if ((($data['status']==1) && ($objectType === 'collection' || $objectType === 'set')) || ($objectType === 'item' && ($data['status']==1 || $data['status']==2))) {
                    self::createNode($sitemap, $objectType, $params, $lang);
                } else {
                    self::removeNode($sitemap, $objectType, $params, $lang);
                }
            }
        }

        $sitemap->save($filePath);

        return $isok;
    }

    private static function removeNode($sitemap, $objectType, $params, $lang)
    {
        $nodeId = $objectType.'-'.$params['id'].'-'.$lang;
        $oldnode = $sitemap->getElementById($nodeId);
        if (!empty($oldnode)) {
            $sitemap->documentElement->removeChild($oldnode);
        }
    }

    private static function createNode($sitemap, $objectType, $params, $lang)
    {
        $nodeId = $objectType.'-'.$params['id'].'-'.$lang;

        $newnode = $sitemap->createElement('url');
        $attrId = $sitemap->createAttribute('xml:id');
        $attrId->value = $nodeId;
        $newnode->appendChild($attrId);

        $params['language'] = $lang;

        $locnode = $sitemap->createElement('loc');
        $textnode = $sitemap->createTextNode(Url::to($params, true)) ;
        $locnode->appendChild($textnode);
        $newnode->appendChild($locnode);

        $lastmodnode = $sitemap->createElement('lastmod');
        $textnode = $sitemap->createTextNode(date('Y-m-d')) ;
        $lastmodnode->appendChild($textnode);
        $newnode->appendChild($lastmodnode);

        $prioritynode = $sitemap->createElement('priority');
        $textnode = $sitemap->createTextNode(0.8) ;
        $prioritynode->appendChild($textnode);
        $newnode->appendChild($prioritynode);

        $oldnode = $sitemap->getElementById($nodeId);
        if (!empty($oldnode)) {
            $sitemap->documentElement->replaceChild($newnode, $oldnode);
        } else {
            $sitemap->documentElement->appendChild($newnode);
        }
    }

    private static function createXmlFile($sitemap)
    {
        $root = $sitemap->createElement("urlset");
        $sitemap->appendChild($root);
        $attr_xmlns = $sitemap->createAttribute('xmlns');
        $root->appendChild($attr_xmlns);
        $attr_xmlns_text = $sitemap->createTextNode('http://www.sitemaps.org/schemas/sitemap/0.9');
        $attr_xmlns->appendChild($attr_xmlns_text);
    }

    private static function insertSiteMapIndexFile($filename)
    {
        $indexFile = Yii::$app->basePath.'/web/sitemap_index.xml';

        $sitemapIndex = new DOMDocument();
        $sitemapIndex->formatOutput = true;
        $sitemapIndex->preserveWhiteSpace = false;
        if (!file_exists($indexFile)) {
            $root = $sitemapIndex->createElement("sitemapindex");
            $sitemapIndex->appendChild($root);
            $attr_xmlns = $sitemapIndex->createAttribute('xmlns');
            $root->appendChild($attr_xmlns);
            $attr_xmlns_text = $sitemapIndex->createTextNode('http://www.sitemaps.org/schemas/sitemap/0.9');
            $attr_xmlns->appendChild($attr_xmlns_text);
        } else {
            $sitemapIndex->load($indexFile);
        }

        $newnode = $sitemapIndex->createElement('sitemap');
        $locnode = $sitemapIndex->createElement('loc');
        $textnode = $sitemapIndex->createTextNode(Yii::$app->params['frontUrl'].$filename) ;
        $locnode->appendChild($textnode);
        $newnode->appendChild($locnode);

        $lastmodnode = $sitemapIndex->createElement('lastmod');
        $textnode = $sitemapIndex->createTextNode(date('Y-m-d')) ;
        $lastmodnode->appendChild($textnode);
        $newnode->appendChild($lastmodnode);

        $sitemapIndex->documentElement->appendChild($newnode);
        $sitemapIndex->save($indexFile);
    }
}
