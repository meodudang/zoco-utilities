<?php

namespace zoco\helpers\common;

use Yii;
use yii\helpers\ArrayHelper as BaseArrayHelper;

/**
 * Helper provides helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.1.0
 */
class ArrayHelper extends BaseArrayHelper
{
    /**
     * Check if wheter an value is in array
     * @param  string|integer $value
     * @param  array $array
     * @return boolean
     */
    public static function inArray($value, $array)
    {
        // Normalize array
        $string = ';' . implode(';', $array) . ';';

        // Searching and return the result
        if (strpos($string, ';' . $value . ';') === false) {
            return false;
        }

        return true;
    }

    /**
     * Get values of elements with specific key from an array
     * @param  array  $objects an array to get data
     * @param  string $key     key to get value (e.g. elements' attribute)
     * @return array
     */
    public static function getArray($objects = [], $key = 'id')
    {
        $temp = [];
        if (!empty($objects)) {
            foreach ($objects as $ob) {
                $temp[] = $ob[$key];
            }
        }

        return $temp;
    }

    /**
     * Get values of elements with specific key and dates from an array
     * @param  array  $objects   an array to get data
     * @param  array  $dates     date to get value
     * @param  string $key       key to get value (e.g. elements' attribute)
     * @param  string $dateField name of date field
     * @return array
     */
    public static function getArrayByDate($objects = [], $dates = [], $key = 'id', $dateField = 'date')
    {
        $temp = [];
        if (!empty($dates)) {
            foreach ($dates as $date) {
                $value = 0;
                if (!empty($objects)) {
                    foreach ($objects as $ob) {
                        if (!empty($ob[$dateField]) && $ob[$dateField] === $date) {
                            if (!empty($ob[$key])) {
                                $value = $ob[$key];
                                break;
                            }
                        }
                    }
                }
                $temp[] = $value;
            }
        }

        return $temp;
    }

    /**
     * Filter items and background|border items...
     * @param  array  $objectItems
     * @return array
     */
    public static function getClassify($objectItems = [])
    {
        $items = [];
        $backgrounds = [];
        if (!empty($objectItems)) {
            foreach ($objectItems as $obItem) {
                $isItem = true;
                if (isset($obItem['categories'])) {
                    $keys = array_keys($obItem['categories']);
                    foreach ($keys as $key) {
                        if (self::inArray($key, Helper::params('notShowCategoryIds'))) {
                            $isItem = false;
                            break;
                        }
                    }

                    if ($isItem) {
                        $items[] = $obItem;
                    } else {
                        $backgrounds[] = $obItem;
                    }
                }
            }
        }
        return [$items, $backgrounds];
    }
}
