<?php

namespace zoco\helpers\common;

use Yii;
use yii\imagine\Image;
use yii\helpers\FileHelper;
use Imagine\Image\ManipulatorInterface;
use yii\helpers\Html;
use yii\helpers\Url;

/**
 * Helper provides IMAGE helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.1.0
 */
class ImageHelper
{
    /**
     * Resize image
     * @param  string  $sfile  file name
     * @param  integer $width
     * @param  integer $height
     * @param  array   $htmlOptions
     * @param  array   $options
     * @param  boolean $includeTag return with img tag or not
     * @return string
     */
    public static function imageResize($sfile = null, $width = 50, $height = 50, $htmlOptions = [], $options = ['quality' => 100], $includeTag = true)
    {
        // check remote file is existed
        if (empty($sfile) || !@GetImageSize($sfile)) {
            $sfile = Yii::getAlias('@webroot') . '/basic/img/placeholder.jpg';
        }

        $dirname = dirname($sfile);
        $subdir = substr($dirname, strrpos($dirname, '/'));
        $filename = basename($sfile);

        $pathThumb = Yii::$app->params['pathUpload'] . Yii::$app->params['dirthumb'] . $subdir . '/';

        if (!is_dir($pathThumb)) {
            FileHelper::createDirectory($pathThumb);
        }

        $filename = $width . '_' . $height . '_' . $filename;
        if (!file_exists($pathThumb . $filename)) {
            Image::thumbnail($sfile, $width, $height, ManipulatorInterface::THUMBNAIL_INSET)->save($pathThumb . $filename, $options);
        }

        if ($includeTag) {
            return Html::img('/' . Yii::$app->params['dirthumb'] . $subdir . '/' . $filename, $htmlOptions);
        }

        return '/' . Yii::$app->params['dirthumb'] . $subdir . '/' . $filename;

        // return HolderJS
        // return Html::img(null, ArrayHelper::merge($htmlOptions, ['data-src' => str_replace(['http://', 'https://'], '', Url::base(true)) . '/' . $width . 'x' . $height . '/zoco/text:' . Yii::$app->name]));

        // return placeholder
        if ($includeTag) {
            return Html::img(Url::base(true) . '/basic/img/placeholder.jpg', ArrayHelper::merge($htmlOptions, ['alt' => Yii::$app->name]));
        }

        return Url::base(true) . '/basic/img/placeholder.jpg';
    }

    /**
     * Crop image
     * @param  string  $sfile  file name
     * @param  integer $width
     * @param  integer $height
     * @param  array   $htmlOptions
     * @param  array   $start the starting point, this must be an array with 2 elements representing x and y coordinates
     * @param  boolean $includeTag return with img tag or not
     * @return string
     */
    public static function imageCrop($sfile = null, $width = 50, $height = 50, $htmlOptions = [], $start = [0, 0], $includeTag = true)
    {
        // check remote file is existed
        if (!empty($sfile) && @GetImageSize($sfile)) {
            $dirname = dirname($sfile);
            $subdir = substr($dirname, strrpos($dirname, '/'));
            $filename = basename($sfile);
            $pathThumb = Yii::$app->params['pathUpload'] . Yii::$app->params['dirthumb'] . $subdir . '/';

            if (!is_dir($pathThumb)) {
                FileHelper::createDirectory($pathThumb);
            }

            $filename = 'crop_' . $width . '_' . $height . '_' . $filename;
            if (!file_exists($pathThumb . $filename)) {
                Image::crop($sfile, $width, $height, $start)->save($pathThumb . $filename);
            }

            if ($includeTag) {
                return Html::img('/' . Yii::$app->params['dirthumb'] . $subdir . '/' . $filename, $htmlOptions);
            }

            return $filename;
        }

        // return HolderJS
        // return Html::img(null, ArrayHelper::merge($htmlOptions, ['data-src' => str_replace(['http://', 'https://'], '', Url::base(true)) . '/' . $width . 'x' . $height . '/zoco/text:' . Yii::$app->name]));

        // return placeholder
        if ($includeTag) {
            return Html::img(Url::base(true) . '/basic/img/placeholder.jpg', ArrayHelper::merge($htmlOptions, ['alt' => Yii::$app->name]));
        }

        return Url::base(true) . '/basic/img/placeholder.jpg';
    }

    /**
     * Get thumbnail url of an image
     * @param  string $image image name
     * @param  string $size  small|medium|large
     * @param  string $dir name of directory where store images
     * @return string
     */
    public static function thumbnail($image, $size, $dir)
    {
        if (!empty($size)) {
            $configs = Helper::params('thumbnails')[$size];
            $url = Helper::params('cdn') . $dir . '/' . $configs['dir'] . '/' . $image;
        } else {
            $url = Helper::params('cdn') . $dir . '/' . $image;
        }

        if (@GetImageSize($url)) {
            return $url;
        }

        // @TODO create thumbnail in CDN
        return self::imageHolder($dir);
    }

    /**
     * Return image holder
     * @param  string $dir upload dir
     * @return string
     */
    public static function imageHolder($dir = '')
    {
        $imgPath = '/basic/img/placeholder.jpg';

        if (!empty($dir)) {
            if ($dir == 'users') {
                $imgPath = '/basic/img/placeholder-boy.jpg';
            }
        }

        return $imgPath;
    }
}
