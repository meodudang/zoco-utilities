<?php

namespace zoco\helpers\common;

use Yii;
use yii\imagine\Image;
use yii\helpers\VarDumper;
use yii\helpers\FileHelper;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\helpers\Json;

/**
 * Helper provides helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.1.0
 */
class Helper
{
    const GENDER_NONE = 0;
    const GENDER_MALE = 1;
    const GENDER_FEMALE = 2;

    /**
     * Retrieves the id (name) of the current controller
     *
     * @return string name of current controller
     */
    public static function currentControllerId()
    {
        return Yii::$app->controller->id;
    }

    /**
     * Retrieves the id (name) of the current action
     *
     * @return string name of current action
     */
    public static function currentActionId()
    {
        return Yii::$app->controller->action->id;
    }

    /**
     * Retrieves current user object
     *
     * @return \yii\base\User
     */
    public static function user()
    {
        return Yii::$app->getUser();
    }

    /**
     * Retrieves the value of all or an application parameter(s)
     *
     * @param  string $name name of parameter
     * @return mixed the value of the parameter(s)
     */
    public static function params($name = '')
    {
        if (!empty($name)) {
            return Yii::$app->params[$name];
        }

        return Yii::$app->params;
    }

    /**
     * Sets a flash message.
     *
     * @param string $key the key identifying the flash message.
     * @param mixed $message flash message
     */
    public static function setFlash($key, $message, $removeAfterAccess = true)
    {
        Yii::$app->getSession()->setFlash($key, $message, $removeAfterAccess);
    }

    /**
     * Get CSS class for a html tag
     * @param  mixed $params if it's a string, will get from application params. If it's an array, will use it instead
     * @param  string $controllerId
     * @param  string $actionId
     * @return array
     */
    public static function getCssClass($params, $controllerId, $actionId = null)
    {
        if (!is_array($params)) {
            $params = static::params($params);
        }

        if (!empty($actionId)) {
            return    isset($params[$controllerId][$actionId])
                    ? $params[$controllerId][$actionId]
                    : (isset($params[$controllerId]['*']) ? $params[$controllerId]['*'] : ['id' => 'new-index']);
        }

        return isset($params[$controllerId]['*']) ? $params[$controllerId]['*'] : ['id' => 'new-index'];
    }

    /**
     * Get user ID
     * @return integer|null
     */
    public static function getUserId()
    {
        if (isset(Yii::$app->user->identity) && !empty(Yii::$app->user->identity->id)) {
            return Yii::$app->user->identity->id;
        }

        return null;
    }

    /**
     * Dump as many variables as you want. Infinite parameters.
     *
     * @param  mixed $x
     * @return void
     */
    public static function dump()
    {
        array_map(function ($x) {
            var_dump($x);
        }, func_get_args());
        exit;
    }

    /**
     * Dump as many variables as you want. Infinite parameters.
     *
     * @param  mixed $args
     * @return void
     */
    public static function dd()
    {
        $args = func_get_args();
        foreach ($args as $k => $arg) {
            echo '<fieldset class="debug">
            <legend>' . ($k+1) . '</legend>';
            VarDumper::dump($arg, 10, true);
            echo '</fieldset>';
        }
        die;
    }

    /**
     * Convert UTF-8 to ASCII
     * @param  string $string utf-8string to convert
     * @return string converted string
     */
    public static function utf2ascii($string)
    {
        $str = str_replace('-', ' ', $string);
        $trans = array(
                "đ"=>"d","Đ"=>"D",
                "â"=>"a","ấ"=>"a","ầ"=>"a","ẩ"=>"a","ẫ"=>"a","ậ"=>"a",
                "á"=>"a","à"=>"a","ả"=>"a","ạ"=>"a","ã"=>"a",
                "ă"=>"a","ắ"=>"a","ằ"=>"a","ẳ"=>"a","ẵ"=>"a","ặ"=>"a",
                "é"=>"e","è"=>"e","ẻ"=>"e","ẽ"=>"e","ẹ"=>"e",
                "ê"=>"e","ế"=>"e","ề"=>"e","ể"=>"e","ễ"=>"e","ệ"=>'e',
                "í"=>"i","ì"=>"i","ỉ"=>"i","ĩ"=>"i","ị"=>"i",
                "ú"=>"u","ù"=>"u","ủ"=>"u","ũ"=>"u","ụ"=>"u",
                "ư"=>"u","ử"=>"u","ừ"=>"u","ứ"=>"u","ữ"=>"u","ự"=>"u",
                "ó"=>"o","ò"=>"o","ỏ"=>"o","õ"=>"o","ọ"=>"o",
                "ô"=>"o","ố"=>"o","ồ"=>"o","ổ"=>"o","ỗ"=>"o","ộ"=>"o",
                "ơ"=>"o","ớ"=>"o","ờ"=>"o","ở"=>"o","ỡ"=>"o","ợ"=>"o",
                "ý"=>"y","ỳ"=>"y","ỷ"=>"y","ỹ"=>"y","ỵ"=>"y",
                "Á"=>"A","À"=>"A","Ả"=>"A","Ã"=>"A","Ạ"=>"A",
                "Ă"=>"A","Ắ"=>"A","Ằ"=>"A","Ẳ"=>"A","Ẵ"=>"A","Ặ"=>"A",
                'Â'=>'A','Ấ'=>'A','Ầ'=>'A','Ẩ'=>'A','Ẫ'=>'A','Ậ'=>'A',
                'É'=>'E','È'=>'E','Ẻ'=>'E','Ẽ'=>'E',"Ẹ"=>"E",
                "Ê"=>"E","Ế"=>"E","Ề"=>"E","Ể"=>"E","Ễ"=>"E","Ệ"=>"E",
                "Ý"=>"Y","Ỳ"=>"Y","Ỷ"=>"Y","Ỹ"=>"Y","Ỵ"=>"Y",
                "Ú"=>"U","Ù"=>"U","Ủ"=>"U","Ũ"=>"U","Ụ"=>"U",
                "Ư"=>"U","Ứ"=>"U","Ừ"=>"U","Ử"=>"U","Ữ"=>"U","Ự"=>"U",
                "Í"=>"I","Ì"=>"I","Ỉ"=>"I","Ĩ"=>"I","Ị"=>"I",
                "Ó"=>"O","Ò"=>"O","Ỏ"=>"O","Õ"=>"O","Ọ"=>"O",
                "Ơ"=>"O","Ớ"=>"O","Ờ"=>"O","Ở"=>"O","Ỡ"=>"O","Ợ"=>"O",
                "Ô"=>"O","Ố"=>"O","Ồ"=>"O","Ổ"=>"O","Ỗ"=>"O","Ộ"=>"O",
                "/"=>"-");
        $str = strtr($str, $trans);
        $str = preg_replace(array('/\s+/', '/[^A-Za-z0-9\-]/'), array('-', ''), $str);

        $str = trim(strtolower($str));
        return $str;
    }

    /**
     * Pick one or more random entries out of an array
     * @param  array $array
     * @param  integer $num number of elements
     * @return mixed
     */
    public static function arrayRandom($array, $num = 1)
    {
        $r = [];

        for ($i = 0; $i < $num; $i++) {
            shuffle($array);
            $r[] = $array[0];
        }

        return $num == 1 ? $r[0] : $r;
    }

    /**
     * Get domain from an url
     * @param  string $url
     * @return string
     */
    public static function getDomain($url)
    {
        $domain = $url;

        $parurl = parse_url($url);
        if (count($parurl) > 1) {
            $domain = $parurl['host'];
        }

        return $domain;
    }

    /**
     * Pick one or more random entries out of an array
     * @param  array $array
     * @param  integer $num number of elements
     * @return mixed
     */
    public static function getArrayKeyRandom($array, $num = 1)
    {
        $arrayKey = [];

        if (is_array($array) && !empty($array)) {
            if ($num > count($array)) {
                $num = count($array);
            }

            $arrayKey = array_rand($array, $num);
        }

        return $arrayKey;
    }

    /**
     * Like an object
     * @param  string $sObjectType
     * @param  integer $iItemId
     * @return boolean
     */
    public static function like($sObjectType, $iItemId)
    {
        $iIsOk = 0;

        if (!isset(Helper::user()->id)) {
            $iIsOk = 3;
            return Json::encode(['objecttype' => $sObjectType, 'ok' => $iIsOk]);
        }

        if (!empty($sObjectType) && $iItemId > 0) {
            $sObjectLikeKey = sprintf(Helper::params('redisKeys')[$sObjectType . 'likes'], $iItemId);
            $sUserLikeKey   = sprintf(Helper::params('redisKeys')['userlike' . $sObjectType . 's'], Helper::user()->id);

            $isCheck = RestHelper::getItems('redis/check', [
                'key' => $sUserLikeKey,
                'element' => $iItemId,
            ]);

            if (!empty($isCheck)) {
                $result = RestHelper::addStatistics($sObjectType, [['id' => $iItemId]], 'likes', -1);

                if ($result) {
                    $iIsOk = 2;
                }
            } else {
                $result = RestHelper::addStatistics($sObjectType, [['id' => $iItemId]], 'likes', 1);

                if ($result) {
                    $iIsOk = 1;
                }
            }
        }

        return Json::encode(['objecttype' => $sObjectType, 'ok' => $iIsOk]);
    }

    /**
     * Format price for item/set/collection
     * @param  float $price
     * @param  string $currency
     * @param  boolean $withLabel
     * @param  boolean $returnOnlyPrice
     * @param  string $decimalSymbol
     * @param  string $thousandSymbol
     * @return string
     * @todo decide decimal and thousand symbols automatically by app/user language
     */
    public static function formatPrice($price, $currency = '', $withLabel = false, $returnOnlyPrice = false, $decimalSymbol = ',', $thousandSymbol = '.')
    {
        if (!empty($price) && $price > 0) {
            // Get currency cache
            $arrCurrencyInfo = static::getCurrencyCache();

            if (empty($currency)) {
                $currency = Helper::params('baseCurrency');
            }

            // Convert price
            if (isset($arrCurrencyInfo[$currency])) {
                $price = $arrCurrencyInfo[$currency]['rate'] * $price;

                // Round price
                if (!is_null($arrCurrencyInfo[Helper::params('baseCurrency')]['precision'])) {
                    $price = round($price, $arrCurrencyInfo[Helper::params('baseCurrency')]['precision']);
                }
            }

            // Return only price (without label & currency)
            if ($returnOnlyPrice) {
                return $price;
            }

            return static::setLabelForPrice($price, $currency, $withLabel);
        }

        // return Yii::t('common','OUT OF STOCK');
        return '&nbsp;';
    }

    /**
     * Convert price to string with label
     * @param  integer $price
     * @param  string $currency
     * @param  boolean $withLabel
     * @param  string $decimalSymbol
     * @param  string $thousandSymbol
     * @return string
     */
    public static function setLabelForPrice($price, $currency, $withLabel = false, $decimalSymbol = ',', $thousandSymbol = '.')
    {
        if (!empty($price) && $price > 0) {
            // Get currency cache
            $arrCurrencyInfo = static::getCurrencyCache();

            $sLabel = '';

            if ($withLabel && $currency != Helper::params('baseCurrency')) {
                $sLabel = Yii::t('common', 'Approximate ');
            }

            $sFormat = isset($arrCurrencyInfo[Helper::params('baseCurrency')]['format']) ? $arrCurrencyInfo[Helper::params('baseCurrency')]['format'] : '{symbol} {price}';

            return strtr($sFormat, [
                '{symbol}' => isset($arrCurrencyInfo[Helper::params('baseCurrency')]['symbol']) ? $arrCurrencyInfo[Helper::params('baseCurrency')]['symbol'] : $arrCurrencyInfo[Helper::params('baseCurrency')]['code'],
                '{price}' => $sLabel . number_format($price, 0, $decimalSymbol, $thousandSymbol),
            ]);
        }

        // return Yii::t('common','OUT OF STOCK');
        return '&nbsp;';
    }

    /**
     * Get currency cache
     * @return array
     */
    public static function getCurrencyCache()
    {
        // Get cache
        $arrCurrencyInfo = Yii::$app->cache->get('CurrencyInfo');

        // Cache expired or empty
        if ($arrCurrencyInfo === false) {
            // Get currency info via API
            $currencyApiResult = RestHelper::callApi('currency/all', [
                'params' => ['base' => Helper::params('baseCurrency')],
                'indexBy' => 'code',
            ]);

            if (isset($currencyApiResult['error']) && $currencyApiResult['error'] == 0) {
                // Set cache
                Yii::$app->cache->set('CurrencyInfo', $currencyApiResult['body'], Helper::params('durationCurrencyCache'));

                // Get cache
                $arrCurrencyInfo = Yii::$app->cache->get('CurrencyInfo');
            }
        }

        return $arrCurrencyInfo;
    }

    /**
     * Check foreign items
     * @return boolean
     */
    public static function foreignGoods($url)
    {
        $result = false;
        $domain = Helper::getDomain($url);

        if (strpos($domain, '.com') !== false) {
            $result = true;
        }

        return $result;
    }

    /**
     * Create item sort URL
     * @param string $order
     * @return string
     */
    public static function createUrlSortItem($order)
    {
        $arr = Yii::$app->request->get();

        if (!empty($order)) {
            $arr = array_merge($arr, $order);
        }

        $url = ['/item/' . Helper::currentControllerId() . '/' . Helper::currentActionId()];

        foreach ($arr as $key => $value) {
            if ($key !== 'page' && $key!=='per-page') {
                $url[$key] = $value;
            }
        }

        return $url;
    }

    /**
     * Get info of search result
     * @param integer|null $page
     * @return string
     */
    public static function getInfoResult($page = null)
    {
        if (isset($page)) {
            $totalcount = $page->totalCount;
            $from = ($page->offset) + 1;
            $temp = ($page->limit) * ($page->getPage() + 1);

            $to = 0;
            if ($temp >= $totalcount) {
                $to = $totalcount;
            } else {
                $to = $temp;
            }

            return Yii::t('common', 'DISPLAY {0} - {1} OF {2} RESULTS', [$from, $to, $totalcount]);
        }
    }

    /**
     * Get number of unread announcements
     * @param  integer $userId
     * @return integer
     */
    public static function getNumberUnreadAnnouncement($userId = null)
    {
        if (is_null($userId)) {
            $userId = Helper::user()->id;
        }

        // Get read announcements
        $arrReadAnnouncements = RedisHelper::getReadAnnouncements($userId);
        $iReadAnnouncements = count($arrReadAnnouncements);

        // Get total of announcements
        $params = [
            'params' => [
                'status' => 1,
                'language_id' =>   isset(Yii::$app->getUser()->identity->userProfile['language_id'])
                                 ? Yii::$app->getUser()->identity->userProfile['language_id']
                                 : static::getIdLanguage(), // @todo get application language id,
            ],
        ];
        $iTotalAnnouncements = RestHelper::getItems('announcement/count', $params); // Call API

        if (empty($iTotalAnnouncements)) {
            $iTotalAnnouncements = 0;
        }

        $iNew = $iTotalAnnouncements - $iReadAnnouncements;

        return $iNew ?: 0;
    }

    /**
     * Get current app's language id
     * @return integer
     */
    public static function getIdLanguage()
    {
        // Default is Vietnamese
        $iLanguageId = 2;

        $arrLanguageInfo = RestHelper::getItems('language/one', [
            'params' => ['code' => Yii::$app->language],
        ]);

        if (!empty($arrLanguageInfo)) {
            $iLanguageId = $arrLanguageInfo['id'];
        }

        return $iLanguageId;
    }

    /**
     * Get client IP
     * @return string
     */
    public static function getClientIp()
    {
        $ip = null;

        // If is console request, return localhost IP
        if (Yii::$app->request->isConsoleRequest) {
            return '127.0.0.1';
        }

        if (!empty($_SERVER['HTTP_CLIENT_IP'])) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR'])) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        return $ip;
    }

    /**
     * Encode object's values
     * @param  array $object
     * @return array
     */
    public static function htmlencodes($object)
    {
        // foreach ($object->attributes as $key=>$value) {
        //     if ($key !== 'json_content') {
        //         if (is_string($value)) {
        //             $object->$key = addslashes(htmlspecialchars($value));
        //         }
        //     }
        // }

        return $object;
    }

    /**
     * Get user information by its slug
     * @param  string $slug user profile url
     * @return array
     */
    public static function getUserInfoBySlug($slug = null)
    {
        if (!empty($slug)) {
            $profiles = RestHelper::getItems('profile/all', [
                'params'    => ['profile_url' => $slug],
                'limit'     => 1,
                'with'      => ['followers', 'followings', 'medals', 'cups', 'merchant', 'likes', 'galleries'],
                'notIndex'  => true,
            ]);

            if (!empty($profiles)) {
                return $profiles[0];
            }
        }

        return [];
    }

    /**
     * Get number of followers
     * @param  integer $iUserId
     * @return integer
     */
    public static function getAmountFollower($iUserId = 0)
    {
        $iFollowerCount = 0;

        if ($iUserId > 0) {
            // Get total followers
            $sKeyFollower = sprintf(Helper::params('redisKeys')['userfollowers'], $iUserId);
            $iFollowerCount = RestHelper::getItems('redis/count', [
                'key' => $sKeyFollower,
            ]);

            if (empty($iFollowerCount)) {
                $iFollowerCount = 0;
            }
        }

        return $iFollowerCount;
    }

    /**
     * Get number of followings
     * @param  integer $iUserId
     * @return integer
     */
    public static function getAmountFollowing($iUserId = 0)
    {
        $iFollowingCount = 0;

        if ($iUserId > 0) {
            // Get total followings
            $sKeyFollowing = sprintf(Helper::params('redisKeys')['userfollowings'], $iUserId);
            $iFollowingCount = RestHelper::getItems('redis/count', [
                'key' => $sKeyFollowing,
            ]);

            if (empty($iFollowingCount)) {
                $iFollowingCount = 0;
            }
        }

        return $iFollowingCount;
    }

    /**
     * Get user type name
     * @param  integer $iUserTypeId
     * @return string
     */
    public static function getUserType($iUserTypeId = 0)
    {
        $sUserTypeName = '';

        if ($iUserTypeId > 0) {
            $result = RestHelper::getItems('user-type/one', ['params' => ['id' => $iUserTypeId]]);
            if (!empty($result)) {
                $sUserTypeName = $result['name'];
            }
        }

        return $sUserTypeName;
    }

    /**
     * Get number of unread message
     * @param  integer $iUserId
     * @return integer
     */
    public static function getUnreadMessage($iUserId = 0)
    {
        $iUnreadMessage = 0;

        if ($iUserId > 0) {
            $sql = "SELECT *
                    FROM (
                        SELECT *
                        FROM messages
                        WHERE receiver_id = {$iUserId}
                            AND unread = 0
                            AND deleted_user_ids not LIKE '%;{$iUserId};%'
                        ORDER BY sender_id ASC, updated_at DESC
                    ) AS u
                    GROUP BY sender_id
                    ORDER BY created_at";

            $result = RestHelper::getItems('message/all-by-sql', [
                'sql' => $sql
            ]);

            if (!empty($result)) {
                $iUnreadMessage = count($result);
            }
        }

        return $iUnreadMessage;
    }

    /**
     * Get users information for auto complete when choosing to send message
     * @return array
     */
    public static function getJsonUsers()
    {
        $jsonUsers = Yii::$app->cache->get('jsonUsers');

        if ($jsonUsers !== false) {
            return $jsonUsers;
        }

        $Users = RestHelper::getItems('profile/all', [
            'select' => ['user_id as id', 'nickname as name'],
            'notIndex' => true,
        ]);


        $jsonUsers = '';
        if (!empty($Users)) {
            $jsonUsers = Json::encode($Users);
        }

        Yii::$app->cache->set('jsonUsers', $jsonUsers, 3600);

        return $jsonUsers;
    }

    /**
     * Get user information
     * @param  integer $id user id
     * @return array
     */
    public static function getUserInfo($id = 0)
    {
        if ($id > 0) {
            $profiles = RestHelper::getItems('profile/all', [
                'params'    => ['user_id' => $id],
                'limit'     => 1,
                'with'      => ['followers', 'followings', 'medals', 'cups', 'merchant', 'likes', 'galleries'],
                'notIndex'  => true,
            ]);

            if (!empty($profiles)) {
                return $profiles[0];
            }
        }
        return [];
    }

    /**
     * Check whether contest is active or not
     * @param  integer $id contest id
     * @return array
     */
    public static function checkActiveContest($id = 0)
    {
        $result = false;

        if ($id > 0) {
            $resultApi = RestHelper::getItems('contest/one', [
                'params' => ['id' => $id, 'status' => 1],
            ]);

            if (!empty($resultApi)) {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * Get contest information by slug
     * @param  string $slug contest slug
     * @return array
     */
    public static function getContestBySlug($slug = '')
    {
        $contest = [];

        if (!empty(trim($slug))) {
            $contest = RestHelper::getItems('contest/one', [
                'params' => ['slug' => $slug],
            ]);
        }

        return $contest;
    }

    /**
     * Get number of bad reports
     * @param  string $objectType
     * @param  integer $objetId
     * @return integer
     */
    public static function getNumBadReport($objectType, $objetId)
    {
        $result = RestHelper::getItems('bad-report/count', [
            'params' => ['object_type' => $objectType, 'object_id' => $objetId],
        ]);

        return !empty($result) ? $result : 0;
    }

    /**
     * Get tags
     * @return array
     */
    public static function getTags()
    {
        $tags = Yii::$app->cache->get('Tags');

        if ($tags === false) {
            $hottrends = RestHelper::getItems('tag/all', [
                'params' => [
                    'type' => 'HOT TREND',
                    'status' => 1,
                ],
            ]);
            $styles = RestHelper::getItems('tag/all', [
                'params' => [
                    'type' => 'STYLE',
                    'status' => 1,
                ],
            ]);
            $events = RestHelper::getItems('tag/all', [
                'params' => [
                    'type' => 'EVENT',
                    'status' => 1,
                ],
            ]);
            $tags = ['hottrends' => $hottrends, 'styles' => $styles, 'events' => $events];
            Yii::$app->cache->set('Tags', $tags, 3600);
        }

        return $tags;
    }

    /**
     * Get time remaining
     * @param  string  $sDate
     * @param  boolean $isMinute
     * @return string
     */
    public static function getTimeRemaining($start, $time_end)
    {
        //Helper::dump([$start,$end]);
        $str = '';

        $remaining = date_diff(date_create(date('Y-m-d H:i:s', $start)), date_create(date('Y-m-d H:i:s', $time_end)));

        if ($remaining->days > 0) {
            $str .= ' ' . $remaining->days . ' ' . Yii::t('common', 'days');
        }

        if ($remaining->h > 0) {
            $str .= ' ' . $remaining->h . ' ' . Yii::t('common', 'hrs');
        }

        if ($remaining->i > 0) {
            $str .= ' ' . $remaining->i . ' ' . Yii::t('common', 'mns');
        }

        return $str;
    }

    /**
     * Get time ago
     * @deprecated 0.5.0
     * @param  string  $sDate
     * @param  boolean $isView
     * @return string
     */
    public static function getTimeAgo($sDate, $isView = false)
    {
        $str = '';
        $ago = date_diff(date_create($sDate), date_create(time()));

        if ($ago->days > 0) {
            $str = $ago->days . ' ' . Yii::t('common', 'day');
            if ($isView) {
                $str .= ' | ' . date("d-m-Y", $sDate);
            }
        } else if ($ago->h > 0) {
            $str = $ago->h . ' ' . Yii::t('common', 'hour');
        } else {
            $str = $ago->i . ' ' . Yii::t('common', 'minute');
        }

        return $str;
    }

    /**
     * Get tag list with json format
     * @return string
     */
    public static function getjsonTagList()
    {
        // $jsonTagList = Yii::$app->cache->get('jsontaglist');

        // if ($jsonTagList === false) {
            $jsonTagList = '';
            $tagList = RestHelper::getItems('tag/all', [
                // 'params' => ['and', '1 = 1'],
                'notSelect' => true
            ]);

            if (!empty($tagList)) {
                $artag = [];
                foreach ($tagList as $tag) {
                    $artag[] = ['name' => $tag['name']];
                }
                if (!empty($artag)) {
                    $jsonTagList = Json::encode($artag);
                }
            }

            // Yii::$app->cache->set('jsontaglist', $jsonTagList, 7200);
        // }

        return $jsonTagList;
    }

    /**
     * Get category list with json format
     * @return string
     */
    public static function getjsonCategoryList()
    {
        // $jsonCategories = Yii::$app->cache->get('jsoncategories');

        // if ($jsonCategories === false) {
            $jsonCategories = '';

            $categories = RestHelper::getItems('category/all', [
                'params' => ['status' => 1],
                'select' => ['id', 'name'],
                //'notIndex' => 1,
            ]);

            $womenCates = RestHelper::getItems('category/list-categories', [
                'code' => Helper::params('codeCategoryFashion')['women'],
            ]);
            if (!empty($womenCates)) {
                $categories = self::editNameCate($categories,$womenCates,'Women\'s {0}');
            }

            $menCates = RestHelper::getItems('category/list-categories', [
                'code' => Helper::params('codeCategoryFashion')['men'],
            ]);
            if (!empty($menCates)) {
                $categories = self::editNameCate($categories,$menCates);
            }

            $categories = array_values($categories);
            if (!empty($categories)) {
                $jsonCategories = Json::encode($categories);
            }

            // Yii::$app->cache->set('jsoncategories', $jsonCategories, 7200);
        // }

        return $jsonCategories;
    }

    private static function editNameCate($categories,$objectcates,$addText = 'Men\'s {0}')
    {
        if (!empty($objectcates)) {
            foreach($objectcates as $ob) {
                $categories[$ob['id']]['name']= Yii::t('common',$addText,[$categories[$ob['id']]['name']]);
                if (isset($ob['children'])) {
                   $categories = self::editNameCate($categories,$ob['children'],$addText);
                }
            }
        }

        return $categories;
    }

    /**
     * Get brand list with json format
     * @return string
     */
    public static function getjsonBrandList()
    {
        $jsonBrands = Yii::$app->cache->get('jsonbrands');

        if ($jsonBrands === false) {
            $jsonBrands = '';

            $brands = RestHelper::getItems('brand/all', [
                'params' => ['status' => 1],
                'select' => ['id', 'name'],
                'notIndex' => 1,
            ]);

            if (!empty($brands)) {
                $jsonBrands = Json::encode($brands);
            }

            Yii::$app->cache->set('jsonbrands', $jsonBrands, 7200);
        }

        return $jsonBrands;
    }

    /**
     * Get store list with json format
     * @return string
     */
    public static function getjsonStoreList()
    {
        $jsonStores = Yii::$app->cache->get('jsonstores');

        if ($jsonStores === false) {
            $jsonStores = '';

            $stores = RestHelper::getItems('store/all', [
                'params' => ['and', 'status > 0'],
                'select' => ['id', 'name'],
                'notIndex' => 1,
            ]);

            if (!empty($stores)) {
                $jsonStores = Json::encode($stores);
            }

            Yii::$app->cache->set('jsonstores', $jsonStores, 7200);
        }
        return $jsonStores;
    }

    /**
     * Check profile had avatar, identity_number and phone yet
     * @param array $profile
     * @param boolean $checkAvatar
     * @return string
     */
    public static function checkProfile($profile = [], $checkAvatar = false)
    {
        if (!empty($profile)) {
            if ($checkAvatar) {
                if (empty($profile['avatar'])) {
                    return false;
                }
            }

            if (!empty($profile['identity_number']) && !empty($profile['phone'])) {
                return true;
            }
        }

        return false;
    }

    /**
     * Convert digit to text for CONTEST
     *
     * @return string
     */
    public static function convertDigitToText($key = 0)
    {
        $ar = [
            0  => Yii::t('common', '0th'),
            1  => Yii::t('common', '1st'),
            2  => Yii::t('common', '2nd'),
            3  => Yii::t('common', '3rd'),
            4  => Yii::t('common', '4th'),
            5  => Yii::t('common', '5th'),
            6  => Yii::t('common', '6th'),
            7  => Yii::t('common', '7th'),
            8  => Yii::t('common', '8th'),
            9  => Yii::t('common', '9th'),
            10 => Yii::t('common', '10th'),
        ];

        return $ar[$key];
    }

    /**
     * Get number of unread announcements
     * @param  integer $userId
     * @return integer
     */
    public static function getNumberUnreadNotification($userId = null)
    {
        if (is_null($userId)) {
            $userId = Helper::user()->id;
        }

        $total = RestHelper::getItems('notification/count', [
            'params' => [
                'receiver_id' => $userId,
                'status' => \app\modules\notification\models\Notification::STATUS_ACTIVE,
            ],
        ]);

        return !empty($total) ? $total : 0;
    }

    /**
     * Get notifications
     * @param  array  $model
     * @param  boolean $bLinkRedirect
     * @return array
     */
    public static function getNotification($model, $bLinkRedirect = false)
    {
        $modelClass = '\app\modules\notification\models\Notification';
        $url = 'javascript:;';
        $objectName = '';
        $whose = '';

        switch ($model['object_type']) {
            case $modelClass::OBJECT_TYPE_ITEM:
                // no break
            case $modelClass::OBJECT_TYPE_SET:
                // no break

            case $modelClass::OBJECT_TYPE_COLLECTION:
                $url = Yii::$app->getModule($model['object_type'])->createUrl($model['object']);

                if ($bLinkRedirect) {
                    $objectName = " <a href='" . $url . "'>" . $model['object']['name'] . "</a> ";
                } else {
                    $objectName = ' <strong>' . $model['object']['name'] . '</strong> ';
                }

                break;

            case $modelClass::OBJECT_TYPE_USER:
                if ($model['action'] === $modelClass::USER_ACTION_MESSAGE) {
                    $url = Url::to(['/user/message/detail', 'slug' => $model['receiver']['profile_url'], 'nickchat' => $model['sender']['profile_url']]);
                } else if ($model['action'] === $modelClass::USER_ACTION_FOLLOW || $model['action'] === $modelClass::USER_ACTION_UNFOLLOW) {
                    $url = Yii::$app->getModule($model['object_type'])->createUrl($model['receiver'], '/user/follow/follower');
                }
                break;

            default:
                # code...
                break;
        }

        switch ($model['action']) {
            case $modelClass::USER_ACTION_FOLLOW:
                // no break;
            case $modelClass::USER_ACTION_UNFOLLOW:
                $whose = 'you';
                break;
            case $modelClass::USER_ACTION_MESSAGE:
                $whose = 'to you';
                break;
            // USER_ACTION_LIKE
            // USER_ACTION_UNLIKE
            // USER_ACTION_COMMENT
            // USER_ACTION_UNCOMMENT
            // USER_ACTION_BOOKMARK
            // USER_ACTION_UNBOOKMARK
            default:
                $objectName = $objectName;
                $whose = 'your';
                break;
        }

        return [$url, $objectName, $whose];
    }

    /**
     * Check whether user is ban globally or not
     * @param  array $user an array of user information
     * @return boolean
     */
    public static function checkBanAccount($user)
    {
        return $user['block_to'] >= time();
    }

    /**
     * Check whether user is ban in ZOCO or not
     * @param  integer $iUserId
     * @return boolean
     */
    public static function checkBanProfileZoco($iUserId)
    {
        $profile = RestHelper::getItems('profile/one', [
            'params' => ['user_id' => $iUserId],
        ]);

        if (empty($profile)) {
            return true;
        }

        return $profile['block_to'] >= time();
    }

    /**
     * Get human readable time
     * @param  integer $timeStart
     * @param  integer $timeEnd if null, it will get current time
     * @return Formatter
     */
    public static function getRelativeTime($timeStart, $timeEnd = null)
    {
        return Yii::$app->formatter->asRelativeTime($timeStart, $timeEnd);
    }

    /**
     * Check is a set joining contest or won a contest
     * @param  integer  $iSetId
     * @return boolean
     */
    public static function isJoiningContestOrWinSet($iSetId)
    {
        // Check if current set is joining any contest
        $activeContestSets = RestHelper::getItems('contest-set/all', [
            'params' => [
                'set_id' => $iSetId,
                'user_id' => Helper::user()->id,
                'status' => 1,
            ],
        ]);

        if (!empty($activeContestSets)) {
            // Check if current set is a winner set or not
            $arrContestIds = [];
            foreach ($activeContestSets as $contestSet) {
                if ($contestSet['rank'] > 0) {
                    return true;
                }

                $arrContestIds[] = $contestSet['contest_id'];
            }

            // Check if is there any contest running
            $arrContests = RestHelper::getItems('contest/all', [
                'params' => [
                    'id' => $arrContestIds,
                    'status' => 1,
                ],
            ]);

            if (!empty($arrContests)) {
                return true;
            }
        }

        return false;
    }

    /**
     * Get random item/set/collection for homepage
     * @return array
     */
    public static function getRandom()
    {
        $temps = Helper::params('random-index');
        if ($temps['type'] == 1) {
            return mt_rand(1, 20)/10;
        } else if ($temps['type'] == 2) {
            $hour = intval(date('h', time()));
            $istime = 0;
            foreach ($temps['times'] as $i => $value) {
                if ($hour > $value) {
                    $istime = $i;
                    break;
                }
                continue;
            }

            $arrRandomIndex = Yii::$app->cache->get('random-index');
            if (($arrRandomIndex === false) ||($arrRandomIndex['istime'] != $istime)) {
                $arrRandomIndex = [];
                $arrRandomIndex['istime'] = $istime;
                $arrRandomIndex['numberRand'] = mt_rand(1, 20)/10;

                Yii::$app->cache->set('random-index', $arrRandomIndex, 3600*5);
            }
            return $arrRandomIndex['numberRand'];
        }
    }

    /**
     * Get message alert error
     */
    public static function getAlertErrors($class = 'danger', $errors = null)
    {
        $strError = '';
        switch ($class) {
            case 'danger':
                    $tags = Html::tag('i', '', ['class' => 'fa-fw fa fa-times']) .
                            Html::tag('strong', 'Error!');
                break;

            case 'success':
                    $tags = Html::tag('i', '', ['class' => 'fa-fw fa fa-check']) .
                            Html::tag('strong', 'Success!');
                break;
        }
        if (is_array($errors)) {
            foreach ($errors as $array) {
               foreach ($array as $error) {
                    $strError .= $error . '<br>';
               }
            }
            return $tags . ' ' . $strError;
        } else {
            return $tags . ' ' . $errors;
        }
    }
}
