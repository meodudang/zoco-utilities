<?php

namespace zoco\helpers\common;

use Yii;

/**
 * Helper provides MAIL helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.3.0
 */
class MailHelper
{
    /**
     * Send email
     * @param  string $to
     * @param  string $subject
     * @param  string $view
     * @param  array  $params
     * @return bool
     */
    public static function sendMessage($to, $subject, $view, $params = [])
    {
        $mailer = Yii::$app->mailer;
        $mailer->viewPath = '@app/mail';

        return $mailer->compose($view, $params)
            ->setTo($to)
            ->setFrom('noreply@zoco.vn')
            ->setSubject($subject)
            ->send();
    }

    /**
     * Validating not disposable email
     * @param  string $email email to validate
     * @return boolean
     */
    public static function validateUndisposable($email)
    {
        $arr = explode('@', $email);

        $domain = array_pop($arr);

        return !ArrayHelper::inArray($domain, Helper::params('disposableEmails'));
    }
}
