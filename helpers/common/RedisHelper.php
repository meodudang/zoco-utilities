<?php

namespace zoco\helpers\common;

use Yii;

/**
 * Helper provides REDIS helper functions for FRONTEND & BACKEND.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 1.0
 */
class RedisHelper
{
    /**
     * Add new element to redis
     * @param integer $announcementId
     * @param integer $iUserId
     * @return boolean
     */
    public static function markAsReadAnnouncement($announcementId, $iUserId = null)
    {
        if (is_null($iUserId)) {
            $iUserId = Helper::user()->id;
        }

        // Get redis key
        $sRedisKey = sprintf(Helper::params('redisKeys')['user-read-announcements'], $iUserId);

        // Add to redis
        $iResult = RestHelper::getItems('redis/add', [
            'key' => $sRedisKey,
            'element' => $announcementId,
        ]);

        return (boolean) $iResult;
    }

    /**
     * Get read announcements
     * @param  integer $iUserId
     * @return array
     */
    public static function getReadAnnouncements($iUserId = null)
    {
        if (is_null($iUserId)) {
            $iUserId = Helper::user()->id;
        }

        // Get redis key
        $sRedisKey = sprintf(Helper::params('redisKeys')['user-read-announcements'], $iUserId);

        $arrIds = RestHelper::getItems('redis/all', [
            'key' => $sRedisKey,
        ]);

        return $arrIds;
    }

    /**
     * Check whether user voted set or not
     * @param  integer $iContestId
     * @param  integer $iSetId
     * @param  integer $iUserId
     * @return boolean
     */
    public static function checkUserVotedContestSet($iContestId = 0, $iSetId = 0, $iUserId = 0)
    {
        if ($iContestId == 0 || $iSetId == 0 || $iUserId == 0) {
            return false;
        }

        // Get redis key
        $sRedisKey = sprintf(Helper::params('redisKeys')['contest-set-vote-by-user'], $iContestId, $iSetId);

        $apiresult = RestHelper::callApi('redis/check', [
            'key' => $sRedisKey,
            'element' => $iUserId
        ]);

        if (isset($apiresult['error']) && $apiresult['error'] == 0) {
            return true;
        }

        return false;
    }

    /**
     * Set number of times that users changed their nickname
     * @param integer $iUserId
     * @return boolean
     */
    public static function setNumChangeNickname($iUserId = 0)
    {
        if ($iUserId > 0) {
            // Get redis key
            $sRedisKey = sprintf(Helper::params('redisKeys')['userchangenickname'], $iUserId);

            $iResult = RestHelper::getItems('redis/count', [
                'key' => $sRedisKey,
            ]);

            if (empty($iResult)) {
                $iResult = 0;
            }

            // Add to redis
            $iResult = RestHelper::getItems('redis/add', [
                'key' => $sRedisKey,
                'element' => ++$iResult,
            ]);

            return (boolean) $iResult;
        }

        return false;
    }

    /**
     * Set number of times that users changed their nickname
     * @param integer $iUserId
     * @return boolean
     */
    public static function isChangedNickname($iUserId = 0)
    {
        $result = false;

        if ($iUserId > 0) {
            // Get redis key
            $sRedisKey = sprintf(Helper::params('redisKeys')['userchangenickname'], $iUserId);

            // Add to redis
            $iResult = RestHelper::getItems('redis/count', [
                'key' => $sRedisKey,
            ]);

            if (empty($iResult)) {
                $iResult = 0;
            }

            if ($iResult < Yii::$app->getModule('user')->limitChangeNickname) {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * Count number ot likes that user did
     * @param  integer $iUserId
     * @param  string  $sObjectType
     * @return integer
     */
    public static function countLikeOfUser($iUserId = 0, $sObjectType = 'set')
    {
        $num = 0;

        if ($iUserId > 0) {
            // Get redis key
            $sRedisKey = sprintf(Helper::params('redisKeys')['userlike' . $sObjectType . 's'], $iUserId);

            // Add to redis
            $num = RestHelper::getItems('redis/count', [
                'key' => $sRedisKey,
            ]);

            if (empty($num)) {
                $num = 0;
            }
        }

        return $num;
    }
}
