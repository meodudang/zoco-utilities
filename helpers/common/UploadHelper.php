<?php

namespace zoco\helpers\common;

/**
 * Helper provides UPLOAD helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.1.0
 */
class UploadHelper
{
    /**
     * Remote upload file
     * @param  string $file file path or base64 encode string
     * @param  string $url remote server url for uploading
     * @param  integer $userId
     * @param  string $fileExtension
     * @param  boolean $isBase64 whether base64 encode string or not
     * @param  string $uploadDir upload directory for saving file
     * @param  string $fileName file name will be used for saving file
     * @return array
     */
    public static function remoteUpload($file, $url, $userId, $fileExtension, $uploadDir = 'misc', $isBase64 = false, $fileName = null, $isClip = false, $createThumb = true)
    {
        // Preparation POST parameters for CURL calling
        if ($isBase64) {
            $fileData = $file;
        } else {
            // $handle   = fopen($file, "rb");
            // $data     = fread($handle, filesize($file));
            // $data     = fread($handle, static::getRemoteFileSize($file));
            $data = file_get_contents($file);
            $fileData = base64_encode($data);
            // fclose($handle);
        }

        $params = [
            'file'          => $fileData,
            'fileExtension' => $fileExtension,
            'fileName'      => $fileName,
            'uploadDir'     => $uploadDir,
            'isClip'        => $isClip,
            'isBase64'      => $isBase64,
            'createThumb'   => $createThumb,
        ];

        // Call API
        return RestHelper::callCurl($url, $params, $userId);
    }

    /**
     * Get remote file size
     * @param  string $url file url
     * @return integer
     */
    public static function getRemoteFileSize($url)
    {
        static $regex = '/^Content-Length: *+\K\d++$/im';

        if (!$fp = @fopen($url, 'rb')) {
            return false;
        }

        if (isset($http_response_header) &&
            preg_match($regex, implode("\n", $http_response_header), $matches)
        ) {
            return (int)$matches[0];
        }

        return strlen(stream_get_contents($fp));
    }
}
