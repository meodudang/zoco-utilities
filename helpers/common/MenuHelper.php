<?php

namespace zoco\helpers\common;

use Yii;
use yii\helpers\Url;

/**
 * MENU helper provides helper functions for using in yii2 projects. (with REST API)
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.1.0
 */
class MenuHelper
{
    /**
     * Lifetime of cache in seconds
     */
    const CACHE_LIFETIME = 3600;

    /**
     * Create menu
     * @param  string $code
     * @param  array $arrGetParams
     * @return array
     */
    public static function createMenu($code = null, $arrGetParams = null)
    {
        if (!isset($code)) {
            return [];
        }

        // $datas = Yii::$app->cache->get($code);
        // if ($datas === false) {
            $params['code'] = $code;

        if (!empty($arrGetParams)) {
            $params['getparam'] = $arrGetParams;
        }

            $arrResult = RestHelper::getItems('menu/itemmenu', $params);
            // Yii::$app->cache->set($code, $arrResult, self::CACHE_LIFETIME);
        // }
        return $arrResult;
    }

    /**
     * Create category menu
     * @param  string|null $sPathUrl
     * @return array
     */
    public static function createMenuCategory($sPathUrl = null)
    {
        /*
        if (!isset($sPathUrl)) {
            return [];
        }

        // $result = Yii::$app->cache->get('MenuCategory');
        $result = false;
        if ($result === false) {
            $params['params'] = [
                'and',
                'show_at_header > 0',
                'status = 1',
            ];
            $params['orderby'] = ['show_at_header' => SORT_ASC];

            $resultApi = RestHelper::callApi('category/all', $params);
            $result = [];
            if (isset($resultApi['body']) && !empty($resultApi['body'])) {
                $arrs = $resultApi['body'];

                foreach ($arrs as $arr) {
                    $result[] = [
                        'label'  => $arr['name'],
                        'url'    => [$sPathUrl, 'slug' => $arr['slug']],
                        'route'  => $sPathUrl,
                        'params' => ['slug' => $arr['slug']],
                    ];
                }
            }

            // Yii::$app->cache->set('MenuCategory', $result, self::CACHE_LIFETIME);
        }

        return $result;
        */

        $resultApi = RestHelper::getItems('category/menu-categories', [
            'params' => ['and', '1 = 1'],
        ]);

        return $resultApi;
    }

    /**
     * Create category menu
     * @return array
     */
    public static function createSubMenuItem()
    {
        $result = Yii::$app->cache->get('SubMenuItem');

        if ($result === false) {
            $params['params'] = ['level' => 1, 'status' => 1];
            $params['orderby'] = ['root' => SORT_ASC];
            $result = static::createMenu('subheaderitemMenuItem');

            if (!empty($result)) {
                foreach ($result as $i => $element) {
                    if ($result[$i]['alias'] === 'itemstore') {
                        $sparams = [
                            'params' => [
                                'status' => 1,
                            ],
                            'limit' => 10,
                        ];

                        $stores = RestHelper::getItems('store/all', $sparams, false, 'itemstore');

                        if (!empty($stores)) {
                            foreach ($stores as $store) {
                                $result[$i]['items'][] = [
                                    'label'  => $store['name'],
                                    'url'    => ['/item/item/store', 'slug' => $store['slug']],
                                    'route'  => '/item/item/store',
                                    'params' => ['slug' => $store['slug']],
                                ];
                            }
                        }

                        // Add store list page into menu's items
                        $result[$i]['items'][] = [
                            'label'  => Yii::t('common', 'Others'),
                            'url'    => ['/store/store/index'],
                            'route'  => ['/store/store/index'],
                            'params' => [],
                        ];
                    } else if ($result[$i]['alias'] === 'prices') {
                        $arr = Yii::$app->request->get();
                        $url = '/item/' . Helper::currentControllerId() . '/' . Helper::currentActionId();

                        foreach ($result[$i]['items'] as $j => $item) {
                            if ($result[$i]['items'][$j]['url'][0] !== '#price-popup') {
                                $result[$i]['items'][$j]['url'][0] = $url;
                                $result[$i]['items'][$j]['route'] = $url ;
                                $params = $result[$i]['items'][$j]['params'];
                                $arr = array_merge($arr, $params);
                                foreach ($arr as $key => $value) {
                                    if ($key !== 'page' && $key !== 'per-page') {
                                        $result[$i]['items'][$j]['url'][$key] = $value;
                                        $result[$i]['items'][$j]['params'][$key] = $value;
                                    }
                                }
                            }
                        }
                    }
                }
            }

            Yii::$app->cache->set('SubMenuItem', $result, self::CACHE_LIFETIME);
        }

        return $result;
    }
}
