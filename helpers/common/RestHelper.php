<?php

namespace zoco\helpers\common;

use Yii;
use yii\helpers\Url;

/**
 * REST API helper provides helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 1.0
 */
class RestHelper
{
    const ERROR_SUCCESS         = 0;
    const ERROR_INVALID_PARAMS  = 1;
    const ERROR_INVALID_REQUEST = 2;
    const ERROR_INVALID_DATA    = 4;
    const ERROR_FAIL            = 8;

    /**
     * Get item of users
     * @param  string  $api
     * @param  array   $params
     * @param  boolean $isApiAccoutUrl
     * @param  string  $key
     * @param  integer $duration
     * @return array
     */
    public static function getItemUsers($api, $params = [], $isApiAccoutUrl = false, $key = '', $duration = 3600)
    {
        if (empty($api)) {
            return 0;
        }

        if (!empty($key)) {
            $result = Yii::$app->cache->get($key);
            if ($result !== false) {
                return $result;
            }
        }

        $result = [];
        $resultApi = static::callApi($api, $params, $isApiAccoutUrl);

        if (isset($resultApi['error']) && $resultApi['error'] == 0) {
            if (isset($resultApi['body']) && !empty($resultApi['body'])) {
                $result = $resultApi['body'];
            }
        }

        if (!empty($key)) {
            Yii::$app->cache->set($key, $result, $duration);
        }

        return $result;
    }

    /**
     * Get items by call api all and set data into cache
     * @param  string $api
     * @param  array $params parameters to call API
     * @param  string $key cache's key
     * @param  integer $duration cache's timelife
     * @return array
     */
    public static function getItems($api, $params = [], $key = '', $duration = 3600)
    {
        if (!empty($key)) {
            $result = Yii::$app->cache->get($key);
            if ($result !== false) {
                return $result;
            }
        }

        $result = [];
        $resultApi = static::callApi($api, $params);

        if (isset($resultApi['error']) && $resultApi['error'] == 0) {
            if (isset($resultApi['body']) && !empty($resultApi['body'])) {
                $result = $resultApi['body'];
            }
        }

        if (!empty($key)) {
            Yii::$app->cache->set($key, $result, $duration);
        }

        return $result;
    }

    /**
     * Get objects of collections by call api all and set data into cache
     * @param  array $collections an array of collections
     * @return mixed|array
     */
    public static function getObjectsOfCollections($collections)
    {
        $arrItems = [];
        $arrSets = [];

        if (!empty($collections)) {
            $arrItemIds = [];
            $arrSetIds = [];

            foreach ($collections as $id => $collection) {
                $collections[$id]['json_content'] = explode(';', $collection['json_content']);

                foreach ($collections[$id]['json_content'] as $i => $element) {
                    $arr = explode(':', $element);
                    $collections[$id]['json_content'][$i] = $arr;
                    if ($arr[0] == 'set') {
                        $arrSetIds[] = $arr[1];
                    } else {
                        $arrItemIds[] = $arr[1];
                    }
                }
            }

            // Get items information
            if (!empty($arrItemIds)) {
                $arrItemIds = array_unique(array_filter($arrItemIds));
                $arrItems = static::getItems('item/all', [
                    'params' => [
                        'id' => $arrItemIds,
                    ],
                ]);
            }

            // Get sets information
            if (!empty($arrSetIds)) {
                $arrSetIds = array_unique(array_filter($arrSetIds));
                $arrSets = static::getItems('set/all', [
                    'params' => [
                        'id' => $arrSetIds,
                    ],
                ]);
            }
        }

        return [$collections, $arrItems, $arrSets];
    }

    /**
     * Add/Update statistics for a model
     * @param string  $sObjectType
     * @param integer $iItemId
     * @param string  $sField
     * @param integer $iValue
     * @return boolean
     */
    public static function addStatistic($sObjectType = '', $iItemId = 0, $sField = '', $iValue = 1)
    {
        $session = Yii::$app->session;
        $result = false;

        if (!empty($sObjectType) && !empty($sField) && $iItemId != 0) {
            if ($sField === 'views') {
                if ((time() - $session['timeviews']) <= 5) {
                    return false;
                }
            }

            $resultApi = static::callApi($sObjectType . '-statistic/add', [
                'params' => [
                    $sObjectType . '_id' => $iItemId,
                    'date' => date("Y-m-d"),
                ],
                'data' => ['field' => $sField, 'value' => $iValue],
            ]);

            if ($sField === 'views') {
                $session['timeviews'] = time();
            }

            if (!empty($resultApi)) {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * Add/Update statistics for a list of models
     * @param string  $sObjectType
     * @param integer $arrItemIds
     * @param string  $sField
     * @param integer $iValue
     * @return boolean
     */
    public static function addStatistics($sObjectType = '', $arrItemIds = [], $sField = '', $iValue = 1)
    {
        $session = Yii::$app->session;
        $result = false;

        if (!empty($sObjectType) && !empty($sField) && !empty($arrItemIds)) {
            if ($sField === 'views') {
                if ((time() - $session['timeviews']) <= 5) {
                    return false;
                }
            } else if ($sField === 'popups') {
                if ((time() - $session['timepopup']) <= 5) {
                    return false;
                }
            }

            $resultApi = static::callApi('total-'. $sObjectType . '-statistic/add', [
                'params' => [
                    'date' => date("Y-m-d"),
                ],
                'data' => ['objects' => $arrItemIds, 'field' => $sField, 'value' => $iValue],
            ]);

            if ($sField === 'views') {
                $session['timeviews'] = time();
            }

            if (isset($resultApi['error']) && $resultApi['error'] == 0) {
                $result = true;
            }
        }

        return $result;
    }

    /**
     * Get bad reports information
     * @param  string $sObjectType
     * @param  integer $iObjectId
     * @return array
     */
    public static function getBadReport($sObjectType, $iObjectId)
    {
        $arrBadReportTypes = [];
        $arrUserReported = [];

        if (isset(Helper::user()->id)) {
            $arrObjectTypeInfo = static::getItems('object-type/one', [
                'params' => ['name' => $sObjectType],
            ]);

            if (!empty($arrObjectTypeInfo)) {
                $params['params'] = [
                    'is_active'      => 1,
                    'language_id'    => Helper::getIdLanguage(),
                    'object_type_id' => $arrObjectTypeInfo['id'],
                ];

                // Get bad report types
                $arrBadReportTypes = static::getItems('bad-report-type/all', $params);

                // Get users that reported
                // @todo chuyển sang dùng redis
                $arrUserReported = static::getItems('bad-report/all', [
                    'select' => ['id'],
                    'params' => [
                        'object_type' => $sObjectType,
                        'object_id'   => $iObjectId,
                        'reporter_id' => Helper::user()->id,
                    ],
                    'notIndex' => 1
                ]);
            }
        }

        return [$arrBadReportTypes, $arrUserReported];
    }

    /**
     * Call API
     * @param  string $method method name
     * @param  mixed $params parameters
     * @param  boolean $isApiAccoutUrl
     * @return array result
     */
    public static function callApi($method, $params, $isApiAccoutUrl = false)
    {
        // Build API URL
        if ($isApiAccoutUrl) {
            $apiUrl = Yii::$app->params['apiAccountUrl'] . $method;
        } else {
            $apiUrl = Yii::$app->params['apiUrl'] . $method;
        }

        // Call API
        return static::callCurl($apiUrl, $params);
    }

    /**
     * Format API result to return
     * @param  integer $timeStart timestamp when call API
     * @param  integer $error error code
     * @param  string $message message for return
     * @param  array/object $data data for return
     * @return array
     */
    public static function apiResult($timeStart, $error, $message, $data = null)
    {
        // Init result
        $result = array();
        $result['error'] = $error;
        $result['message'] = $message;

        if (isset($data)) {
            $result['body'] = serialize($data);
        }

        $result['api_exec_time'] = time() - $timeStart;

        return $result;
    }

    /**
     * Request validating
     * @param array $valids parameters for validating: ['timestamp' => '', 'token' => '']
     * @return boolean
     */
    public static function isValidApi($valids)
    {
        $isSuccess = 0;

        if (isset($valids['timestamp']) && isset($valids['token'])) {
            if (time() - $valids['timestamp'] <= Yii::$app->params['apiTimeout']) {
                $str = '';

                foreach ($valids as $key => $val) {
                    if ($key !== 'token') {
                        $str .= $val;
                    }
                }

                $str .= Yii::$app->params['apiSecret'];

                if ($valids['token'] === md5($str)) {
                    $isSuccess = 1;
                }
            }
        }

        return $isSuccess;
    }

    /**
     * Check and validate request parameters
     * @param array $params parameters
     *
     * Array must have format like below:
     *
     * ```
     * [
     *     'validate' => [ // for validating
     *         'user' => '', // user id if already login
     *         'timestamp' => 'value',
     *         'token' => 'value',
     *     ],
     *     'object' => [
     *         'params' => [ // search conditions
     *             'model_attribute' => 'value',
     *         ],
     *         'data' => [ // using for create/update
     *             'model_attribute' => 'value',
     *         ],
     *
     *         // using for selectAll/selectOne
     *         '{ActiveRecord method name}' => ['value', 'value'],
     *     ],
     * ]
     * ```
     *
     * @return array
     */
    public static function checkParamsAndValidate($params)
    {
        $timeStart = time();

        // Get request parameters
        // $params = Yii::$app->getRequest()->post();

        // Check request parameters
        if (empty($params['validate']) || empty($params['object'])) {
            return static::apiResult($timeStart, self::ERROR_INVALID_PARAMS, Yii::t('api', 'The requested page invalid!'));
        }

        // Validating request
        if (static::isValidApi(unserialize($params['validate'])) != 1) {
            return static::apiResult($timeStart, self::ERROR_INVALID_REQUEST, Yii::t('api', 'The request is unauthorized!'));
        }

        // Check request data
        $params['object'] = unserialize($params['object']);

        // Deprecated
        // if (!isset($params['object']['params']) && !isset($params['object']['data']))
        // {
        //     return static::apiResult($timeStart, self::ERROR_INVALID_DATA, Yii::t('api', 'The request is invalid!'));
        // }

        return [
            'timeStart' => $timeStart,
            'object'    => $params['object'],
            'validate'  => unserialize($params['validate']),
        ];
        ;
    }

    /**
     * Get message of error code
     * @param  integer $status error code
     * @return string
     */
    public static function getStatusCodeMessage($status)
    {
        // these could be stored in a .ini file and loaded
        // via parse_ini_file()... however,self will suffice
        // for an example
        $codes = [
            200 => 'OK',
            400 => 'Bad Request',
            401 => 'Unauthorized',
            402 => 'Payment Required',
            403 => 'Forbidden',
            404 => 'Not Found',
            500 => 'Internal Server Error',
            501 => 'Not Implemented',
        ];

        return (isset($codes[$status])) ? $codes[$status] : '';
    }

    /**
     * Call API
     * @param  string $url  API url
     * @param  string $body serialized data
     * @return array
     */
    public static function callCurl($url, $body = null, $userId = null)
    {
        if (!isset($body)) {
            $body = '';
        };

        // Validating API token for indentify
        $valid = [];
        if (is_null($userId)) {
            if (isset(Yii::$app->user) && isset(Yii::$app->user->identity) && !empty(Yii::$app->user->identity->id)) {
                $valid['user'] = Yii::$app->user->identity->id;
            }
        } else {
            $valid['user'] = $userId;
        }

        $valid['timestamp'] = time();
        $valid['lang'] = Yii::$app->language;

        $str = '';
        foreach ($valid as $key => $value) {
            $str .= $value;
        }
        $valid['token'] = md5($str . Yii::$app->params['apiSecret']);

        // Preparation POST parameters for CURL calling
        $posts = [
            'validate' => serialize($valid),
            'object' => serialize($body),
        ];

        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        /*
        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            //'Content-Type: application/json',
            //'Content-Length: ' . strlen($body),
            //'Content-Md5: ' . base64_encode(md5($body, true)),
        ));
        */

        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $posts);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false); //
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false); //
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; da; rv:1.9.1) Gecko/20090624 Firefox/3.5');

        // Grab URL and pass it to the browser
        $temp = curl_exec($ch);
        curl_close($ch);

        // Decode JSON into array
        $result= json_decode($temp, true);

        if (isset($result['body'])) {
            $result['body'] = unserialize($result['body']);
        }

        return $result;
    }

    /**
     * Check item bad link
     * @param  string $url item's url
     * @return boolean
     */
    public static function checkBadLink($url)
    {
        $bResult = false;

        if (!empty($url)) {
            $arrUrlParts = parse_url($url);
            $sUrl = '';
            if (!empty($arrUrlParts['host'])) {
                $sUrl .= $arrUrlParts['host'];
            }
            if (!empty($arrUrlParts['port'])) {
                $sUrl .= ':' . $arrUrlParts['port'];
            }
            $sUrl .= $arrUrlParts['path'];
            if (!empty($arrUrlParts['query'])) {
                $sUrl .= '?' . $arrUrlParts['query'];
            }
            if (!empty($arrUrlParts['scheme'])) {
                $sUrl = $arrUrlParts['scheme'] . '://' . $sUrl;
            }

            if (!empty($sUrl)) {
                $ch = curl_init();
                curl_setopt($ch, CURLOPT_HEADER, 1);
                if (!empty($arrUrlParts['fragment'])) {
                    curl_setopt($ch, CURLOPT_HTTPHEADER, ['Hash: ' . $arrUrlParts['fragment']]);
                }
                curl_setopt($ch, CURLOPT_URL, $sUrl);
                // curl_setopt($ch, CURLOPT_NOBODY, 1);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 1);
                curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; da; rv:1.9.1) Gecko/20090624 Firefox/3.5');
                curl_setopt($ch, CURLOPT_SSL_VERIFYPEER , FALSE);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST , FALSE);

                $temp = curl_exec($ch);

                $status = curl_getinfo($ch, CURLINFO_HTTP_CODE);
                curl_close($ch);

                if ($status == 200) {
                    $bResult = true;
                }
            }
        }

        return $bResult;
    }

    /**
     * Update profile of user
     * @param  integer $userId
     * @param  string  $nickname
     * @return boolean
     */
    public static function updateProfile($userId = 0, $nickname = '')
    {
        $result = false;

        if (!empty($profile)) {
            $model = static::getItems('profile/all', [
                'params' => ['user_id' => $userId],
            ]);

            if (empty($model)) {
                $profile = static::getItems('user/profile/one', ['params' => ['user_id' => $userId]], true);

                if (isset($profile)) {
                    $apiResult = static::getItems('profile/add', [
                        'data' => [
                            'user_id'  => $profile['user_id'],
                            'nickname' => empty($nickname)
                                          ? $profile['last_name'] . ' ' . $profile['first_name']
                                          : $nickname,
                        ]
                    ]);

                    if (!empty($apiResult)) {
                        $result = true;
                    }
                }
            }
        }

        return $result;
    }

    /**
     * Get country from user IP
     * @param  string $ip
     * @return string
     */
    public static function getCountryByIp($ip = null)
    {
        $sUrl = 'freegeoip.net/json/';
        $language = 'vi-VN';
        if (!empty($ip)) {
            $sUrl .= $ip;
            $ch = curl_init();
            curl_setopt($ch, CURLOPT_URL, $sUrl);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
            curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 6.0; da; rv:1.9.1) Gecko/20090624 Firefox/3.5');

            $temp = \yii\helpers\Json::decode(curl_exec($ch));
            curl_close($ch);

            if (!empty($temp) && isset($temp['country_code']) && (!empty($temp['country_code']) && $temp['country_code']!=='VN')) {
                $language = 'en-US';
            }
        }

        return $language;
    }
}
