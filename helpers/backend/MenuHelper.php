<?php

namespace zoco\helpers\backend;

use zoco\helpers\common\MenuHelper as BaseMenuHelper;

/**
 * MENU helper provides helper functions for using in yii2 projects. (with REST API)
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.5.0
 */
class MenuHelper extends BaseMenuHelper
{

}
