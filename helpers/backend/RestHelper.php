<?php

namespace zoco\helpers\backend;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use zoco\helpers\common\RestHelper as BaseRestHelper;

/**
 * REST API helper provides helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 1.0
 */
class RestHelper extends BaseRestHelper
{
    /**
     * Map model attributes with API result (array)
     * @param  string $modelClass model class name, include namespace
     * @param  array $attributes API result
     * @param  string $scenario model scenario
     * @param  string $id models primary key value
     * @param  string $primaryKey name of primary key attribute
     * @return models/{MODEL}
     */
    public static function mapAttributes($modelClass, $attributes, $scenario = '', $id = '')
    {
        $model = new $modelClass;
        foreach ($attributes as $key => $value)
        {
           if ($model->hasProperty($key))
           {
                $model->$key = $value;
           }
        }
        if (!empty($scenario)) {
            $model->scenario = $scenario;
        }

        return $model;
    }

    /**
     * Map model attributes with API result (array)
     * @param  string $modelClass model class name, include namespace
     * @param  array $attributes API result
     * @param  string $scenario model scenario
     * @param  string $id models primary key value
     * @param  string $primaryKey name of primary key attribute
     * @return models/{MODEL}
     */
    public static function mapAttributesUser($modelClass, $attributes, $scenario = '', $id = '', $primaryKey = 'id')
    {
        $model = new $modelClass;
        foreach ($attributes as $key => $value)
        {
           if ($model->hasAttribute($key))
           {
                $model->$key = $value;
           }
        }
        if (!empty($scenario)) {
            $model->scenario = $scenario;
        }
        if (isset($id)) {
            $model->{$primaryKey} = $id;
        }

        return $model;
    }
}
