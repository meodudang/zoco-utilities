<?php

namespace zoco\helpers\backend;

use Yii;
use yii\helpers\Html;
use zoco\helpers\backend\RestHelper;
use zoco\helpers\backend\HtmlHelper;
use zoco\helpers\common\ArrayHelper;
use zoco\helpers\common\Helper as BaseHelper;

/**
 * Helper provides helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.5.0
 */
class Helper extends BaseHelper
{
    /**
     * Get name tag for select box
     */
    public static function getNameTags($module)
    {
        $arrayId = [];
        $nameTags            = [];
        $params['notSelect'] = true;
        $params['andWhere'] = ['object_type' => $module];
        $arrLanguage = HtmlHelper::listDropDown('language', 'id', 'code');
        $resulTagTypes = RestHelper::getItems('tag-type/all',$params);
        if (!empty($resulTagTypes)) {
            $_params['notSelect'] = true;
            $arrayId = ArrayHelper::getArray($resulTagTypes);
            $_params['params'] = ['in', 'tag_type_id', $arrayId];
            $resultTags          = RestHelper::getItems('tag/all', $_params);
            if (!empty($resultTags)) {
                foreach ($resultTags as $key => $value) {
                    if (isset($value['name'])) {
                        if (!empty($arrLanguage)) {
                            foreach ($arrLanguage as $k => $v) {
                                if (isset($value['language'])) {
                                    if ($value['language'] == $k) {
                                        $nameTags[$value['id']] = $value['name'] . '(' . substr($v, '0', 2) . ')';
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }
        return $nameTags;
    }

    /**
     * Get nickname for select box
     */
    public static function getNickName()
    {
        $nickNames = [];
        $key = 'nickname_key';
        $data = Yii::$app->cache->get($key);
        $params = [
                    'orderBy' => ['user_id' => SORT_DESC],
                    'indexBy' => 'user_id',
        ];
        $params['select'] = ['user_id', 'nickname'];
        $count = RestHelper::getItems('profile/count', $params);
        if ($data === false || $data['count'] != $count) {
            if (!empty($count)) {
                $resultProfile = RestHelper::getItems('profile/all', $params);
                if (!empty($resultProfile)) {
                    foreach ($resultProfile as $attribute => $value) {
                        foreach ($value as $k => $v) {
                            if ($k == 'nickname') {
                                $nickNames[$attribute] = $v;
                            }
                        }
                    }
                }
                $listNickNames = ['nickNames' => $nickNames, 'count' => $count];
                Yii::$app->cache->set($key, $listNickNames, 3600); // time in seconds to store cache
            } else {
                $listNickNames = ['nickNames' => $nickNames, 'count' => 0];
                Yii::$app->cache->set($key, $listNickNames, 3600); // time in seconds to store cache
            }
        }
    }
}
