<?php

namespace zoco\helpers\backend;

use Yii;
use yii\helpers\Url;
use yii\helpers\ArrayHelper;
use zoco\helpers\common\HtmlHelper as BaseHtmlHelper;

/**
 * HTML helper provides helper functions for using in yii2 projects (with REST API).
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.5.0
 */
class HtmlHelper extends BaseHtmlHelper
{
    /**
     * @inheritdoc
     */
    public static function listDropDown($apiPrefix, $sKey = 'id', $sValue = 'name', $arrParams = [])
    {
        $result = [];

        if (!empty($apiPrefix)) {
            $params = [];
            $params['select'] = [$sKey, $sValue];
            if (!empty($arrParams)) {
                $params['params'] = $arrParams;
            }

            $resultApi = RestHelper::callApi($apiPrefix . '/all', $params);

            if (isset($resultApi['body'])) {
                $result = ArrayHelper::map($resultApi['body'], $sKey, $sValue);
            }
        }

        return $result;
    }

    /**
     * @inheritdoc
     */
    public static function listDropDownNest($apiPrefix, $key = 'id', $value = 'name', $param = [], $level = 'level', $lft = 'left')
    {
        $result = [];

        if (isset($apiPrefix)  && !empty($apiPrefix)) {
            $params = [];
            $params['select'] = [$key, $value, $level, $lft];
            $params['orderBy'] = 'root, ' . $lft;

            if (!empty($param)) {
                $params['params'] = $param;
            }

            $resultApi = RestHelper::callApi($apiPrefix . '/all', $params);

            if (isset($resultApi['body'])) {
                $objects = $resultApi['body'];
                foreach ($objects as $object) {
                    $result[$object[$key]] = str_repeat('→', $object[$level]) . ' ' . $object[$value];
                }
            }
        }

        return $result;
    }
}
