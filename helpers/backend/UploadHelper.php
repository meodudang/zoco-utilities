<?php

namespace zoco\helpers\backend;

use zoco\helpers\common\UploadHelper as BaseUploadHelper;

/**
 * Helper provides UPLOAD helper functions for using in yii2 projects.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 * @since 0.5.0
 */
class UploadHelper extends BaseUploadHelper
{

}
