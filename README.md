Using Instructions for Yii Framework 2.*
=======================================

You need to add the following code to your project's `composer.json` file:

```json
"autoload": {
    "psr-4": {
        "zoco\\widgets\\": "vendor/zoco/yii2-widgets/widgets",
        "zoco\\helpers\\": "vendor/zoco/helpers",
        "zoco\\base\\": "vendor/zoco/base",
        "zoco\\models\\": "vendor/zoco/models"
    }
}
```

After please remember to dump the autoloader

```
composer dump-autoload
```

How to use:

```
use zoco\helpers\Helper;

Helper::dump($value);
```