<?php

/*
 * This file is part of the ZOCO project.
 *
 * (c) ZOCO project <http://www.bbd.com.vn/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace zoco\base;

use Yii;
use yii\web\Controller as BaseController;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\Pagination;
use yii\helpers\Url;
use yii\helpers\Inflector;
use zoco\helpers\common\RestHelper;
use zoco\helpers\common\Helper;
use zoco\helpers\common\UploadHelper;

/**
 * ZOCO REST Controller
 * This class is used almost by ZOCO back-end
 *
 * @deprecated deprecated since version 0.3.0
 * @author Cang Ta <hoksilato176@gmail.com>
 */
class Controller extends BaseController
{
    /**
     * @inheritdoc
     */
    protected $modelClass = '';

    /**
     * Model search's class name - must be precise
     * @var string
     */
    protected $modelSearchClass = '';

    /**
     * Model's api controller
     * @var string
     */
    protected $apiPrefix = '';

    /**
     * Module ID
     * @var string
     */
    protected $moduleId = '';

    /**
     * Page size for using in list page
     * @var integer
     */
    protected $pageSize = 15;

    /**
     * Model relations that will get information when return data from API
     * @var array
     */
    protected $withRelation = [];

    /**
     * Model fields to display in INDEX view
     *
     * Array has the format:
     *
     * ```
     * [
     *     'model_attribute' => [
     *         'label' => 'label will be displayed', // if this element is not set, will get model attribute's label
     *         'class' => 'CSS class',
     *         'filter' => true/false,
     *         'value' => 'value to display', @todo this should be in MODEL Class
     *         'format' => 'value' // @see Formater
     *     ],
     * ]
     * ```
     *
     * @var array
     * @todo implement custom value (anonymous function) for fields
     */
    protected $modelFields = [];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                        'matchCallback' => function ($rule, $action) {
                            return in_array(Yii::$app->user->identity->email, Yii::$app->getModule('user')->admins);
                        }
                    ],
                ]
            ]
        ];
    }

    /**
     * Lists all {MODEL} models.
     * @return mixed
     * @version 0.0.2
     */
    public function actionIndex()
    {
        $params = [];

        if (!empty($this->withRelation) && isset($this->withRelation['index']) && !empty($this->withRelation['index'])) {
            $params['with'] = $this->withRelation['index'];
        }

        // Get total count
        $totalCount = 0;
        $result = RestHelper::callApi(Inflector::camel2id($this->apiPrefix) . '/count', $params);
        if (isset($result['error']) && $result['error'] == 0) {
            $totalCount = $result['body'];
        }

        // Prepare pagination
        $pages = new Pagination(['totalCount' => $totalCount]);
        $pages->setPageSize($this->pageSize);

        // Get models
        $params['limit'] = $pages->limit;
        $params['offset'] = $pages->offset;
        $result = RestHelper::callApi(Inflector::camel2id($this->apiPrefix) . '/all', $params);

        $models = [];
        if (isset($result['error']) && $result['error'] == 0) {
            $models = $result['body'];
        }

        return $this->render('index', [
            'models' => $models,
            'pages' => $pages,
            'modelFields' => $this->modelFields,
            'modelInstance' => new $this->modelClass(),
        ]);
    }

    /**
     * Displays a single {MODEL} model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new {MODEL} model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new $this->modelClass();

        // Get default values
        $model->loadDefaultValues();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->hasAttribute('created_by')) {
                $model->setAttribute('created_by', Yii::$app->getUser()->id);
            }

            if ($model->hasAttribute('reporter_id')) {
                $model->setAttribute('reporter_id', Yii::$app->getUser()->id);
            }

            if ($model->hasAttribute('owner_id')) {
                $model->setAttribute('owner_id', Yii::$app->getUser()->id);
            }

            if ($model->hasAttribute('author_id')) {
                $model->setAttribute('author_id', Yii::$app->getUser()->id);
            }

            if ($model->hasAttribute('sender_id')) {
                $model->setAttribute('sender_id', Yii::$app->getUser()->id);
            }

            if ($model->validate()) {
                // Get parameters
                $post = Yii::$app->request->post();
                $params['data'] = $post[$this->apiPrefix];

                $result = RestHelper::callApi(Inflector::camel2id($this->apiPrefix) . '/add', $params);
                if ($result['error'] == 0) {
                    // Upload img
                    if (isset($params['data']['image']) && !empty($params['data']['image'])) {
                        $this->uploadImage($params['data']['image']);
                    }

                    Helper::setFlash('success', $result['message']);

                    return $this->redirect(['view', 'id' => $result['body']['id']]);
                } else {
                    Helper::setFlash('error', $result['message']);
                }
            } else {
                Helper::setFlash('error', $model->getErrors());
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing {MODEL} model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // Get MODEL data
        $object = $this->findModel($id);

        // Create MODEL instance
        $model = RestHelper::mapAttributes($this->modelClass, $object, '', $id);
        $model->isNewRecord = false; // @toto: test lại code vì sao phải có dòng này

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $post = Yii::$app->request->post();
                $params['params'] = ['id' => $id];
                $params['data'] = $post[$this->apiPrefix];

                $result = RestHelper::callApi(Inflector::camel2id($this->apiPrefix) . '/update', $params);

                if ($result['error'] == 0) {
                    // Upload img
                    if (isset($params['data']['image']) && !empty($params['data']['image'])) {
                        $this->uploadImage($params['data']['image']);
                    }

                    // Delete image if exist
                    if (!empty($object['image']) && ($params['data']['image'] !== $object['image'])) {
                        $this->deleteImage($object['image']);
                    }

                    Helper::setFlash('success', $result['message']);
                    return $this->redirect(['view', 'id' => $result['body']['id']]);
                } else {
                    Helper::setFlash('error', $result['message']);
                }
            } else {
                Helper::setFlash('error', $model->getErrors());
            }
        }

        return $this->render('update', [
            'model' => $model,
        ]);
    }

    /**
     * Deletes an existing {MODEL} model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $result = RestHelper::callApi(Inflector::camel2id($this->apiPrefix) . '/delete', [
            'params' => ['id' => $id],
        ]);

        if ($result['error'] == 0) {
            Helper::setFlash('success', $result['message']);

            // Delete image if exist
            // Note: This should be done in API
            // if (!empty($result['body']['image']))
            // {
            //     $this->deleteImage($result['body']['image']);
            // }
        } else {
            Helper::setFlash('error', $result['message']);
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the {MODEL} model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return {MODEL} the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $params = [];

        if (!empty($this->withRelation) && isset($this->withRelation['view']) && !empty($this->withRelation['view'])) {
            $params['with'] = $this->withRelation['view'];
        }

        $params['params'] = ['id' => $id];

        $result = RestHelper::callApi(Inflector::camel2id($this->apiPrefix) . '/one', $params);

        if (isset($result['body'])) {
            return $result['body'];
        } else {
            throw new NotFoundHttpException('Item not found.');
        }
    }

    /**
     * Delete image if exist
     * @param  string $image image name
     * @return boolean
     */
    protected function deleteImage($image)
    {
        $fileimage = Helper::params('pathUploadTmp') . Yii::$app->getModule(Inflector::camel2id($this->apiPrefix))->uploadDir . '/' . $image;

        if (file_exists($fileimage)) {
            if (!unlink($fileimage)) {
                //TODO: Write log

                return false;
            }
        }

        return true;
    }

    /**
     * upload image if exist
     * @param  string $image image name
     * @return boolean
     */
    protected function uploadImage($image)
    {
        $result = false;
        $sImageData = '';

        $sUploadDir = Yii::$app->getModule(Inflector::camel2id($this->apiPrefix))->uploadDir;
        $sImagePath = Helper::params('pathUploadTmp') . $sUploadDir . '/' . $image;

        $arrTemp = explode('.', $image);
        if (count($arrTemp) == 2) {
            $sFileExtension = $arrTemp[1];
        } else {
            // @TODO return error
        }

        if (file_exists($sImagePath)) {
            $data = file_get_contents($sImagePath);
            $sImageData = base64_encode($data);
        }

        if (!empty($sImageData)) {
            $resultApi = UploadHelper::remoteUpload($sImageData, Helper::params('cdnUpload'), Helper::user()->id, $sFileExtension, $sUploadDir, true, $image);

            if (isset($resultApi['error']) && $resultApi['error'] == 0) {
                $result = true;
            }
        }

        return $result;
    }
}
