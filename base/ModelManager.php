<?php

/*
 * This file is part of the POCO project.
 *
 * (c) POCO project <http://www.baobaodigital.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace zoco\base;

use yii\base\Component;

/**
 * ModelManager is used in order to create models and find users.
 *
 * @method models\{modelClass}        createModel
 * @method \yii\db\ActiveQuery        createModelQuery
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 */
class ModelManager extends Component
{
    /**
     * {Model} Class
     * @var string
     */
    public $modelClass = '';

    /**
     * {Model} Search Class
     * @var string
     */
    public $searchClass = '';

    /**
     * {Model} Form Class
     * @var string
     */
    public $formClass = '';

    /**
     * Profile Class for author information
     * @var string
     */
    public $profileClass = 'app\modules\user\models\Profile';

    /**
     * Find a {model} by the given condition.
     *
     * @param  mixed               $condition Condition to be used on search.
     * @return \yii\db\ActiveQuery
     */
    public function findModel($condition)
    {
        return $this->createModelQuery()->where($condition);
    }

    /**
     * Find a {model} by the given id.
     *
     * @param  integer     $id {Model} id to be used on search.
     * @return models\{Model}
     */
    public function findModelById($id)
    {
        return $this->findModel(['id' => $id])->one();
    }

    /**
     * Find a {model} by the given name.
     *
     * @param  string      $name {Model} name to be used on search.
     * @param  boolean     $isExact Search {model} with exact or like.
     * @param  boolean     $isPercentageIncluded Is percentage characters included yet?
     * @return models\{Model}
     * @see    http://www.yiiframework.com/doc-2.0/yii-db-query.html#where()-detail
     */
    /*
    public function findModelByName($name, $isExact = true, $isPercentageIncluded = false)
    {
        if ($isExact)
        {
            if ($isPercentageIncluded) {
                return $this->findModel(['like', 'name', $name, false])->all();
            }

            return $this->findModel(['like', 'name', $name])->all();
        }

        return $this->findModel(['name' => $name])->one();
    }
    */

    /**
     * Find a {model} by the given slug.
     *
     * @param  string      $slug {Model} slug to be used on search.
     * @return models\{Model}
     */
    /*
    public function findModelBySlug($slug)
    {
        return $this->findModel(['slug' => $slug])->one();
    }
    */

    /**
     * Find a {model} by the author ID.
     *
     * @param  string      $userId Author ID to be used on search.
     * @return models\{Model}
     */
    public function findModelsByAuthorId($userId)
    {
        return $this->findModel(['created_by' => $userId])->all();
    }

    /**
     * Find active {models} (is_show = 1).
     *
     * @return models\{Model}
     */
    public function findActiveModels()
    {
        return $this->findModel(['is_show' => 1])->all();
    }

    /**
     * Find inactive {models} (is_show = 0).
     *
     * @return models\{Model}
     */
    public function findInactiveModels()
    {
        return $this->findModel(['is_show' => 0])->all();
    }

    /**
     * Find an author profile by author id.
     *
     * @param integer $id Author ID to be used on search
     *
     * @return null|models\Profile
     */
    public function findAuthorProfileById($id)
    {
        return $this->createProfileQuery()->where(['user_id' => $id])->one();
    }

    /**
     * @param string $name
     * @param array $params
     * @return mixed|object
     */
    public function __call($name, $params)
    {
        $property = (false !== ($query = strpos($name, 'Query'))) ? mb_substr($name, 6, -5) : mb_substr($name, 6);
        $property = lcfirst($property) . 'Class';
        if ($query) {
            return forward_static_call([$this->$property, 'find']);
        }
        if (isset($this->$property)) {
            $config = [];
            if (isset($params[0]) && is_array($params[0])) {
                $config = $params[0];
            }
            $config['class'] = $this->$property;
            return \Yii::createObject($config);
        }

        return parent::__call($name, $params);
    }
}
