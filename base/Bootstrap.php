<?php

/*
 * This file is part of the ZOCO project.
 *
 * (c) ZOCO project <http://www.bbd.com.vn/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace zoco\base;

use yii\base\BootstrapInterface;
use yii\web\GroupUrlRule;

/**
 * Bootstrap class registers module and {MODEL} application component. It also creates some url rules which will be applied
 * when UrlManager.enablePrettyUrl is enabled.
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 */
class Bootstrap implements BootstrapInterface
{
    /**
     * Module ID in configurations
     * @var string
     */
    protected $moduleId = '';

    /**
     * Module namespace name
     * @var string
     */
    protected $moduleNamespace = '';

    /**
     * Module URL prefix
     * @var string
     */
    protected $moduleUrlPrefix = '';

    /**
     * Module translation category (also is translation file's name)
     * @var string
     */
    protected $moduleTranslationCategory = '';

    /**
     * @inheritdoc
     */
    public function bootstrap($app)
    {
        if (!$app->hasModule($this->moduleId)) {
            $app->setModule($this->moduleId, [
                'class' => "app\modules\{$this->moduleNamespace}\Module"
            ]);
        }

        /** @var $module Module */
        $module = $app->getModule($this->moduleId);

        $this->moduleUrlPrefix = $this->moduleNamespace;

        if ($app instanceof \yii\console\Application) {
            $module->controllerNamespace = "app\modules\{$this->moduleNamespace}\commands";
        } else {
            $configUrlRule = [
                // 'ruleConfig' => ['class' => 'zoco\base\UrlRule'],
                'prefix' => $module->urlPrefix,
                'rules'  => $module->urlRules
            ];

            $configUrlRule['routePrefix'] = $this->moduleUrlPrefix;

            $app->get('urlManager')->rules[] = new GroupUrlRule($configUrlRule);
        }
    }
}
