<?php

/*
 * This file is part of the POCO project.
 *
 * (c) POCO project <http://www.baobaodigital.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace zoco\base;

use Yii;
use yii\base\Module as BaseModule;
use yii\helpers\FileHelper;

/**
 * This is the main module class for the BRAND Module.
 *
 * @property ModelManager $manager
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 */
class Module extends BaseModule
{
    const VERSION = '0.1.0';

    /**
     * @var string The prefix for user module URL.
     * @see [[GroupUrlRule::prefix]]
     */
    public $urlPrefix = '';

    /**
     * Font Awesome icon for content-header
     * @var string
     * @see @app/views/layouts/content-header.php
     */
    public $faIcon = '';

    /**
     * Using for content-header. If not set, will get Module ID.
     * @var string
     * @see @app/views/layouts/content-header.php
     */
    public $name = '';

    /**
     * Name of upload directory
     * @var string
     */
    public $uploadDir = '';

    /**
     * Module namespace name
     * @var string
     */
    protected $moduleNamespace = '';

    /**
     * Module's api prefix controller
     * @var string
     */
    protected $apiPrefix = '';

    /**
     * @var array The rules to be used in URL management.
     */
    public $urlRules = [
        ''                                  => 'default/index',
        '<id:\d+>'                          => 'default/view',
        'create'                            => 'default/create',
        '<action:(update|delete)>/<id:\d+>' => 'default/<action>',
    ];

    /**
     * Returns module components.
     *
     * @return array
     */
    protected function getModuleComponents()
    {
        return [
            'manager' => [
                'class' => "app\modules\{$this->moduleNamespace}\ModelManager"
            ],
        ];
    }

    /**
     * @inheritdoc
     */
    public function __construct($id, $parent = null, $config = [])
    {
        foreach ($this->getModuleComponents() as $name => $component) {
            if (!isset($config['components'][$name])) {
                $config['components'][$name] = $component;
            } elseif (is_array($config['components'][$name]) && !isset($config['components'][$name]['class'])) {
                $config['components'][$name]['class'] = $component['class'];
            }
        }

        parent::__construct($id, $parent, $config);
    }
}
