<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace zoco\base;

use Yii;
use yii\web\GroupUrlRule as BaseGroupUrlRule;
use zoco\helpers\frontend\Helper;
use yii\base\InvalidConfigException;
use yii\web\UrlRuleInterface;

/**
 * GroupUrlRule represents a collection of URL rules sharing the same prefix in their patterns and routes.
 *
 * GroupUrlRule is best used by a module which often uses module ID as the prefix for the URL rules.
 * For example, the following code creates a rule for the `admin` module:
 *
 * ```php
 * new GroupUrlRule([
 *     'prefix' => 'admin',
 *     'rules' => [
 *         'login' => 'user/login',
 *         'logout' => 'user/logout',
 *         'dashboard' => 'default/dashboard',
 *     ],
 * ]);
 *
 * // the above rule is equivalent to the following three rules:
 *
 * [
 *     'admin/login' => 'admin/user/login',
 *     'admin/logout' => 'admin/user/logout',
 *     'admin/dashboard' => 'admin/default/dashboard',
 * ]
 * ```
 *
 * The above example assumes the prefix for patterns and routes are the same. They can be made different
 * by configuring [[prefix]] and [[routePrefix]] separately.
 *
 * Using a GroupUrlRule is more efficient than directly declaring the individual rules it contains.
 * This is because GroupUrlRule can quickly determine if it should process a URL parsing or creation request
 * by simply checking if the prefix matches.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class GroupUrlRule extends BaseGroupUrlRule
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        $this->prefix = Yii::t('route', $this->prefix);
        parent::init();
    }

    /**
     * @inheritdoc
     */
    protected function createRules()
    {
        if (Helper::params('allow-multilanguage')) {
            if (Yii::$app->session->has('language')) {
                Yii::$app->language = Yii::$app->session->get('language');
            } else if (isset(Yii::$app->request->cookies['language'])) {
                Yii::$app->language = Yii::$app->request->cookies['language']->value;
            }
        }

        $rules = [];
        foreach ($this->rules as $key => $rule) {
            if (!is_array($rule)) {
                $key = Yii::t('route', $key);
                $rule = [
                    'pattern' => ltrim($this->prefix . '/' . $key, '/'),
                    'route' => ltrim($this->routePrefix . '/' . $rule, '/'),
                ];
            } elseif (isset($rule['pattern'], $rule['route'])) {
                $rule['pattern'] = Yii::t('route', $rule['pattern']);
                $rule['pattern'] = ltrim($this->prefix . '/' . $rule['pattern'], '/');
                $rule['route'] = ltrim($this->routePrefix . '/' . $rule['route'], '/');
            }

            $rule = Yii::createObject(array_merge($this->ruleConfig, $rule));
            if (!$rule instanceof UrlRuleInterface) {
                throw new InvalidConfigException('URL rule class must implement UrlRuleInterface.');
            }
            $rules[] = $rule;
        }
        return $rules;
    }
}
