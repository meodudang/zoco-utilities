<?php

/*
 * This file is part of the ZOCO project.
 *
 * (c) ZOCO project <http://www.bbd.com.vn/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace zoco\base;

use Yii;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use yii\data\Pagination;
use yii\data\ArrayDataProvider;
use yii\helpers\Url;
use yii\helpers\Json;
use yii\helpers\Html;
use yii\helpers\Inflector;
use zoco\helpers\common\Helper;
use zoco\helpers\backend\RestHelper;
use zoco\helpers\common\UploadHelper;

/**
 * ZOCO REST Controller
 * This class is used almost by ZOCO back-end
 *
 * @author Cang Ta <hoksilato176@gmail.com>
 */
class ControllerModel extends Controller
{
    /**
     * @inheritdoc
     */
    protected $modelClass = '';

    /**
     * Model search's class name - must be precise
     * @var string
     */
    protected $modelSearchClass = '';

    /**
     * Model's api controller
     * @var string
     */
    protected $apiPrefix = '';

    /**
     * Module ID
     * @var string
     */
    protected $moduleId = '';

    /**
     * Page size for using in list page
     * @var integer
     */
    protected $pageSize = 15;

    /**
     * Model relations that will get information when return data from API
     * @var array
     */
    protected $withRelation = [];

    /**
     * Get sort attributes from model
     * @var array
     */
    protected $sortingAttributes = [];

    /**
     * Model fields to display in INDEX view
     *
     * Array has the format:
     *
     * ```
     * [
     *     'model_attribute' => [
     *         'label' => 'label will be displayed', // if this element is not set, will get model attribute's label
     *         'class' => 'CSS class',
     *         'filter' => true/false,
     *         'value' => 'value to display', @todo this should be in MODEL Class
     *         'format' => 'value' // @see Formater
     *     ],
     * ]
     * ```
     *
     * @var array
     * @todo implement custom value (anonymous function) for fields
     */
    protected $modelFields = [];

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['post'],
                ],
            ],
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['index', 'create', 'update', 'delete', 'view'],
                        'allow' => true,
                        'roles' => ['@'],
                        // 'matchCallback' => function ($rule, $action) {
                        //     return in_array(Yii::$app->user->identity->email, Yii::$app->getModule('user')->admins);
                        // }
                    ],
                ]
            ]
        ];
    }

    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->apiPrefix = Inflector::id2camel($this->module->apiPrefix);
    }

    /**
     * Lists all {MODEL} models.
     * @return mixed
     * @version 0.0.2
     */
    public function actionIndex()
    {
        // Get sort attributes of model
        $sortingAttributes = [];
        if (!empty($this->sortingAttributes) && isset($this->sortingAttributes['namesort'])) {
            $sortingAttributes = $this->sortingAttributes['namesort'];
        }

        // Get models
        $searchModel = new $this->modelSearchClass;
        $apiResult = $searchModel->search(Yii::$app->request->queryParams);

        if (Yii::$app->request->post('hasEditable')) {
            // Get id of model for saving
            $modelId = Yii::$app->request->post('editableKey');

            // Get data of model
            $model = $this->findModel($modelId);

            // Store a default json response as desired by editable
            $editableResult = Json::encode(['output' => '', 'message' => '']);
            if (isset($_POST)) {
                // Prepare parameters
                $params['params'] = ['id' => $modelId];

                foreach ($_POST as $attribute => $value) {
                    $params['data'][$attribute] = $value;
                }

                // Update model through API
                $editableApiResult = RestHelper::callApi(Inflector::camel2id($this->apiPrefix) . '/update', $params);

                if (isset($editableApiResult) && $editableApiResult['error'] == 0) {
                    // custom output to return to be displayed as the editable grid cell
                    // data. Normally this is empty - whereby whatever value is edited by
                    // in the input by user is updated automatically.
                    $output = '';
                    $modelAttributes = array_keys($model);
                    foreach ($_POST as $attribute => $value) {
                        if (in_array($attribute, $modelAttributes)) {
                            if ($attribute == 'status') {
                                if ($value == 1) {
                                    $icon = 'glyphicon-ok-circle';
                                    $title = 'Active';
                                    $textClass = 'text-primary';
                                } else {
                                    $icon = 'glyphicon-remove-circle';
                                    $title = 'Inactive';
                                    $textClass = 'text-muted';
                                }
                                $output = Html::label('<span class="glyphicon ' . $icon . ' '. $textClass .'" title="' . $title . '" style="cursor: pointer;"></span>');
                            } else {
                                $output = $value;
                            }
                        }
                    }
                    $editableResult = Json::encode(['output' => $output, 'message' => '']);
                } else {
                    $editableResult = Json::encode(['output' => '', 'message' => $editableApiResult['message']]);
                }
            }

            return $editableResult;
        }

        $models = [];
        if (isset($apiResult['error']) && $apiResult['error'] == 0) {
            $models = $apiResult['body'];
        }

        $dataProvider = new ArrayDataProvider([
            'allModels' => $models,
            'key' => 'id',
            'sort' => [
                'attributes' => $sortingAttributes,
            ],
            'pagination' => [
                'pageSize' => $this->pageSize,
            ],
        ]);

        return $this->render('index', [
            'modelFields'   => $this->modelFields,
            'modelInstance' => new $this->modelClass(),
            'dataProvider'  => $dataProvider,
            'searchModel'   => $searchModel,
        ]);
    }

    /**
     * Displays a single {MODEL} model.
     * @param integer $id
     * @return mixed
     */
    public function actionView($id)
    {
        // Get MODEL data
        $object = $this->findModel($id);

        // Create MODEL instance
        $model = RestHelper::mapAttributes($this->modelClass, $object, '', $id);
        return $this->render('view', [
            'model' => $model,
        ]);
    }

    /**
     * Creates a new {MODEL} model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new $this->modelClass();

        // Get default values
        // $model->loadDefaultValues();

        if ($model->load(Yii::$app->request->post())) {
            if ($model->hasProperty('created_by')) {
                $model->created_by = Yii::$app->getUser()->id;
            }

            if ($model->hasProperty('reporter_id')) {
                $model->reporter_id = Yii::$app->getUser()->id;
            }

            if ($model->hasProperty('owner_id')) {
                $model->owner_id = Yii::$app->getUser()->id;
            }

            if ($model->hasProperty('author_id')) {
                $model->author_id = Yii::$app->getUser()->id;
            }

            if ($model->hasProperty('sender_id')) {
                $model->sender_id = Yii::$app->getUser()->id;
            }

            if ($model->validate()) {
                // Get parameters
                $post = Yii::$app->request->post();
                $params['data'] = $post[$this->apiPrefix];

                $result = RestHelper::callApi(Inflector::camel2id($this->apiPrefix) . '/add', $params);

                if ($result['error'] == 0) {
                    // Upload img
                    if (isset($params['data']['image']) && !empty($params['data']['image'])) {
                        $this->uploadImage($params['data']['image']);
                    }

                    Helper::setFlash('success', Helper::getAlertErrors('success', $result['message']));

                    return $this->redirect(['view', 'id' => $result['body']['id']]);
                } else {
                    Helper::setFlash('error', $result['message']);
                }
            } else {
                Helper::setFlash('error', $model->getErrors());
            }
        }

        return $this->render('create', [
            'model' => $model,
        ]);
    }

    /**
     * Updates an existing {MODEL} model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     */
    public function actionUpdate($id)
    {
        // Get MODEL data
        $object = $this->findModel($id);

        // Create MODEL instance
        $model = RestHelper::mapAttributes($this->modelClass, $object, '', $id);

        if ($model->load(Yii::$app->request->post())) {
            if ($model->validate()) {
                $post = Yii::$app->request->post();
                $params['params'] = ['id' => $id];
                $params['data'] = $post[$this->apiPrefix];

                $result = RestHelper::callApi(Inflector::camel2id($this->apiPrefix) . '/update', $params);

                if (isset($result['error']) && $result['error'] == 0) {
                    // Upload img
                    if (isset($params['data']['image']) && !empty($params['data']['image'])) {
                        $this->uploadImage($params['data']['image']);
                    }

                    // Delete image if exist
                    if (!empty($object['image']) && isset($params['data']['image']) && !empty($params['data']['image']) && ($params['data']['image'] !== $object['image'])) {
                        $this->deleteImage($object['image']);
                    }

                    Helper::setFlash('success', Helper::getAlertErrors('success', $result['message']));
                } else {
                    Helper::setFlash('danger', Helper::getAlertErrors('danger', $result['message']));
                }
            } else {
                $errors = $model->getErrors();
                $alertErrors = Helper::getAlertErrors('danger', $errors);
                Helper::setFlash('danger', $alertErrors);
            }
        }

        return $this->redirect(['view', 'id' => $id]);
    }

    /**
     * Deletes an existing {MODEL} model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     */
    public function actionDelete($id)
    {
        $result = RestHelper::callApi(Inflector::camel2id($this->apiPrefix) . '/delete', [
            'params' => ['id' => $id],
        ]);

        if (isset($result['error']) && $result['error'] == 0) {
            Helper::setFlash('success', $result['message']);

            // @todo: this should be done in API
            // Delete image if exist
            // if (!empty($result['body']['image']))
            // {
            //     $this->deleteImage($result['body']['image']);
            // }
        } else {
            Helper::setFlash('error', $result['message']);
        }

        return $this->redirect(['index']);
    }

    /**
     * Finds the {MODEL} model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return {MODEL} the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        $params = [];

        if (!empty($this->withRelation) && isset($this->withRelation['view']) && !empty($this->withRelation['view'])) {
            $params['with'] = $this->withRelation['view'];
        }

        $params['params'] = ['id' => $id];

        $result = RestHelper::callApi(Inflector::camel2id($this->apiPrefix) . '/all', $params);

        if (isset($result['body'])) {
            return array_shift($result['body']); // return first model in API result
        } else {
            throw new NotFoundHttpException('Item not found.');
        }
    }

    /**
     * Delete image if exist
     * @param  string $image image name
     * @return boolean
     */
    protected function deleteImage($image)
    {
        $fileimage = Helper::params('pathUploadTmp') . Yii::$app->getModule(Inflector::camel2id($this->apiPrefix))->uploadDir . '/' . $image;

        if (file_exists($fileimage)) {
            if (!unlink($fileimage)) {
                //TODO: Write log

                return false;
            }
        }

        return true;
    }

    /**
     * upload image if exist
     * @param  string $image image name
     * @return boolean
     */
    protected function uploadImage($image)
    {
        $result = false;
        $sImageData = '';

        $sUploadDir = Yii::$app->getModule(Inflector::camel2id($this->apiPrefix))->uploadDir;

        $sImagePath = Helper::params('pathUploadTmp') . $sUploadDir . '/' . $image;

        $arrTemp = explode('.', $image);
        if (count($arrTemp) == 2) {
            $sFileExtension = $arrTemp[1];
        } else {
            // @TODO return error
        }

        if (file_exists($sImagePath)) {
            $data = file_get_contents($sImagePath);
            $sImageData = base64_encode($data);
        }

        if (!empty($sImageData)) {
            $resultApi = UploadHelper::remoteUpload($sImageData, Helper::params('cdnUpload'), Helper::user()->id, $sFileExtension, $sUploadDir, true, $image);

            if (isset($resultApi['error']) && $resultApi['error'] == 0) {
                $result = true;
            }
        }

        return $result;
    }
}
