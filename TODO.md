CLOSED BETA
===========

**TO DO**

1. Translating EN & VI
  * API error messages
  * Application messages

2. Back-end profile page
  * Apply Smart Admin layout

3. User Admin Index
  * Block modal form
  * Confirm modal form

4. Error handling
  	* Error codes

5. Widgets
  	* GridView using ArrayDataProvider (implements DataProviderInterface methos)