<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $author_id
 * @property string $date
 * @property integer $num_set
 * @property string $author
 */
class SetUser extends Model
{
    public $author_id;
    public $date;
    public $num_set;
    public $author;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'num_set'], 'integer'],
            [['date'], 'safe'],
            [['author'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'author_id' => Yii::t('set-user', 'Author ID'),
            'date' => Yii::t('set-user', 'Date'),
            'num_set' => Yii::t('set-user', 'Number of sets'),
            'author' => Yii::t('set-user', 'Nickname'),
        ];
    }
}
