<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property integer $root
 * @property integer $left
 * @property integer $right
 * @property integer $level
 * @property string $alias
 * @property string $code
 * @property string $title
 * @property string $href
 * @property string $access
 * @property json $options
 * @property json $link_options
 * @property integer $status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property Profile $createdBy
 * @property Profile $updatedBy
 */

class Menu extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $root;
    public $left;
    public $right;
    public $level;
    public $alias;
    public $code;
    public $title;
    public $href;
    public $access;
    public $options;
    public $link_options;
    public $status;
    public $created_by;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['created_by'], 'required'],
            [['title', 'code'], 'required', 'on' => ['insertRoot', 'updateRoot']],
            [['title', 'href'], 'required', 'on' => ['insert', 'update']],
            [['root', 'left', 'right', 'level', 'status', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['code', 'alias'], 'string', 'max' => 50],
            [['title', 'href'], 'string', 'max' => 150],
            [['access'], 'string', 'max' => 20],
            [['code'], 'unique'],
            [['options', 'link_options'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('menu', 'ID'),
            'root' => Yii::t('menu', 'Root'),
            'left' => Yii::t('menu', 'Left'),
            'right' => Yii::t('menu', 'Right'),
            'level' => Yii::t('menu', 'Level'),
            'alias' => Yii::t('menu', 'Alias'),
            'code' => Yii::t('menu', 'Code'),
            'title' => Yii::t('menu', 'Title'),
            'href' => Yii::t('menu', 'Href'),
            'access' => Yii::t('menu', 'Access'),
            'options' => Yii::t('menu', 'Options'),
            'link_options' => Yii::t('menu', 'Link Options'),
            'status' => Yii::t('menu', 'Status'),
            'created_by' => Yii::t('menu', 'Created By'),
            'created_at' => Yii::t('menu', 'Created At'),
            'updated_by' => Yii::t('menu', 'Updated By'),
            'updated_at' => Yii::t('menu', 'Updated At'),
        ];
    }
}
