<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property integer $owner_id
 * @property string $name
 * @property string $cover
 * @property string $description
 * @property integer $is_public
 * @property integer $allow_member_approve
 * @property integer $need_approve
 * @property integer $status
 * @property integer $latest_activity_at
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Conversation[] $conversations
 * @property GroupStatistic[] $groupStatistics
 * @property Profile $owner
 */
class Group extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $owner_id;
    public $name;
    public $cover;
    public $description;
    public $is_public;
    public $allow_member_approve;
    public $need_approve;
    public $status;
    public $latest_activity_at;
    public $created_at;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['owner_id', 'name'], 'required'],
            [['owner_id', 'is_public', 'allow_member_approve', 'need_approve', 'status'], 'integer'],
            [['name', 'description'], 'string'],
            [['latest_activity_at', 'created_at', 'updated_at'], 'safe'],
            [['cover'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('group', 'ID'),
            'owner_id' => Yii::t('group', 'Owner ID'),
            'name' => Yii::t('group', 'Name'),
            'cover' => Yii::t('group', 'Cover'),
            'description' => Yii::t('group', 'Description'),
            'is_public' => Yii::t('group', 'Is Public'),
            'allow_member_approve' => Yii::t('group', 'Allow Member Approve'),
            'need_approve' => Yii::t('group', 'Need Approve'),
            'status' => Yii::t('group', 'Is Show'),
            'latest_activity_at' => Yii::t('group', 'Latest Activity At'),
            'created_at' => Yii::t('group', 'Created At'),
            'updated_at' => Yii::t('group', 'Updated At'),
        ];
    }
}
