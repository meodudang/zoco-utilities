<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property string $words
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property Profile $createdBy
 * @property Profile $updatedBy
 */
class FilterWord extends Model
{
    public $id;
    public $words;
    public $created_by;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['words'], 'required'],
            [['created_by', 'created_at', 'updated_by', 'updated_at'], 'safe'],
            [['words'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('filter-word', 'ID'),
            'words' => Yii::t('filter-word', 'Filtering Words'),
            'created_by' => Yii::t('filter-word', 'Created By'),
            'created_at' => Yii::t('filter-word', 'Created At'),
            'updated_by' => Yii::t('filter-word', 'Updated By'),
            'updated_at' => Yii::t('filter-word', 'Updated At'),
        ];
    }
}
