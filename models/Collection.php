<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property integer $author_id
 * @property integer $owner_id
 * @property integer $category_id
 * @property string $name
 * @property string $slug
 * @property string $json_content
 * @property string $item_ids
 * @property string $set_ids
 * @property string $description
 * @property integer $is_sticky
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property TotalCollectionStatistic $totalCollectionStatistic
 * @property CollectionStatistic[] $collectionStatisticDaily
 * @property Profile $author
 * @property Profile $owner
 * @property Category $category
 * @property Set[] $set
 * @property Item[] $item
 * @property Like[] $like
 */
class Collection extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_PUBLIC = 2;
    const STATUS_DRAFT = 4;

    public $id;
    public $author_id;
    public $owner_id;
    public $category_id;
    public $name;
    public $slug;
    public $json_content;
    public $item_ids;
    public $set_ids;
    public $description;
    public $is_sticky;
    public $status;
    public $created_at;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'name'], 'required'],
            [['author_id', 'owner_id', 'category_id', 'is_sticky', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'description', 'json_content', 'item_ids', 'set_ids'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['slug'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('collection', 'ID'),
            'author_id' => Yii::t('collection', 'Author ID'),
            'owner_id' => Yii::t('collection', 'Owner ID'),
            'category_id' => Yii::t('collection', 'Category ID'),
            'name' => Yii::t('collection', 'Name'),
            'slug' => Yii::t('collection', 'Slug'),
            'description' => Yii::t('collection', 'Description'),
            'json_content' => Yii::t('collection', 'Json Content'),
            'item_ids' => Yii::t('collection', 'Item Ids'),
            'set_ids' => Yii::t('collection', 'Set Ids'),
            'is_sticky' => Yii::t('collection', 'Is Sticky'),
            'status' => Yii::t('collection', 'Status'),
            'created_at' => Yii::t('collection', 'Created At'),
            'updated_at' => Yii::t('collection', 'Updated At'),
        ];
    }
}
