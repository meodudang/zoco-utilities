<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $set_id
 * @property integer $views
 * @property integer $likes
 * @property integer $comments
 * @property integer $collections
 * @property integer $impressions
 * @property integer $emotions
 * @property integer $shares
 * @property integer $delicates
 * @property integer $splendids
 * @property integer $personalities
 * @property integer $uniques
 * @property integer $rustics
 * @property integer $engagings
 * @property string $date
 * @property integer $updated_at
 *
 * @property Set $set
 */
class SetStatistic extends Model
{
    public $set_id;
    public $views;
    public $likes;
    public $comments;
    public $collections;
    public $impressions;
    public $emotions;
    public $shares;
    public $delicates;
    public $splendids;
    public $personalities;
    public $uniques;
    public $rustics;
    public $engagings;
    public $date;
    public $updated_at;

    /**
     * Relation
     * @var array
     */
    public $set;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['set_id', 'date'], 'required'],
            [['set_id', 'views', 'likes', 'comments', 'impressions', 'emotions', 'shares', 'collections', 'delicates', 'splendids', 'personalities', 'uniques', 'rustics', 'engagings', 'updated_at'], 'integer'],
            [['set_id', 'date'], 'unique', 'targetAttribute' => ['set_id', 'date'], 'message' => 'The combination of Set ID, Date has already been taken.'],
            [['date', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'set_id' => Yii::t('set-statistic', 'Set ID'),
            'views' => Yii::t('set-statistic', 'Views'),
            'likes' => Yii::t('set-statistic', 'Likes'),
            'comments' => Yii::t('set-statistic', 'Comments'),
            'collections' => Yii::t('set-statistic', 'Collections'),
            'impressions' => Yii::t('set-statistic', 'Impressions'),
            'emotions' => Yii::t('set-statistic', 'Emotions'),
            'shares' => Yii::t('set-statistic', 'Shares'),
            'delicates' => Yii::t('set-statistic', 'Delicates'),
            'splendids' => Yii::t('set-statistic', 'Splendids'),
            'personalities' => Yii::t('set-statistic', 'Personalities'),
            'uniques' => Yii::t('set-statistic', 'Uniques'),
            'rustics' => Yii::t('set-statistic', 'Rustics'),
            'engagings' => Yii::t('set-statistic', 'engagings'),
            'date' => Yii::t('set-statistic', 'Date'),
            'updated_at' => Yii::t('set-statistic', 'Updated At'),
        ];
    }
}
