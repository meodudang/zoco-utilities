<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property string $name
 * @property integer $votes
 * @property integer $language_id
 * @property integer $status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property Profile[] $profiles
 * @property Profile $createdBy
 * @property Profile $updatedBy
 * @property Language $language
 */
class UserType extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $name;
    public $votes;
    public $language_id;
    public $status;
    public $created_by;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'votes', 'language_id', 'created_by'], 'required'],
            [['votes', 'language_id', 'status', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user-type', 'ID'),
            'name' => Yii::t('user-type', 'Name'),
            'votes' => Yii::t('user-type', 'Votes'),
            'language_id' => Yii::t('user-type', 'Language ID'),
            'status' => Yii::t('user-type', 'Status'),
            'created_by' => Yii::t('user-type', 'Created By'),
            'created_at' => Yii::t('user-type', 'Created At'),
            'updated_by' => Yii::t('user-type', 'Updated By'),
            'updated_at' => Yii::t('user-type', 'Updated At'),
        ];
    }
}
