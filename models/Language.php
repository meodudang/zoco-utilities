<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property integer $status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property BadReportType[] $badReportTypes
 * @property Country[] $countries
 * @property Profile $createdBy
 * @property Profile $updatedBy
 * @property SystemSetting[] $systemSettings
 * @property UserType[] $userTypes
 */
class Language extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $name;
    public $code;
    public $status;
    public $created_by;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code', 'created_by'], 'required'],
            [['status', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 30],
            [['code'], 'string', 'max' => 5]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('language', 'ID'),
            'name' => Yii::t('language', 'Name'),
            'code' => Yii::t('language', 'Code'),
            'status' => Yii::t('language', 'Status'),
            'created_by' => Yii::t('language', 'Created By'),
            'created_at' => Yii::t('language', 'Created At'),
            'updated_by' => Yii::t('language', 'Updated By'),
            'updated_at' => Yii::t('language', 'Updated At'),
        ];
    }
}
