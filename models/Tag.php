<?php
namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $type
 * @property integer $status
 *
 * @property Item[] $items
 * @property Set[] $sets
 * @property Collection[] $collections
 */
class Tag extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $tag_type_id;
    public $name;
    public $status;
    public $show_at_header;
    public $language;
    public $created_at;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['name'], 'string'],
            [['status', 'created_at', 'updated_at', 'show_at_header', 'tag_type_id', 'language'], 'integer'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('tag', 'ID'),
            'tag_type_id' => Yii::t('tag', 'Tag Type'),
            'name' => Yii::t('tag', 'Name'),
            'status' => Yii::t('tag', 'Status'),
            'show_at_header'=>Yii::t('tag', 'Show At Header'),
            'language'=>Yii::t('tag', 'Language'),
            'created_at'=>Yii::t('tag', 'Created At'),
            'updated_at'=>Yii::t('tag', 'Updated At'),
        ];
    }
}
