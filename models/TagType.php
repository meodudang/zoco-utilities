<?php
namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property string $name
 * @property string $object_type
 * @property string $created_at
 * @property integer $updated_at
 */
class TagType extends Model
{
    const OBJECT_ITEM = 'item';
    const OBJECT_SET = 'set';
    const OBJECT_COLLECTION = 'collection';

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $name;
    public $object_type;
    public $language;
    public $status;
    public $created_at;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'object_type'], 'required'],
            [['created_at', 'updated_at', 'status', 'language'], 'integer'],
            [['object_type'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('tag', 'ID'),
            'name' => Yii::t('tag', 'Name'),
            'object_type'=>Yii::t('tag', 'Object Type'),
            'language'=>Yii::t('tag', 'Language'),
            'status'=>Yii::t('tag', 'Status'),
            'created_at'=>Yii::t('tag', 'Created At'),
            'updated_at'=>Yii::t('tag', 'Update At'),
        ];
    }
}
