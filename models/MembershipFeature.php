<?php

namespace zoco\models;

use Yii;
use yii\base\Model;
use yii\db\Expression;
use yii\behaviors\TimestampBehavior;

/**
 * @property integer $id
 * @property string $name
 * @property integer $type
 * @property string $input_type
 * @property integer $status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property Profile $user
 * @property Profile $createdBy
 * @property Profile $updatedBy
 * @property MembershipType $membershipType
 */
class MembershipFeature extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $name;
    public $type;
    public $input_type;
    public $status;
    public $created_by;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['type', 'status', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 200],
            [['input_type'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('membership-feature', 'ID'),
            'name' => Yii::t('membership-feature', 'Name'),
            'type' => Yii::t('membership-feature', 'Type'),
            'input_type' => Yii::t('membership-feature', 'Input Type'),
            'status' => Yii::t('membership-feature', 'Status'),
            'created_by' => Yii::t('membership-feature', 'Created By'),
            'created_at' => Yii::t('membership-feature', 'Created At'),
            'updated_by' => Yii::t('membership-feature', 'Updated By'),
            'updated_at' => Yii::t('membership-feature', 'Updated At'),
        ];
    }
}
