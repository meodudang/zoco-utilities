<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property string $date
 * @property integer $num_new_user
 * @property integer $num_new_active
 */
class NewUser extends Model
{
    public $date;
    public $num_new_user;
    public $num_new_active;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['num_new_user', 'num_new_active'], 'integer'],
            [['date'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'date' => Yii::t('new-user', 'Date'),
            'num_new_user' => Yii::t('new-user', 'Number of new users'),
            'num_new_active' => Yii::t('new-user', 'Number of new activated users'),
        ];
    }
}
