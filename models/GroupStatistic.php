<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $group_id
 * @property integer $conversations
 * @property integer $members
 * @property integer $comments
 * @property integer $date
 * @property integer $updated_at
 *
 * @property Group $group
 */
class GroupStatistic extends Model
{
    public $group_id;
    public $conversations;
    public $members;
    public $comments;
    public $date;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['group_id', 'conversations', 'members', 'comments', 'date'], 'required'],
            [['group_id', 'conversations', 'members', 'comments'], 'integer'],
            [['group_id', 'date'], 'unique', 'targetAttribute' => ['group_id', 'date'], 'message' => 'The combination of Group ID, Date has already been taken.'],
            [['date', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'group_id' => Yii::t('group-statistic', 'Group ID'),
            'conversations' => Yii::t('group-statistic', 'Conversations'),
            'members' => Yii::t('group-statistic', 'Members'),
            'comments' => Yii::t('group-statistic', 'Comments'),
            'date' => Yii::t('group-statistic', 'Date'),
            'updated_at' => Yii::t('group-statistic', 'Updated At'),
        ];
    }
}
