<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $who_banned
 * @property integer $is_permanent
 * @property integer $from
 * @property integer $to
 * @property string $reason
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property Profile $user
 * @property Profile $whoBanned
 * @property Profile $updatedBy
 */
class Ban extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $user_id;
    public $who_banned;
    public $is_permanent;
    public $from;
    public $to;
    public $reason;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public $user;

    /**
     * @inheritdoc
     */
    public $whoBanned;

    /**
     * @inheritdoc
     */
    public $updatedBy;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id'], 'required'],
            [['user_id', 'who_banned', 'is_permanent', 'from', 'to', 'updated_by'], 'integer'],
            [['from', 'to', 'created_at', 'updated_at'], 'safe'],
            [['reason'], 'string']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('ban', 'ID'),
            'user_id' => Yii::t('ban', 'User ID'),
            'who_banned' => Yii::t('ban', 'Who Banned'),
            'is_permanent' => Yii::t('ban', 'Is Permanent'),
            'from' => Yii::t('ban', 'From'),
            'to' => Yii::t('ban', 'To'),
            'reason' => Yii::t('ban', 'Reason'),
            'created_at' => Yii::t('ban', 'Created At'),
            'updated_by' => Yii::t('ban', 'Updated By'),
            'updated_at' => Yii::t('ban', 'Updated At'),
        ];
    }
}
