<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property string $name
 * @property string $description
 * @property integer $status
 * @property integer $created_by
 * @property integer $updated_by
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Profile $createdBy
 * @property Profile $updatedBy
 * @property Setting[] $settings
 * @property SystemSetting[] $systemSettings
 */
class Module extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $name;
    public $description;
    public $status;
    public $created_by;
    public $updated_by;
    public $created_at;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['status', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('module', 'ID'),
            'name' => Yii::t('module', 'Name'),
            'description' => Yii::t('module', 'Description'),
            'status' => Yii::t('module', 'Status'),
            'created_by' => Yii::t('module', 'Created By'),
            'updated_by' => Yii::t('module', 'Updated By'),
            'created_at' => Yii::t('module', 'Created At'),
            'updated_at' => Yii::t('module', 'Updated At'),
        ];
    }
}
