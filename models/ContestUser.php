<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $contest_id
 * @property integer $rank
 * @property string $message
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Profile $user
 * @property Contest $contest
 */
class ContestUser extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $user_id;
    public $contest_id;
    public $rank;
    public $message;
    public $status;
    public $created_at;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'contest_id'], 'required'],
            [['rank', 'status', 'created_at', 'updated_at'], 'integer'],
            ['message', 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('contest-user', 'ID'),
            'user_id' => Yii::t('contest-user', 'Title'),
            'contest_id' => Yii::t('contest-user', 'Image'),
            'rank' => Yii::t('contest-user', 'Rank'),
            'message' => Yii::t('contest-user', 'Message'),
            'status' => Yii::t('contest-user', 'Status'),
            'created_at' => Yii::t('contest-user', 'Created At'),
            'updated_at' => Yii::t('contest-user', 'Updated At'),
        ];
    }
}
