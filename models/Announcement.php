<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property string $title
 * @property string $slug
 * @property string $content
 * @property string $publish_at
 * @property integer $language_id
 * @property integer $status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property Profile $createdBy
 * @property Profile $updatedBy
 * @property Language $language
 */
class Announcement extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $title;
    public $slug;
    public $content;
    public $publish_at;
    public $language_id;
    public $status;
    public $created_by;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * Relation
     * @var array
     */
    public $createdBy;

    /**
     * Relation
     * @var array
     */
    public $updatedBy;

    /**
     * Relation
     * @var array
     */
    public $language;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['title', 'content', 'language_id', 'created_by'], 'required'],
            [['language_id', 'publish_at', 'status', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['content'], 'string'],
            [['publish_at', 'created_at', 'updated_at'], 'safe'],
            [['title', 'slug'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('announcement', 'ID'),
            'title' => Yii::t('announcement', 'Title'),
            'slug' => Yii::t('announcement', 'Slug'),
            'content' => Yii::t('announcement', 'Content'),
            'publish_at' => Yii::t('announcement', 'Publish At'),
            'language_id' => Yii::t('announcement', 'Language'),
            'status' => Yii::t('announcement', 'Status'),
            'created_by' => Yii::t('announcement', 'Created By'),
            'created_at' => Yii::t('announcement', 'Created At'),
            'updated_by' => Yii::t('announcement', 'Updated By'),
            'updated_at' => Yii::t('announcement', 'Updated At'),
        ];
    }
}
