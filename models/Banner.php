<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property string $title
 * @property string $image
 * @property string $link
 * @property integer $position
 * @property integer $status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property Profile $createdBy
 * @property Profile $updatedBy
 */
class Banner extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $title;
    public $image;
    public $link;
    public $position;
    public $status = 1;
    public $created_by;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * Relation
     * @var array
     */
    public $createdBy;

    /**
     * Relation
     * @var array
     */
    public $updatedBy;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['image', 'position', 'created_by'], 'required'],
            [['status', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['image', 'title', 'link'], 'string', 'max' => 255],
            [['position'], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('banner', 'ID'),
            'title' => Yii::t('banner', 'title'),
            'image' => Yii::t('banner', 'Image'),
            'link' => Yii::t('banner', 'Link'),
            'position' => Yii::t('banner', 'Position'),
            'status' => Yii::t('banner', 'Status'),
            'created_by' => Yii::t('banner', 'Created By'),
            'created_at' => Yii::t('banner', 'Created At'),
            'updated_by' => Yii::t('banner', 'Updated By'),
            'updated_at' => Yii::t('banner', 'Updated At'),
        ];
    }
}
