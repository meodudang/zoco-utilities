<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property integer $date_start
 * @property integer $date_end
 * @property string $rule
 * @property string $prize
 * @property string $num_prize
 * @property string $img_prize
 * @property string $img_contest
 * @property string $cover_img
 * @property integer $allow_old_set
 * @property integer $sets_per_user
 * @property integer $min_votes
 * @property integer $judging_mode
 * @property integer $allow_comment
 * @property integer $type
 * @property integer $is_sticky
 * @property integer $is_hot
 * @property string $result
 * @property integer $status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property Profile $createdBy
 * @property Profile $updatedBy
 * @property ContestSet[] $contestSets
 * @property EventUser $eventUsers
 */
class Contest extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ON_GOING = 1;
    const STATUS_UP_COMING = 2;
    const STATUS_THE_END = 3;

    const METHOD_AUTO = 0;
    const METHOD_HANDLE = 1;

    const TYPE_CONTEST = 0;
    const TYPE_ACTIVITY = 1;
    const TYPE_OUTTER = 2;

    const NOT_HOT = 0;
    const IS_HOT = 1;
    const NOT_STICKY = 0;
    const IS_STICKY = 1;

    public $id;
    public $name;
    public $slug;
    public $description;
    public $date_start;
    public $date_end;
    public $rule;
    public $prize;
    public $num_prize;
    public $img_prize;
    public $img_contest;
    public $cover_img;
    public $allow_old_set;
    public $sets_per_user;
    public $min_votes;
    public $judging_mode;
    public $allow_comment;
    public $type;
    public $is_sticky;
    public $is_hot;
    public $result;
    public $status;
    public $created_by;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'description', 'date_start', 'date_end', 'rule', 'prize', 'num_prize', 'img_prize', 'img_contest', 'created_by'], 'required'],
            [['date_start', 'date_end', 'num_prize', 'allow_old_set', 'sets_per_user', 'min_votes', 'judging_mode', 'allow_comment', 'type', 'is_sticky', 'is_hot', 'status', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['rule', 'prize', 'description', 'result'], 'string'],
            [['img_prize', 'img_contest', 'cover_img'], 'string', 'max'=>200],
            [['name'], 'string', 'min' => 5, 'max' => 200],
            [['name'], 'filter', 'filter' => 'trim'],
            [['name'], 'unique'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('contest', 'ID'),
            'name' => Yii::t('contest', 'Name'),
            'slug' => Yii::t('contest', 'Slug'),
            'description' => Yii::t('contest', 'Description'),
            'date_start' => Yii::t('contest', 'Date Start'),
            'date_end' => Yii::t('contest', 'Date End'),
            'rule' => Yii::t('contest', 'Rule'),
            'prize' => Yii::t('contest', 'Prize'),
            'num_prize' => Yii::t('contest', 'Num Prize'),
            'img_prize' => Yii::t('contest', 'Prize Image'),
            'img_contest' => Yii::t('contest', 'Contest Image'),
            'cover_img' => Yii::t('contest', 'Cover Image'),
            'allow_old_set' => Yii::t('contest', 'Allow Old Set'),
            'sets_per_user' => Yii::t('contest', 'Sets/user'),
            'min_votes' => Yii::t('contest', 'Min Votes'),
            'judging_mode' => Yii::t('contest', 'Judging Method'),
            'allow_comment' => Yii::t('contest', 'Allow Comment'),
            'type' => Yii::t('contest', 'Type'),
            'is_sticky' => Yii::t('contest', 'Is Sticky'),
            'is_hot' => Yii::t('contest', 'Is Hot'),
            'result' => Yii::t('contest', 'Result'),
            'status' => Yii::t('contest', 'Status'),
            'created_at' => Yii::t('contest', 'Created At'),
            'updated_at' => Yii::t('contest', 'Updated At'),
            'created_by' => Yii::t('contest', 'Created By'),
            'updated_by' => Yii::t('contest', 'Updated By'),
        ];
    }
}
