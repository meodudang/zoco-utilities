<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property integer $contest_id
 * @property integer $author_id
 * @property integer $set_id
 * @property string $json_content
 * @property string $item_ids
 * @property string $image
 * @property integer $votes
 * @property integer $rank
 * @property integer $is_violated
 * @property integer $num_of_violations
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Profile $author
 * @property Set $set
 * @property Contest $contest
 */
class ContestSet extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $contest_id;
    public $author_id;
    public $set_id;
    public $json_content;
    public $item_ids;
    public $image;
    public $votes;
    public $rank;
    public $is_violated;
    public $num_of_violations;
    public $status;
    public $created_at;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contest_id', 'author_id', 'set_id'], 'required'],
            [['contest_id', 'author_id', 'set_id', 'votes', 'rank', 'is_violated', 'num_of_violations', 'status', 'created_at', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['json_content', 'item_ids'], 'string'],
            [['image'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('contest-set', 'ID'),
            'contest_id' => Yii::t('contest-set', 'Contest ID'),
            'author_id' => Yii::t('contest-set', 'Author ID'),
            'set_id' => Yii::t('contest-set', 'Set ID'),
            'json_content' => Yii::t('contest-set', 'Json Content'),
            'item_ids' => Yii::t('contest-set', 'Item Ids'),
            'image' => Yii::t('contest-set', 'Image'),
            'votes' => Yii::t('contest-set', 'Votes'),
            'rank' => Yii::t('contest-set', 'Rank'),
            'is_violated' => Yii::t('contest-set', 'Is Violated'),
            'num_of_violations' => Yii::t('contest-set', 'Num Of Violations'),
            'status' => Yii::t('contest-set', 'Status'),
            'created_at' => Yii::t('contest-set', 'Created At'),
            'updated_at' => Yii::t('contest-set', 'Updated At'),
        ];
    }
}
