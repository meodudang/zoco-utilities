<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $author_id
 * @property string $date
 * @property integer $num_item
 * @property string $author
 */
class ClipUser extends Model
{
    public $author_id;
    public $date;
    public $num_item;
    public $author;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'num_item'], 'integer'],
            [['date'], 'safe'],
            [['author'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'author_id' => Yii::t('clip-user', 'Author ID'),
            'date' => Yii::t('clip-user', 'Date'),
            'num_item' => Yii::t('clip-user', 'Number of items'),
            'author' => Yii::t('clip-user', 'Nickname'),
        ];
    }
}
