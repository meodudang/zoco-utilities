<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property string $name
 * @property integer $status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property BadReportType[] $badReportTypes
 * @property Profile $createdBy
 * @property Profile $updatedBy
 */
class ObjectType extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $name;
    public $status;
    public $created_by;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'created_by'], 'required'],
            [['status', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 255],
            [['name'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('object-type', 'ID'),
            'name' => Yii::t('object-type', 'Name'),
            'status' => Yii::t('object-type', 'Status'),
            'created_by' => Yii::t('object-type', 'Created By'),
            'created_at' => Yii::t('object-type', 'Created At'),
            'updated_by' => Yii::t('object-type', 'Updated By'),
            'updated_at' => Yii::t('object-type', 'Updated At'),
        ];
    }
}
