<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property integer $user_id
 * @property string $image
 * @property integer $order
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property Profile $user
 * @property Profile $updatedBy
 */
class Gallery extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $user_id;
    public $image;
    public $order;
    public $status;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'image'], 'required'],
            [['user_id', 'order', 'status', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['image'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('gallery', 'ID'),
            'user_id' => Yii::t('gallery', 'User ID'),
            'image' => Yii::t('gallery', 'Image'),
            'order' => Yii::t('gallery', 'Order'),
            'status' => Yii::t('gallery', 'Status'),
            'created_at' => Yii::t('gallery', 'Created At'),
            'updated_by' => Yii::t('gallery', 'Updated By'),
            'updated_at' => Yii::t('gallery', 'Updated At'),
        ];
    }
}
