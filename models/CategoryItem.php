<?php
namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $item_id
 * @property integer $category_id
 *
 * @property Item $item
 * @property Category $category
 */
class CategoryItem extends Model
{
    public $item_id;
    public $category_id;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'category_id'], 'required'],
            [['item_id', 'category_id'], 'integer'],
            [['item_id', 'category_id'], 'unique', 'targetAttribute' => ['item_id', 'category_id'], 'message' => 'The combination of Item ID, Category ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_id' => Yii::t('category', 'Item ID'),
            'category_id' => Yii::t('category', 'Category ID'),
        ];
    }
}
