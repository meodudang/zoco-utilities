<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $collection_id
 * @property integer $views
 * @property integer $likes
 * @property integer $impressions
 * @property integer $emotions
 * @property integer $shares
 * @property integer $comments
 * @property string $date
 * @property integer $updated_at
 *
 * @property Collection $collection
 */
class CollectionStatistic extends Model
{
    public $collection_id;
    public $views;
    public $likes;
    public $impressions;
    public $emotions;
    public $shares;
    public $comments;
    public $date;
    public $updated_at;

    /**
     * Relation
     * @var array
     */
    public $collection;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['collection_id', 'date'], 'required'],
            [['collection_id', 'views', 'likes', 'impressions', 'emotions', 'shares', 'comments', 'updated_at'], 'integer'],
            [['collection_id', 'date'], 'unique', 'targetAttribute' => ['collection_id', 'date'], 'message' => 'The combination of Collection ID, Date has already been taken.'],
            [['date', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'collection_id' => Yii::t('collection-statistic', 'Collection ID'),
            'views' => Yii::t('collection-statistic', 'Views'),
            'likes' => Yii::t('collection-statistic', 'Likes'),
            'impressions' => Yii::t('collection-statistic', 'Impressions'),
            'emotions' => Yii::t('collection-statistic', 'Emotions'),
            'shares' => Yii::t('collection-statistic', 'Shares'),
            'comments' => Yii::t('collection-statistic', 'Comments'),
            'date' => Yii::t('collection-statistic', 'Date'),
            'updated_at' => Yii::t('collection-statistic', 'Updated At'),
        ];
    }
}
