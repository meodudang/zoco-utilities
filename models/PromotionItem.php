<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property integer $promotion_id
 * @property integer $item_id
 * @property integer $percent_sale_off
 * @property integer $sale_off
 * @property integer $type
 * @property integer $created_at
 *
 * @property Item $item
 * @property Promotion $promotion
 */
class PromotionItem extends Model
{
    public $id;
    public $item_id;
    public $promotion_id;
    public $percent_sale_off;
    public $sale_off;
    public $type;
    public $created_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'promotion_id'], 'required'],
            [['item_id', 'promotion_id', 'type', 'created_at'], 'integer'],
            [['sale_off', 'percent_sale_off'], 'number'],
            [['created_at'], 'safe'],
            [['item_id', 'promotion_id'], 'unique', 'targetAttribute' => ['item_id', 'promotion_id'], 'message' => 'The combination of Item ID, Promotion ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('promotion-item', 'ID'),
            'promotion_id' => Yii::t('promotion-item', 'Promotion ID'),
            'item_id' => Yii::t('promotion-item', 'Item ID'),
            'sale_off' => Yii::t('promotion-item', 'Sale Off Price'),
            'percent_sale_off' => Yii::t('promotion-item', 'Percent Sale Off'),
            'type' => Yii::t('promotion-item', 'Type'),
            'created_at' => Yii::t('promotion-item', 'Created At'),
        ];
    }
}
