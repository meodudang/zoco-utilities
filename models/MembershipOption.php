<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $membership_type_id
 * @property integer $membership_feature_id
 * @property string $values
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property Profile $updatedBy
 * @property MembershipType $membershipType
 * @property MembershipFeature $membershipFeature
 */
class MembershipOption extends Model
{
    public $membership_type_id;
    public $membership_feature_id;
    public $values;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['membership_type_id', 'membership_feature_id'], 'required'],
            [['membership_feature_id', 'membership_type_id', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['value'], 'string', 'max' => 150],
            [['membership_type_id', 'membership_feature_id'], 'unique', 'targetAttribute' => ['membership_type_id', 'membership_feature_id'], 'message' => 'The combination of Membership ID, Membership Feature ID has already been taken.'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'membership_type_id' => Yii::t('membership-option', 'Membership Type ID'),
            'membership_feature_id' => Yii::t('membership-option', 'Membership Feature ID'),
            'values' => Yii::t('membership-option', 'Values'),
            'created_at' => Yii::t('membership-option', 'Created At'),
            'updated_by' => Yii::t('membership-option', 'Updated By'),
            'updated_at' => Yii::t('membership-option', 'Updated At'),
        ];
    }
}
