<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property integer $language_id
 * @property integer $module_id
 * @property string $key
 * @property string $name
 * @property string $value
 * @property string $description
 * @property integer $start
 * @property integer $end
 * @property integer $status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property Language $language
 * @property Module $module
 * @property Profile $createdBy
 * @property Profile $updatedBy
 */
class SystemSetting extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $language_id;
    public $module_id;
    public $key;
    public $name;
    public $value;
    public $description;
    public $start;
    public $end;
    public $status;
    public $created_by;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'key', 'value', 'language_id', 'module_id', 'created_by'], 'required'],
            [['language_id', 'module_id', 'start', 'end', 'status', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['start', 'end', 'created_at', 'updated_at'], 'safe'],
            [['key', 'value'], 'string', 'max' => 30],
            [['name'], 'string', 'max' => 50],
            [['language_id', 'module_id', 'key'], 'unique', 'targetAttribute' => ['language_id', 'module_id', 'key'], 'message' => 'The combination of Language ID, Module ID and Key has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('system-setting', 'ID'),
            'language_id' => Yii::t('system-setting', 'Language ID'),
            'module_id' => Yii::t('system-setting', 'Module ID'),
            'key' => Yii::t('system-setting', 'Key'),
            'name' => Yii::t('system-setting', 'Name'),
            'value' => Yii::t('system-setting', 'Value'),
            'description' => Yii::t('system-setting', 'Description'),
            'start' => Yii::t('system-setting', 'Start'),
            'end' => Yii::t('system-setting', 'End'),
            'status' => Yii::t('system-setting', 'Status'),
            'created_by' => Yii::t('system-setting', 'Created By'),
            'created_at' => Yii::t('system-setting', 'Created At'),
            'updated_by' => Yii::t('system-setting', 'Updated By'),
            'updated_at' => Yii::t('system-setting', 'Updated At'),
        ];
    }
}
