<?php


namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property integer $author_id
 * @property integer $group_id
 * @property string $message
 * @property integer $is_pin
 * @property string $latest_comment_at
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Profile $author
 * @property Group $group
 */
class Conversation extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $author_id;
    public $group_id;
    public $message;
    public $is_pin;
    public $latest_comment_at;
    public $status;
    public $created_at;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'group_id'], 'required'],
            [['author_id', 'group_id', 'status', 'is_pin'], 'integer'],
            [['message'], 'string'],
            [['latest_comment_at', 'created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('conversation', 'ID'),
            'author_id' => Yii::t('conversation', 'Author ID'),
            'group_id' => Yii::t('conversation', 'Group ID'),
            'message' => Yii::t('conversation', 'Message'),
            'is_pin' => Yii::t('conversation', 'Is Pin'),
            'latest_comment_at' => Yii::t('conversation', 'Latest Comment At'),
            'status' => Yii::t('conversation', 'Status'),
            'created_at' => Yii::t('conversation', 'Created At'),
            'updated_at' => Yii::t('conversation', 'Updated At'),
        ];
    }
}
