<?php
namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class fo REDIS table "like_objects".
 *
 * @property integer $id
 * @property integer $tag_id
 * @property integer $object_id
 * @property string $object_type
 * @property integer $created_at
 *
 * @property Item $item
 * @property Set $set
 * @property Collection $collection
 * @property Tag $tag
 */
class TagObject extends Model
{
    const OBJECT_ITEM = 'item';
    const OBJECT_SET = 'set';
    const OBJECT_COLLECTION = 'collection';

    public $id;
    public $tag_id;
    public $object_id;
    public $object_type;
    public $created_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['tag_id', 'object_type', 'object_id'], 'required'],
            [['tag_id', 'object_id','created_at'], 'integer'],
            [['object_type' ], 'string', 'max' => 20],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('tag-object', 'ID'),
            'tag_id' => Yii::t('tag-object', 'Tag ID'),
            'object_id' => Yii::t('tag-object', 'Object ID'),
            'object_type' => Yii::t('tag-object', 'Object Type'),
            'created_at' => Yii::t('tag-object', 'Created At'),
        ];
    }
}
