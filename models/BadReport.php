<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property integer $reporter_id
 * @property integer $bad_report_type_id
 * @property integer $object_id
 * @property string $object_type
 * @property string $object_name
 * @property string $description
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property Profile $reporter
 * @property Profile $updatedBy
 * @property BadReportType $badReportType
 */
class BadReport extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $reporter_id;
    public $bad_report_type_id;
    public $object_id;
    public $object_type;
    public $object_name;
    public $description;
    public $status;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['reporter_id', 'object_id', 'object_name', 'bad_report_type_id'], 'required'],
            [['reporter_id', 'object_id', 'bad_report_type_id', 'status', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['object_name', 'description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['object_type'], 'string', 'max' => 255],
            [['reporter_id', 'object_id', 'bad_report_type_id'], 'unique', 'targetAttribute' => ['reporter_id', 'object_id', 'bad_report_type_id'], 'message' => 'The combination of Reporter ID, Object ID and Bad Report Type ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('bad-report', 'ID'),
            'reporter_id' => Yii::t('bad-report', 'Reporter ID'),
            'object_type' => Yii::t('bad-report', 'Object Type'),
            'object_id' => Yii::t('bad-report', 'Object ID'),
            'object_name' => Yii::t('bad-report', 'Object Name'),
            'bad_report_type_id' => Yii::t('bad-report', 'Bad Report Type ID'),
            'description' => Yii::t('bad-report', 'Description'),
            'status' => Yii::t('bad-report', 'Status'),
            'created_at' => Yii::t('bad-report', 'Created At'),
            'updated_by' => Yii::t('bad-report', 'Updated By'),
            'updated_at' => Yii::t('bad-report', 'Updated At'),
        ];
    }
}
