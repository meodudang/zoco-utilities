<?php

namespace zoco\models;

use Yii;
use yii\base\Model;
use vova07\fileapi\behaviors\UploadBehavior;

/**
 * @property integer $id
 * @property integer $root
 * @property integer $left
 * @property integer $right
 * @property integer $level
 * @property string $name
 * @property string $code
 * @property string $slug
 * @property string $image
 * @property string $description
 * @property integer $show_at_header
 * @property string $show_at_mix
 * @property integer $is_keyword
 * @property integer $status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property Profile $createdBy
 * @property Profile $updatedBy
 * @property Item[] $items
 */
class Category extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $root;
    public $left;
    public $right;
    public $level;
    public $name;
    public $code;
    public $slug;
    public $image;
    public $description;
    public $show_at_header;
    public $show_at_mix;
    public $is_keyword;
    public $status;
    public $created_by;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['root', 'left', 'right', 'level', 'status', 'show_at_header', 'is_keyword', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'slug', 'image'], 'string', 'max' => 255],
            [['show_at_mix'], 'string', 'max' => 20],
            [['code'], 'string', 'max' => 50],
            [['code'], 'required', 'on' => ['insertRoot', 'updateRoot']],
            [['code'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('category', 'ID'),
            'root' => Yii::t('category', 'Root'),
            'left' => Yii::t('category', 'Left'),
            'right' => Yii::t('category', 'Right'),
            'level' => Yii::t('category', 'Level'),
            'name' => Yii::t('category', 'Name'),
            'code' => Yii::t('category', 'Code'),
            'slug' => Yii::t('category', 'Slug'),
            'image' => Yii::t('category', 'Image'),
            'description' => Yii::t('category', 'Description'),
            'status' => Yii::t('category', 'Status'),
            'show_at_header' => Yii::t('category', 'Show At Header'),
            'show_at_mix' => Yii::t('category', 'Show At Mix'),
            'is_keyword' => Yii::t('category', 'Is Keyword'),
            'created_by' => Yii::t('category', 'Created By'),
            'created_at' => Yii::t('category', 'Created At'),
            'updated_by' => Yii::t('category', 'Updated By'),
            'updated_at' => Yii::t('category', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'uploadBehavior' => [
                'class' => UploadBehavior::className(),
                'attributes' => [
                    'image' => [
                        'path' => Yii::$app->params['pathUploadTmp'] . $this->module->uploadDir,
                        'tempPath' => Yii::$app->params['pathUploadTmp'] . $this->module->uploadDir,
                        'url' => Yii::$app->params['pathUploadUrl'] . $this->module->uploadDir,
                    ]
                ]
            ]
        ];
    }
}
