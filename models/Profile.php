<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $user_id
 * @property string $nickname
 * @property string $avatar
 * @property string $about_me
 * @property string $profile_url
 * @property integer $user_type_id
 * @property integer $language_id
 * @property integer $is_superuser
 * @property integer $membership_type_id
 * @property integer $block_to
 * @property integer $updated_at
 *
 * @property Language $language
 * @property UserType $userType
 * @property MembershipType $membershipType
 * @property Set[] $sets
 * @property Collection[] $collections
 * @property Item[] $items
 * @property LikeItem[] $likeItems
 * @property LikeSet[] $likeSets
 * @property LikeCollection[] $likeCollections
 * @property CartItem[] $cartItems
 * @property Follower[] $followers
 * @property Follower[] $followings
 */
class Profile extends Model
{
    public $user_id;
    public $nickname;
    public $avatar;
    public $about_me;
    public $profile_url;
    public $user_type_id;
    public $language_id;
    public $is_superuser;
    public $membership_type_id;
    public $block_to;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'nickname'], 'required'],
            [['user_id', 'user_type_id', 'language_id', 'is_superuser', 'membership_type_id', 'updated_at'], 'integer'],
            [['block_to', 'updated_at'], 'safe'],
            [['about_me', 'profile_url'], 'string'],
            [['avatar'], 'string', 'max' => 255],
            [['profile_url'], 'string', 'max' => 15],
            [['nickname'], 'match', 'pattern' => '/^[a-z0-9A-Z_ÀÁÂÃÈÉÊÌÍÒÓÔÕÙÚĂĐĨŨƠàáâãèéêìíòóôõùúăđĩũơƯĂẠẢẤẦẨẪẬẮẰẲẴẶẸẺẼỀỀỂưăạảấầẩẫậắằẳẵặẹẻẽềềểỄỆỈỊỌỎỐỒỔỖỘỚỜỞỠỢỤỦỨỪễệỉịọỏốồổỗộớờởỡợụủứừỬỮỰỲỴÝỶỸửữựỳỵỷỹ ]*$/u', 'message' => Yii::t('profile', 'starts with a letter and contains only word characters')],
            [['nickname'], 'string', 'length' => [3, 30]],
            ['nickname', 'filter', 'filter' => 'trim'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('profile', 'User ID'),
            'nickname' => Yii::t('profile', 'Nickname'),
            'avatar' => Yii::t('profile', 'Avatar'),
            'about_me' => Yii::t('profile', 'About Me'),
            'profile_url' => Yii::t('profile', 'Profile Url'),
            'user_type_id' => Yii::t('profile', 'User Type ID'),
            'language_id' => Yii::t('profile', 'Language ID'),
            'is_superuser' => Yii::t('profile', 'Is Superuser'),
            'membership_type_id' => Yii::t('profile', 'Membership Type ID'),
            'block_to' => Yii::t('profile', 'Block To'),
            'updated_at' => Yii::t('profile', 'Updated At'),
        ];
    }
}
