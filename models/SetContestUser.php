<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property string $contest_id
 * @property string $contest_name
 * @property string $user_id
 * @property string $nickname
 * @property string $set_id
 * @property string $set_name
 * @property string $votes
 */
class SetContestUser extends Model
{
    public $contest_id;
    public $contest_name;
    public $user_id;
    public $nickname;
    public $set_id;
    public $set_name;
    public $votes;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['contest_id', 'user_id', 'set_id', 'votes'], 'integer'],
            [['contest_name', 'nickname', 'set_name'], 'string', 'max' => 255],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'contest_id' => Yii::t('set-contest-user', 'Contest ID'),
            'user_id' => Yii::t('set-contest-user', 'User ID'),
            'set_id' => Yii::t('set-contest-user', 'Set ID'),
            'contest_name' => Yii::t('set-contest-user', 'Contest Name'),
            'nickname' => Yii::t('set-contest-user', 'Nick Name'),
            'set_name' => Yii::t('set-contest-user', 'Set Name'),
            'votes' => Yii::t('set-contest-user', 'Votes'),
        ];
    }
}
