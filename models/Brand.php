<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "brands".
 *
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $image
 * @property integer $type
 * @property integer $status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property Profile $createdBy
 * @property Profile $updatedBy
 * @property Item[] $items
 */
class Brand extends Model
{
    const TYPE_VIETNAM = 0;
    const TYPE_FOREIGN = 1;
    const TYPE_BOTH    = 2;
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $name;
    public $slug;
    public $description;
    public $image;
    public $type;
    public $status;
    public $created_by;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['description'], 'string'],
            [['type', 'status', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'slug'], 'string', 'max' => 100],
            [['image'], 'string', 'max' => 255],
            [['name'], 'unique'],
            [['slug'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('brand', 'ID'),
            'name' => Yii::t('brand', 'Name'),
            'slug' => Yii::t('brand', 'Slug'),
            'description' => Yii::t('brand', 'Description'),
            'image' => Yii::t('brand', 'Image'),
            'type' => Yii::t('brand', 'Type'),
            'status' => Yii::t('brand', 'Status'),
            'created_by' => Yii::t('brand', 'Created By'),
            'created_at' => Yii::t('brand', 'Created At'),
            'updated_by' => Yii::t('brand', 'Updated By'),
            'updated_at' => Yii::t('brand', 'Updated At'),
        ];
    }
}
