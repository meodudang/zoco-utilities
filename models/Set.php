<?php

namespace zoco\models;

use Yii;
use yii\base\Model;
use yii\db\Expression;

/**
 * @property integer $id
 * @property integer $author_id
 * @property integer $owner_id
 * @property integer $category_id
 * @property string $name
 * @property string $slug
 * @property string $image
 * @property string $json_content
 * @property string $item_ids
 * @property string $description
 * @property integer $is_sticky
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property TotalSetStatistic[] $totalSetStatistic
 * @property SetStatistic[] $setStatistic
 * @property Profile $author
 * @property Profile $owner
 * @property Category $category
 * @property ContestSet[] $contestSets
 * @property ContestSet[] $setWinners
 * @property ContestSet[] $setContesting
 * @property Collection[] $collections
 * @property Item[] $items
 * @property LikeObject[] $likes
 */
class Set extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;
    const STATUS_NOT_PUBLIC = 2;
    const STATUS_DRAFT = 4;

    public $id;
    public $author_id;
    public $owner_id;
    public $category_id;
    public $name;
    public $slug;
    public $image;
    public $json_content;
    public $item_ids;
    public $description;
    public $is_sticky;
    public $status;
    public $created_at;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'name', 'image', 'json_content'], 'required'],
            [['author_id', 'owner_id', 'category_id', 'is_sticky', 'status', 'created_at', 'updated_at'], 'integer'],
            [['json_content', 'item_ids', 'description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            ['name', 'string', 'max' => 255],
            // ['image', 'string', 'max' => 255], // nếu uncomment thì save SET ở FE sẽ bị lỗi
            [['slug'], 'string', 'max' => 150]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('set', 'ID'),
            'author_id' => Yii::t('set', 'Author ID'),
            'owner_id' => Yii::t('set', 'Owner ID'),
            'category_id' => Yii::t('set', 'Category ID'),
            'name' => Yii::t('set', 'Name'),
            'slug' => Yii::t('set', 'Slug'),
            'image' => Yii::t('set', 'Image'),
            'json_content' => Yii::t('set', 'Json Content'),
            'item_ids' => Yii::t('set', 'Item Ids'),
            'description' => Yii::t('set', 'Description'),
            'is_sticky' => Yii::t('set', 'Is Sticky'),
            'status' => Yii::t('set', 'Status'),
            'created_at' => Yii::t('set', 'Created At'),
            'updated_at' => Yii::t('set', 'Updated At'),
        ];
    }
}
