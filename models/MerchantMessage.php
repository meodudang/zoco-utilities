<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property integer $author_id
 * @property string $title
 * @property string $message
 * @property integer $quantity
 * @property integer $views
 * @property integer $likes
 * @property integer $replies
 * @property integer $deletes
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Profile $author
 */
class MerchantMessage extends Model
{
    public $id;
    public $author_id;
    public $title;
    public $message;
    public $quantity;
    public $views;
    public $likes;
    public $replies;
    public $deletes;
    public $created_at;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'title', 'message'], 'required'],
            [['author_id', 'quantity', 'views', 'likes', 'replies', 'deletes'], 'integer'],
            [['title'], 'string', 'max' => 100],
            [['message'], 'string'],
            [['created_at', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('merchant-message', 'ID'),
            'author_id' => Yii::t('merchant-message', 'Author ID'),
            'title' => Yii::t('merchant-message', 'Title'),
            'message' => Yii::t('merchant-message', 'Message'),
            'quantity' => Yii::t('merchant-message', 'Quantity'),
            'views' => Yii::t('merchant-message', 'Views'),
            'likes' => Yii::t('merchant-message', 'Likes'),
            'replies' => Yii::t('merchant-message', 'Replies'),
            'deletes' => Yii::t('merchant-message', 'Deletes'),
            'created_at' => Yii::t('merchant-message', 'Created At'),
            'updated_at' => Yii::t('merchant-message', 'Updated At'),
        ];
    }
}
