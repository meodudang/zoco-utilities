<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property integer $author_id
 * @property integer $shop_owner_id
 * @property integer $point
 * @property string $message
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Profile $author
 * @property Profile $shopOwner
 */
class Review extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $author_id;
    public $shop_owner_id;
    public $point;
    public $message;
    public $status;
    public $created_at;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'shop_owner_id', 'point', 'message'], 'required'],
            [['author_id', 'shop_owner_id', 'point', 'status'], 'integer'],
            [['message'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['author_id', 'shop_owner_id'], 'unique', 'targetAttribute' => ['author_id', 'shop_owner_id'], 'message' => 'The combination of Author ID and Shop Owner ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('review', 'ID'),
            'author_id' => Yii::t('review', 'Author ID'),
            'shop_owner_id' => Yii::t('review', 'Shop Owner ID'),
            'point' => Yii::t('review', 'Point'),
            'message' => Yii::t('review', 'Message'),
            'status' => Yii::t('review', 'Status'),
            'created_at' => Yii::t('review', 'Created At'),
            'updated_at' => Yii::t('review', 'Updated At'),
        ];
    }
}
