<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $user_id
 * @property string $email
 * @property string $nickname
 * @property string $registration_ip
 * @property integer $created_at
 */
class UserVoted extends Model
{
    public $user_id;
    public $email;
    public $nickname;
    public $registration_ip;
    public $created_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'created_at'], 'integer'],
            [['created_at'], 'safe'],
            [['email', 'nickname'], 'string', 'max' => 255],
            [['registration_ip'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('user-voted', 'User ID'),
            'email' => Yii::t('user-voted', 'Email'),
            'nickname' => Yii::t('user-voted', 'Nick Name'),
            'registration_ip' => Yii::t('user-voted', 'Registration Ip'),
            'created_at' => Yii::t('user-voted', 'Created At'),
        ];
    }
}
