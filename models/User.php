<?php

namespace zoco\models;

use Yii;
use yii\base\Model;
use yii\web\IdentityInterface;
use zoco\helpers\backend\RestHelper;

/**
 * @property integer $id
 * @property string  $email
 * @property string  $password_hash
 * @property string  $auth_key
 * @property integer $confirmed_at
 * @property string  $unconfirmed_email
 * @property integer $block_to
 * @property integer $registration_ip
 * @property integer $flags
 * @property integer $is_superuser
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Profile  $profile
 */
class User extends Model implements IdentityInterface
{
    public $id;
    public $email;
    public $password_hash;
    public $auth_key;
    public $confirmed_at;
    public $unconfirmed_email;
    public $block_to;
    public $registration_ip;
    public $flags;
    public $is_superuser;
    public $created_at;
    public $updated_at;

    public $profile;

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('user', 'ID'),
            'email' => Yii::t('user', 'Email'),
            'password' => Yii::t('user', 'Password'),
            'auth_key' => Yii::t('user', 'Auth Key'),
            'confirmed_at' => Yii::t('user', 'Confirmed At'),
            'unconfirmed_email' => Yii::t('user', 'New Email'),
            'block_to' => Yii::t('user', 'Block To'),
            'registration_ip' => Yii::t('user', 'Registration IP'),
            'flags' => Yii::t('user', 'Flags'),
            'is_superuser' => Yii::t('user', 'Super User'),
            'created_at' => Yii::t('user', 'Registered At'),
            'updated_at' => Yii::t('user', 'Updated At'),

            'current_password' => Yii::t('user', 'Current Password'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function scenarios()
    {
        return [
            'register'        => ['email', 'password'],
            'connect'         => ['email'],
            'create'          => ['email', 'password'],
            'update'          => ['email', 'password'],
            'update_password' => ['password', 'current_password'],
            'update_email'    => ['unconfirmed_email', 'current_password']
        ];
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            // email rules
            ['email', 'required', 'on' => ['register', 'connect', 'create', 'update', 'update_email']],
            ['email', 'email'],
            ['email', 'string', 'max' => 255],
            ['email', 'unique'],
            ['email', 'trim'],

            // unconfirmed email rules
            ['unconfirmed_email', 'required', 'on' => 'update_email'],
            ['unconfirmed_email', 'unique', 'targetAttribute' => 'email', 'on' => 'update_email'],
            ['unconfirmed_email', 'email', 'on' => 'update_email'],
            ['unconfirmed_email', 'string', 'max' => 255],

            // password rules
            ['password', 'required', 'on' => ['register', 'update_password']],
            ['password', 'string', 'min' => 6, 'max' => 60, 'on' => ['register', 'update_password', 'create']],

            // current password rules
            ['current_password', 'required', 'on' => ['update_email', 'update_password']],
            ['current_password', function ($attr) {
                if (!empty($this->$attr) && !Password::validate($this->$attr, $this->password_hash)) {
                    $this->addError($attr, \Yii::t('user', 'Current Password is not valid'));
                }
            }, 'on' => ['update_email', 'update_password']],

            [['confirmed_at', 'block_to', 'created_at', 'updated_at'], 'safe'],
            [['flags'], 'integer'],
            [['registration_ip'], 'string', 'max' => 50],
            [['auth_key'], 'string', 'max' => 32],
        ];
    }

    /**
     * @return bool Whether the user is blocked or not.
     */
    public function getIsBlocked()
    {
        return $this->block_to >= date("Y-m-d H:i:s");
    }

    /**
     * @return bool Whether the user is an admin or not.
     */
    public function getIsAdmin()
    {
        return in_array($this->email, $this->module->admins);
    }

    /**
     * Blocks the user by setting 'block_to' field to current time.
     *
     * @param  integer $time Timestamp when user is free
     */
    public function block($time)
    {
        return (bool) $this->updateAttributes(['block_to' => $time]);
    }

    /**
     * Unblocks the user by setting 'block_to' field to null.
     */
    public function unblock()
    {
        return (bool) $this->updateAttributes(['block_to' => null]);
    }

    /**
     * @inheritdoc
     */
    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $profile = $this->module->manager->createProfile([
                'user_id' => $this->id,
            ]);
            $profile->save(false);
        }
        parent::afterSave($insert, $changedAttributes);
    }



    /**
     * @inheritdoc
     */
    public function validateAuthKey($authKey)
    {
        return $this->authKey === $authKey;
    }

    /**
     * @inheritdoc
     */
    public static function findIdentity($id)
    {
        return new static(self::getUserId($id));
    }

    /**
     * @inheritdoc
     */
    public static function getUserId($id)
    {
        $user = null;

        $result = RestHelper::callApi('user/admin/one', [
            'params' => ['id' => $id],
        ], true);

        if (isset($result['error']) && $result['error'] == 0) {
            $user = $result['body'];
        }

        return $user;
    }
    /**
     * @inheritdoc
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        $user = null;
        $result = RestHelper::callApi('user/admin/one', ['params' => ['access_token'=>$token]]);
        if ($result['error'] == 0) {
            $user = new User();
            foreach ($result['body'] as $key => $value) {
                $user->$key = $value;
            }
        }
        return isset($user) ? new static($user) : null;

    }

    /**
     * @inheritdoc
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @inheritdoc
     */
    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function getUserProfile()
    {
        if (empty($this->profile)) {
            $result = RestHelper::callApi('user/profile/one', ['params' => ['user_id' => $this->id]], true);
            if ($result['error'] == 0) {
                $this->profile = $result['body'];
            }
        }

        return $this->profile;
    }
}
