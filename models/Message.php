<?php
namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property integer $sender_id
 * @property integer $receiver_id
 * @property string $message
 * @property integer $unread
 * @property string $deleted_user_ids
 * @property integer $merchant_message_id
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Profile $sender
 * @property Profile $receiver
 * @property MerchantMessage $merchantMessage
 */
class Message extends Model
{
    const STATUS_READ = 1;
    const STATUS_UNREAD = 0;

    public $id;
    public $sender_id;
    public $receiver_id;
    public $message;
    public $unread;
    public $deleted_user_ids;
    public $merchant_message_id;
    public $created_at;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['sender_id', 'receiver_id'], 'required'],
            [['sender_id', 'receiver_id', 'unread', 'merchant_message_id', 'created_at', 'updated_at'], 'integer'],
            [['message', 'deleted_user_ids'], 'string'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('message', 'ID'),
            'sender_id' => Yii::t('message', 'Sender ID'),
            'receiver_id' => Yii::t('message', 'Receiver ID'),
            'message' => Yii::t('message', 'Message'),
            'unread' => Yii::t('message', 'Unread'),
            'deleted_user_ids' => Yii::t('message', 'Deleted User Ids'),
            'merchant_message_id' => Yii::t('message', 'Merchant Message ID'),
            'created_at' => Yii::t('message', 'Created At'),
            'updated_at' => Yii::t('message', 'Updated At'),
        ];
    }
}
