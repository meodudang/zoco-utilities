<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property string $name
 * @property integer $price
 * @property integer $status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property Profile $createdBy
 * @property Profile $updatedBy
 * @property Membership[] $memberships
 * @property Profile[] $users
 * @property MembershipOption[] $membershipOptions
 */
class MembershipType extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $name;
    public $price;
    public $status;
    public $created_by;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name'], 'required'],
            [['status', 'price', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 100],
            [['name'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('membership-type', 'ID'),
            'name' => Yii::t('membership-type', 'Name'),
            'price' => Yii::t('membership-type', 'Price'),
            'status' => Yii::t('membership-type', 'Status'),
            'created_by' => Yii::t('membership-type', 'Created By'),
            'created_at' => Yii::t('membership-type', 'Created At'),
            'updated_by' => Yii::t('membership-type', 'Updated By'),
            'updated_at' => Yii::t('membership-type', 'Updated At'),
        ];
    }
}
