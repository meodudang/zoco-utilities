<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $description
 * @property string $type
 * @property integer $date_start
 * @property integer $date_end
 * @property integer $price
 * @property integer $percent_sale_off
 * @property integer $sale_off
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Profile $createdBy
 * @property Profile $updatedBy
 * @property PromotionItem[] $promotionItems
 * @property Item[] $items
 */
class Promotion extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_UPCOMING = 1;
    const STATUS_ONGOING = 2;
    const STATUS_THEEND = 3;
    const STATUS_NOAPPROVAL = 4;

    public $id;
    public $name;
    public $image;
    public $slug;
    public $description;
    public $type;
    public $date_start;
    public $date_end;
    public $price;
    public $percent_sale_off;
    public $sale_off;
    public $status;
    public $created_at;
    public $updated_at;
    public $created_by;
    public $updated_by;

   /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'status', 'date_start', 'date_end', 'created_by'], 'required'],
            [['type', 'date_start', 'date_end', 'status', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['price', 'sale_off', 'percent_sale_off'], 'number'],
            [['name', 'slug'], 'string', 'max' => 150],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('promotion', 'ID'),
            'name' => Yii::t('promotion', 'Name'),
            'slug' => Yii::t('promotion', 'Slug'),
            'description' => Yii::t('promotion', 'Description'),
            'type' => Yii::t('promotion', 'Type'),
            'date_start' => Yii::t('promotion', 'Start Date'),
            'date_end' => Yii::t('promotion', 'End Date'),
            'price' => Yii::t('promotion', 'Price'),
            'sale_off' => Yii::t('promotion', 'Sale Off Price'),
            'percent_sale_off' => Yii::t('promotion', 'Percent Sale Off'),
            'status' => Yii::t('promotion', 'Status'),
            'created_by' => Yii::t('promotion', 'Created By'),
            'created_at' => Yii::t('promotion', 'Created At'),
            'updated_by' => Yii::t('promotion', 'Updated By'),
            'updated_at' => Yii::t('promotion', 'Updated At'),
        ];
    }
}
