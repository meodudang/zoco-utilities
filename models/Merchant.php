<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $user_id
 * @property string $name
 * @property string $slug
 * @property string $about_merchant
 * @property string $phone
 * @property string $website
 * @property integer $address
 * @property integer $ratings
 * @property integer $auto_update
 * @property integer $auto_import
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Profile $profile
 */
class Merchant extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $user_id;
    public $name;
    public $slug;
    public $about_merchant;
    public $phone;
    public $website;
    public $address;
    public $ratings;
    public $auto_update;
    public $auto_import;
    public $status;
    public $created_at;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'name', 'phone', 'address'], 'required'],
            [['user_id', 'auto_update', 'auto_import', 'status', 'created_at', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['about_merchant', 'address'], 'string'],
            [['ratings'], 'number'],
            [['website'], 'string', 'max' => 255],
            [['name', 'slug'], 'string', 'length' => [3, 50]],
            [['phone'], 'string', 'max' => [9, 20]],
            ['name', 'filter', 'filter' => 'trim'],
        ];
    }
    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'user_id' => Yii::t('merchant', 'User ID'),
            'name' => Yii::t('merchant', 'Name'),
            'slug' => Yii::t('merchant', 'Slug'),
            'about_merchant' => Yii::t('merchant', 'About Merchant'),
            'website' => Yii::t('merchant', 'Website'),
            'phone' => Yii::t('merchant', 'Phone'),
            'address' => Yii::t('merchant', 'Address'),
            'ratings' => Yii::t('merchant', 'Ratings'),
            'auto_update' => Yii::t('merchant', 'Auto Update'),
            'auto_import' => Yii::t('merchant', 'Auto Import'),
            'status' => Yii::t('merchant', 'Status'),
            'created_at' => Yii::t('merchant', 'Created At'),
            'updated_at' => Yii::t('merchant', 'Updated At'),
        ];
    }
}
