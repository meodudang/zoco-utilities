<?php

namespace zoco\models;

use Yii;
use yii\base\Model;
use yii\db\Expression;

/**
 * @property integer $id
 * @property string $image
 * @property integer $customer
 * @property string $link
 * @property string $start
 * @property string $end
 * @property integer $order
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property Profile $createdBy
 * @property Profile $updatedBy
 */
class Highlight extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $image;
    public $customer;
    public $link;
    public $start;
    public $end;
    public $order;
    public $created_by;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['order','created_by','updated_by'], 'integer'],
            [['image','customer','link'], 'string','max'=>250],
            [['start', 'end'],  'string','max'=>20],
            [['created_at', 'updated_at'], 'safe'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('highlight', 'ID'),
            'image' => Yii::t('highlight', 'Image'),
            'customer' => Yii::t('highlight', 'Customer'),
            'link' => Yii::t('highlight', 'Link'),
            'start' => Yii::t('highlight', 'Start'),
            'end' => Yii::t('highlight', 'End'),
            'order' => Yii::t('highlight', 'Order'),
            'created_by' => Yii::t('highlight', 'Created By'),
            'created_at' => Yii::t('highlight', 'Created At'),
            'updated_by' => Yii::t('highlight', 'Updated By'),
            'updated_at' => Yii::t('highlight', 'Updated At'),
        ];
    }
}
