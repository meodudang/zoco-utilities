<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property integer $user_id
 * @property integer $module_id
 * @property string $key
 * @property string $value
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Profile $user
 * @property Module $module
 */
class Setting extends Model
{
    public $id;
    public $user_id;
    public $module_id;
    public $key;
    public $value;
    public $created_at;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'module_id', 'key', 'value'], 'required'],
            [['user_id', 'module_id'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['key', 'value'], 'string', 'max' => 30],
            [['module_id', 'key', 'user_id'], 'unique', 'targetAttribute' => ['module_id', 'key', 'user_id'], 'message' => 'The combination of User ID, Module ID and Key has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('setting', 'ID'),
            'user_id' => Yii::t('setting', 'User ID'),
            'module_id' => Yii::t('setting', 'Module ID'),
            'key' => Yii::t('setting', 'Key'),
            'value' => Yii::t('setting', 'Value'),
            'created_at' => Yii::t('setting', 'Created At'),
            'updated_at' => Yii::t('setting', 'Updated At'),
        ];
    }
}
