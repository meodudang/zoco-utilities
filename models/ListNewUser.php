<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property string $email
 * @property string $nickname
 * @property string $registration_ip
 * @property integer $confirmed_at
 * @property integer $created_at
 */
class ListNewUser extends Model
{
    public $id;
    public $email;
    public $nickname;
    public $registration_ip;
    public $confirmed_at;
    public $created_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id'], 'integer'],
            [['created_at'], 'safe'],
            [['email', 'nickname'], 'string', 'max' => 255],
            [['registration_ip'], 'string', 'max' => 50],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('list-new-user', 'ID'),
            'email' => Yii::t('list-new-user', 'Email'),
            'nickname' => Yii::t('list-new-user', 'Nick Name'),
            'registration_ip' => Yii::t('list-new-user', 'Registration Ip'),
            'confirmed_at' => Yii::t('list-new-user', 'Confirmed At'),
            'created_at' => Yii::t('list-new-user', 'Created At'),
        ];
    }
}
