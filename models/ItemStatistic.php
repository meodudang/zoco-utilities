<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $item_id
 * @property integer $views
 * @property integer $likes
 * @property integer $comments
 * @property integer $bookmarks
 * @property integer $collections
 * @property integer $sets
 * @property integer $impressions
 * @property integer $popups
 * @property integer $emotions
 * @property integer $shares
 * @property integer $clicks
 * @property string $date
 * @property integer $updated_at
 *
 * @property Items $item
 */
class ItemStatistic extends Model
{
    public $item_id;
    public $views;
    public $likes;
    public $comments;
    public $bookmarks;
    public $collections;
    public $sets;
    public $impressions;
    public $popups;
    public $emotions;
    public $shares;
    public $clicks;
    public $date;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['item_id', 'date'], 'required'],
            [['item_id', 'views', 'likes', 'impressions', 'popups', 'emotions', 'shares', 'comments', 'bookmarks', 'collections', 'sets', 'clicks'], 'integer'],
            [['item_id', 'date'], 'unique', 'targetAttribute' => ['item_id', 'date'], 'message' => 'The combination of Item ID, Date has already been taken.'],
            [['date', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'item_id' => Yii::t('item-statistic', 'Item ID'),
            'views' => Yii::t('item-statistic', 'Views'),
            'likes' => Yii::t('item-statistic', 'Likes'),
            'comments' => Yii::t('item-statistic', 'Comments'),
            'bookmarks' => Yii::t('item-statistic', 'Bookmarks'),
            'collections' => Yii::t('item-statistic', 'Collections'),
            'sets' => Yii::t('item-statistic', 'Sets'),
            'impressions' => Yii::t('item-statistic', 'Impressions'),
            'popups' => Yii::t('item-statistic', 'Popups'),
            'emotions' => Yii::t('item-statistic', 'Emotions'),
            'shares' => Yii::t('item-statistic', 'Shares'),
            'clicks' => Yii::t('item-statistic', 'Clicks'),
            'date' => Yii::t('item-statistic', 'Date'),
            'updated_at' => Yii::t('item-statistic', 'Updated At'),
        ];
    }
}
