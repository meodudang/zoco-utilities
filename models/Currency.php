<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property string $name
 * @property string $code
 * @property string $symbol
 * @property string $format
 * @property string $base
 * @property float $rate
 * @property integer $precision
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 */
class Currency extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $name;
    public $code;
    public $symbol;
    public $format;
    public $base;
    public $rate;
    public $precision; // number of decimal digits to round to
    public $status;
    public $created_at;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'code', 'symbol', 'rate'], 'required'],
            [['rate'], 'number'],
            [['precision', 'status', 'created_at', 'updated_at'], 'integer'],
            [['name', 'symbol', 'format'], 'string', 'max' => 50],
            [['created_at', 'updated_at'], 'safe'],
            [['code', 'base'], 'string', 'max' => 3],
            [['code', 'base'], 'unique', 'targetAttribute' => ['code', 'base'], 'message' => 'The combination of Code and Base Currency has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('currency', 'ID'),
            'name' => Yii::t('currency', 'Name'),
            'code' => Yii::t('currency', 'Code'),
            'symbol' => Yii::t('currency', 'Symbol'),
            'format' => Yii::t('currency', 'Format'),
            'base' => Yii::t('currency', 'Base Currency'),
            'rate' => Yii::t('currency', 'Exchange Rate'),
            'precision' => Yii::t('currency', 'Precision'),
            'status' => Yii::t('currency', 'Status'),
            'created_at' => Yii::t('currency', 'Created At'),
            'updated_at' => Yii::t('currency', 'Updated At'),
        ];
    }
}
