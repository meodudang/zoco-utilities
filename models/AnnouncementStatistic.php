<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $announcement_id
 * @property integer $views
 * @property integer $likes
 * @property string $date
 * @property integer $updated_at
 *
 * @property Announcement $announcement
 */
class AnnouncementStatistic extends Model
{
    public $announcement_id;
    public $views;
    public $likes;
    public $date;
    public $updated_at;

    /**
     * Relation
     * @var array
     */
    public $announcement;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['announcement_id', 'views', 'likes', 'date'], 'required'],
            [['announcement_id', 'views', 'likes', 'updated_at'], 'integer'],
            [['announcement_id', 'date'], 'unique', 'targetAttribute' => ['announcement_id', 'date'], 'message' => 'The combination of Announcement ID, Date has already been taken.'],
            [['date', 'updated_at'], 'safe']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'announcement_id' => Yii::t('announcement-statistic', 'Announcement ID'),
            'views' => Yii::t('announcement-statistic', 'Views'),
            'likes' => Yii::t('announcement-statistic', 'Likes'),
            'date' => Yii::t('announcement-statistic', 'Date'),
            'updated_at' => Yii::t('announcement-statistic', 'Updated At'),
        ];
    }
}
