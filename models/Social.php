<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * This is the model class for table "socials".
 *
 * @property integer $id
 * @property integer $user_id
 * @property string $provider
 * @property string $client_id
 * @property string $properties
 * @property string $session_data
 * @property integer $is_show
 * @property string $updated_at
 *
 * @property Users $user
 */
class Social extends Model
{
    public $id;
    public $user_id;
    public $provider;
    public $client_id;
    public $properties;
    public $session_data;
    public $is_show;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public $user;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['user_id', 'provider', 'client_id'], 'required'],
            [['user_id', 'is_show'], 'integer'],
            [['properties', 'session_data'], 'string'],
            [['updated_at'], 'safe'],
            [['provider'], 'string', 'max' => 10],
            [['client_id'], 'string', 'max' => 50],
            [['user_id', 'provider', 'client_id'], 'unique', 'targetAttribute' => ['user_id', 'provider', 'client_id'], 'message' => 'The combination of User ID, Provider and Client ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('social', 'ID'),
            'user_id' => Yii::t('social', 'User ID'),
            'provider' => Yii::t('social', 'Provider'),
            'client_id' => Yii::t('social', 'Client ID'),
            'properties' => Yii::t('social', 'Properties'),
            'session_data' => Yii::t('social', 'Session Data'),
            'is_show' => Yii::t('social', 'Is Show'),
            'updated_at' => Yii::t('social', 'Updated At'),
        ];
    }
}
