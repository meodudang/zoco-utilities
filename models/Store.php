<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property string $name
 * @property string $slug
 * @property string $website
 * @property integer $in_vietnam
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property Item[] $items
 * @property Profile $updatedBy
 */
class Store extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $name;
    public $slug;
    public $website;
    public $in_vietnam;
    public $status;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['name', 'website'], 'required'],
            [['in_vietnam', 'status', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'slug'], 'string', 'max' => 150],
            [['website'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('store', 'ID'),
            'name' => Yii::t('store', 'Name'),
            'slug' => Yii::t('store', 'Slug'),
            'website' => Yii::t('store', 'Website'),
            'in_vietnam' => Yii::t('store', 'In Vietnam'),
            'status' => Yii::t('store', 'Status'),
            'created_at' => Yii::t('store', 'Created At'),
            'updated_by' => Yii::t('store', 'Updated By'),
            'updated_at' => Yii::t('store', 'Updated At'),
        ];
    }
}
