<?php

namespace zoco\models;

use Yii;
use yii\base\Model;
use vova07\fileapi\behaviors\UploadBehavior;

/**
 * @property integer $id
 * @property integer $brand_id
 * @property integer $author_id
 * @property integer $owner_id
 * @property integer $store_id
 * @property string $name
 * @property string $url
 * @property string $url_image
 * @property integer $gender
 * @property string $slug
 * @property string $image
 * @property integer $image_width
 * @property integer $image_height
 * @property string $image_transparent
 * @property integer $image_transparent_width
 * @property integer $image_transparent_height
 * @property double $price
 * @property double $sale_price
 * @property string $currency
 * @property string $color
 * @property integer $made_in
 * @property string $description
 * @property integer $has_model
 * @property integer $type
 * @property integer $is_sticky
 * @property integer $is_nsfw
 * @property integer $is_shop
 * @property string $more
 * @property integer $status
 * @property integer $created_at
 * @property integer $updated_at
 *
 * @property TotalItemStatistic $totalItemStatistic
 * @property ItemStatistic[] $itemStatisticDaily
 * @property Profile $author
 * @property Store $store
 * @property Brand $brand
 * @property Category[] $categories
 * @property Profile $owner
 * @property Country $madeIn
 * @property Set[] $sets
 * @property Collection[] $collections
 * @property Like[] $likes
 * @property Cart[] $carts
 * @property PromotionItem[] $promotionItems
 * @property Promotion[] $promotions
 */
class Item extends Model
{
    const FAIL = 0;
    const SUCCESS = 1;
    const WAITING = 2;

    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $brand_id;
    public $author_id;
    public $owner_id;
    public $store_id;
    public $name;
    public $url;
    public $url_image;
    public $gender;
    public $slug;
    public $image;
    public $image_width;
    public $image_height;
    public $image_transparent;
    public $image_transparent_width;
    public $image_transparent_height;
    public $price;
    public $sale_price;
    public $currency;
    public $color;
    public $made_in;
    public $description;
    public $type;
    public $has_model;
    public $is_sticky;
    public $is_nsfw;
    public $is_shop;
    public $more;
    public $status;
    public $created_at;
    public $updated_at;

     /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['author_id', 'store_id', 'name', 'url', 'image'], 'required'],
            [['brand_id', 'author_id', 'owner_id', 'store_id', 'gender', 'image_width', 'image_height', 'image_transparent_width', 'image_transparent_height', 'made_in', 'type', 'has_model', 'is_sticky', 'is_nsfw', 'is_shop', 'status', 'created_at', 'updated_at'], 'integer'],
            [['price', 'sale_price'], 'number'],
            [['description', 'more'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name', 'image', 'image_transparent'], 'string', 'max' => 255],
            [['url', 'url_image'], 'string', 'max' => 255],
            [['slug'], 'string', 'max' => 150],
            [['currency'], 'string', 'max' => 3],
            [['color'], 'string', 'max' => 150],
            // [['url'], 'unique']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('item', 'ID'),
            'brand_id' => Yii::t('item', 'Brand ID'),
            'author_id' => Yii::t('item', 'Author ID'),
            'owner_id' => Yii::t('item', 'Owner ID'),
            'store_id' => Yii::t('item', 'Store ID'),
            'name' => Yii::t('item', 'Name'),
            'url' => Yii::t('item', 'Url'),
            'url_image' => Yii::t('item', 'Url Image'),
            'gender' => Yii::t('item', 'Gender'),
            'slug' => Yii::t('item', 'Slug'),
            'image' => Yii::t('item', 'Image'),
            'image_width' => Yii::t('item', 'Image Width'),
            'image_height' => Yii::t('item', 'Image Height'),
            'image_transparent' => Yii::t('item', 'Image Transparent'),
            'image_transparent_width' => Yii::t('item', 'Image Transparent Width'),
            'image_transparent_height' => Yii::t('item', 'Image Transparent Height'),
            'price' => Yii::t('item', 'Price'),
            'sale_price' => Yii::t('item', 'Sale Price'),
            'currency' => Yii::t('item', 'Currency'),
            'color' => Yii::t('item', 'Color'),
            'made_in' => Yii::t('item', 'Made In'),
            'description' => Yii::t('item', 'Description'),
            'type' => Yii::t('item', 'Type'),
            'has_model' => Yii::t('item', 'Has Model'),
            'is_sticky' => Yii::t('item', 'Is Sticky'),
            'is_nsfw' => Yii::t('item', 'Is NSFW'),
            'is_shop' => Yii::t('item', 'Shop Item'),
            'more' => Yii::t('item', 'More'),
            'status' => Yii::t('item', 'Status'),
            'created_at' => Yii::t('item', 'Created At'),
            'updated_at' => Yii::t('item', 'Updated At'),
        ];
    }

    /**
     * @inheritdoc
     */
    public function behaviors()
    {
        return [
            'uploadBehavior' => [
                'class' => UploadBehavior::className(),
                'attributes' => [
                    'image' => [
                        'path' => Yii::$app->params['pathUpload'] . $this->module->uploadDir,
                        'tempPath' => Yii::$app->params['pathUploadTmp'] . $this->module->uploadDir,
                        'url' => Yii::$app->params['pathUploadUrl'] . $this->module->uploadDir,
                    ]
                ]
            ]
        ];
    }
}
