<?php

namespace zoco\models;

use Yii;
use yii\base\Model;

/**
 * @property integer $id
 * @property string $name
 * @property integer $object_type_id
 * @property string $message
 * @property string $description
 * @property integer $language_id
 * @property integer $subtract_point
 * @property integer $plus_point
 * @property integer $status
 * @property integer $created_by
 * @property integer $created_at
 * @property integer $updated_by
 * @property integer $updated_at
 *
 * @property Language $language
 * @property Profile $createdBy
 * @property Profile $updatedBy
 * @property BadReport[] $badReports
 */
class BadReportType extends Model
{
    const STATUS_INACTIVE = 0;
    const STATUS_ACTIVE = 1;

    public $id;
    public $name;
    public $object_type_id;
    public $message;
    public $description;
    public $language_id;
    public $subtract_point;
    public $plus_point;
    public $status;
    public $created_by;
    public $created_at;
    public $updated_by;
    public $updated_at;

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['object_type_id', 'name', 'message', 'language_id', 'created_by'], 'required'],
            [['object_type_id', 'language_id', 'subtract_point', 'plus_point', 'status', 'created_by', 'created_at', 'updated_by', 'updated_at'], 'integer'],
            [['description'], 'string'],
            [['created_at', 'updated_at'], 'safe'],
            [['name'], 'string', 'max' => 150],
            [['message'], 'string', 'max' => 255],
            [['name', 'object_type_id', 'message', 'language_id'], 'unique', 'targetAttribute' => ['name', 'object_type_id', 'message', 'language_id'], 'message' => 'The combination of Name, Object Type ID, Message and Language ID has already been taken.']
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('bad-report-type', 'ID'),
            'name' => Yii::t('bad-report-type', 'Name'),
            'object_type_id' => Yii::t('bad-report-type', 'Object Type ID'),
            'message' => Yii::t('bad-report-type', 'Message'),
            'description' => Yii::t('bad-report-type', 'Description'),
            'language_id' => Yii::t('bad-report-type', 'Language ID'),
            'subtract_point' => Yii::t('bad-report-type', 'Subtract Point'),
            'plus_point' => Yii::t('bad-report-type', 'Plus Point'),
            'status' => Yii::t('bad-report-type', 'Status'),
            'created_by' => Yii::t('bad-report-type', 'Created By'),
            'created_at' => Yii::t('bad-report-type', 'Created At'),
            'updated_by' => Yii::t('bad-report-type', 'Updated By'),
            'updated_at' => Yii::t('bad-report-type', 'Updated At'),
        ];
    }
}
