<?php
/**
 * @copyright Copyright (c) 2013 2amigOS! Consulting Group LLC
 * @link http://2amigos.us
 * @license http://www.opensource.org/licenses/bsd-license.php New BSD License
 */

namespace zoco\widgets;

use dosamigos\fileupload\FileUploadUI as BaseFileUploadUI;
use yii\helpers\ArrayHelper;

/**
 * FileUploadUI
 *
 * Widget to render the jQuery File Upload UI plugin as shown in
 * [its demo](http://blueimp.github.io/jQuery-File-Upload/index.html)
 *
 * @author Antonio Ramirez <amigo.cobos@gmail.com>
 * @link http://www.ramirezcobos.com/
 * @link http://www.2amigos.us/
 * @package dosamigos\fileupload
 */
class FileUploadUI extends BaseFileUploadUI
{
    /**
     * @inheritdoc
     */
    public function init()
    {
        parent::init();

        $this->fieldOptions['multiple'] = true;
        $this->fieldOptions['id'] = ArrayHelper::getValue($this->options, 'id');
        $this->options['id'] = $this->fieldOptions['idform'];
        $this->options['showForm'] = $this->fieldOptions['showForm'];
        $this->options['enctype'] = 'multipart/form-data';
        $this->options['uploadTemplateId'] = $this->uploadTemplateId ? : '#template-upload';
        $this->options['downloadTemplateId'] = $this->downloadTemplateId ? : '#template-download';
    }
}
