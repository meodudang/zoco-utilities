<?php
/**
 * @link http://www.yiiframework.com/
 * @copyright Copyright (c) 2008 Yii Software LLC
 * @license http://www.yiiframework.com/license/
 */

namespace zoco\widgets;

use yii\base\InvalidCallException;
use yii\helpers\Html;
use yii\helpers\Json;
use yii\widgets\ActiveForm as BaseActiveForm;

/**
 * ActiveForm is a widget that builds an interactive HTML form for one or multiple data models.
 *
 * @author Qiang Xue <qiang.xue@gmail.com>
 * @since 2.0
 */
class ActiveForm extends BaseActiveForm
{
    /**
     * Runs the widget.
     * This registers the necessary javascript code and renders the form close tag.
     * @throws InvalidCallException if `beginField()` and `endField()` calls are not matching
     */
    public function run()
    {
        if (!empty($this->_fields)) {
            throw new InvalidCallException('Each beginField() should have a matching endField() call.');
        }

        $id = $this->options['id'];
        $options = Json::encode($this->getClientOptions());
        $attributes = Json::encode($this->attributes);
        $view = $this->getView();
        // ActiveFormAsset::register($view);
        // $view->registerJs("jQuery('#$id').yiiActiveForm($attributes, $options);");

        echo Html::endForm();
    }
}
